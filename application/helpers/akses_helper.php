<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Session Helper
 *
 * A simple session class helper for Codeigniter
 *
 * @package     Codeigniter Session Helper
 * @author      Dwayne Charrington
 * @copyright   Copyright (c) 2014, Dwayne Charrington
 * @license     http://www.apache.org/licenses/LICENSE-2.0.html
 * @link        http://ilikekillnerds.com
 * @since       Version 1.0
 * @filesource
 */

function safe($str)
{
    return strip_tags(trim($str));
}

function createFile($string, $path)
{
    $create = fopen($path, "w") or die("Change your permision folder for application and harviacode folder to 777");
    fwrite($create, $string);
    fclose($create);

    return $path;
}

function label($str)
{
    $label = str_replace('_', ' ', $str);
    $label = ucwords($label);
    return $label;
}


function angka($str)
{
    // $label = str_replace('_', ' ', $str);
    $label = number_format($str,'0','','.');
    return $label;
}

function status($str)
{
    $CI = get_instance();
    $CI->menu_db=$CI->load->database('default',true);
    $cek=$CI->menu_db->query("SELECT status FROM piutang where kd_piutang=$str")->row();
    if($cek->status==0){
        $sts='Belum Lunas';
    } elseif($cek->status==1){
        $sts='Lunas';
    } else {
        $sts='Cicilan';
    }
    return $sts;
}

function kelas($str)
{
    $CI = get_instance();
    $CI->menu_db=$CI->load->database('default',true);
    $status=$CI->menu_db->query("SELECT nama_kelas FROM ms_kelas WHERE kd_kelas='$str'")->row();
    return $status->nama_kelas;
}
function nama_petugas($str)
{
    $CI = get_instance();
    $CI->menu_db=$CI->load->database('default',true);
    $status=$CI->menu_db->query("SELECT * FROM petugas WHERE id='$str'")->row();
    return $status->nama;
}
function nama_petugas2($str)
{
    $CI = get_instance();
    $CI->menu_db=$CI->load->database('default',true);
    $status=$CI->menu_db->query("SELECT * FROM petugas WHERE id='$str'")->row();
    return $status->nickname;
}
function nama_teknisi($str)
{
    $CI = get_instance();
    $CI->menu_db=$CI->load->database('default',true);
    $status=$CI->menu_db->query("SELECT * FROM teknisi WHERE id='$str'")->row();
    return $status->nama;
}
function foto()
{
    $CI = get_instance();
    $CI->menu_db=$CI->load->database('default',true);
    $x=get_userdata('app_id_pengguna');
    $status=$CI->menu_db->query("SELECT * FROM ms_pengguna WHERE id_inc='$x'")->row();
    return $status->foto;
}
function nama_pegawai($str)
{
    $CI = get_instance();
    $CI->menu_db=$CI->load->database('default',true);
    $status=$CI->menu_db->query("SELECT * FROM ms_pegawai WHERE kd_pegawai='$str'")->row();
    return $status->nm_pegawai;
}
function qr_a($str)
{
    $CI = get_instance();
    $CI->menu_db=$CI->load->database('default',true);
    $status=$CI->menu_db->query("SELECT * FROM asisten_manajer WHERE nama='$str'")->row();
    return $status->qr;
}
function qr_b($str)
{
    $CI = get_instance();
    $CI->menu_db=$CI->load->database('default',true);
    $status=$CI->menu_db->query("SELECT * FROM petugas WHERE nama='$str'")->row();
    return $status->qr;
}
function kd_kategori_tagihan($str)
{
    $CI = get_instance();
    $CI->menu_db=$CI->load->database('default',true);
    $status=$CI->menu_db->query("SELECT kd_kategori_tagihan FROM ms_kategori_tagihan WHERE kd_kategori_tagihan='$str'")->row();
    return $status->kd_kategori_tagihan;
}
if (!function_exists('showMenu'))
{
    function showMenu($kode_group)
    {
        $CI = get_instance();
         $CI->menu_db=$CI->load->database('default',true);
        $qmenu0  =$CI->menu_db->query("SELECT a.*
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'
                                          AND parent='0'
                                          ORDER BY sort ASC")->result_array();

        $varmenu='';
              foreach ($qmenu0 as $row0) {
                $parent  =$row0['id_inc'];
                $qmenu1  =$CI->menu_db->query("SELECT a.*
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'
                                          AND parent='$parent'
                                          ORDER BY sort ASC");
                $cekmenu =$qmenu1->num_rows();
                $dmenu1  =$qmenu1->result_array();
                  if($cekmenu>0){

                    $varmenu.="<li class='treeview'>
                    <a href='#'>
                      <i class='".$row0['icon']."'></i> <span>".ucwords($row0['nama_menu'])."</span>
                      <span class='pull-right-container'>
                        <i class='fa fa-angle-left pull-right'></i>
                      </span>
                    </a>";


                        $varmenu.= "<ul class='treeview-menu'>";
                          foreach($dmenu1 as $row1){
                              $varmenu.= "<li>".anchor(strtolower($row1['link_menu']),"<i class='fa fa-circle-o'></i> ".ucwords($row1['nama_menu']))."</li>";
                          }
                      $varmenu.= "</ul>
                      </li>";
                      }else{
                       $varmenu.= "<li >".anchor(strtolower($row0['link_menu']),"<i class='".$row0['icon']."'></i> ".ucwords($row0['nama_menu']))."</li>";
                        // $varmenu.='<li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>';
                    }
                  }
           echo $varmenu;
    }
}


if (!function_exists('errorbos'))
{
    function errorbos()
    {
            $CI = &get_instance();
            echo $CI->load->view('errors/403','',true);
            die();
    }
}

if (!function_exists('cek'))
{
    function cek($pengguna,$url,$var)
    {
        $CI = get_instance();
        $CI->menu_db=$CI->load->database('default',true);

        if(empty($var)){
            errorbos();
        }
        switch ($var) {
            case 'read':
                # code...
                $vv='status';
                break;
            case 'create':
                  # code...
            $vv='is_create';
                  break;
            case 'update':
            $vv='is_update';
                # code...
                break;
              case 'delete':
              $vv='is_delete';
                # code...
                break;
            default:
                # code...
                $vv='';
                break;
        }

        $res=$CI->menu_db->query("SELECT a.*
                                FROM ms_privilege a
                                JOIN ms_menu b ON a.ms_menu_id=b.id_inc
                                JOIN ms_assign_role d on d.ms_role_id=a.ms_role_id
                                JOIN ms_pengguna c ON c.id_inc=d.ms_pengguna_id
                                WHERE c.id_inc=$pengguna AND REPLACE(link_menu,'/','.')='$url' AND a.".$vv." ='1'")->row();

        if($res){
                return array(
                    'is_read'=>$res->status,
                    'is_create'=>$res->is_create,
                    'is_update'=>$res->is_update,
                    'is_delete'=>$res->is_delete
                );

        }else{
            errorbos();
        }
    }
}


if (!function_exists('nomor_wo')) {
    function nomor_wo()
    {

        // $tg   = (int) date('His'); //4
        $m   = date('m'); //4
        $y   = date('Y'); //4
        $CI   = get_instance();
        $CI->load->database();


        $var = $m."/".$y;

        $ras = $CI->db->query("SELECT MAX(nomor) ID FROM wo WHERE nomor LIKE '%$var'")->row();
        $idmax = $ras->ID;
        $NoUrut = (int) substr($idmax ,3,3);
        $NoUrut++; //nomor urut +1
        $newId = "WO.". sprintf('%03s', $NoUrut)."/".$m."/".$y;

        return $newId;
    }
}

  if(!function_exists('dd')){
    function dd($str){
        var_dump($str);
        die();
      return var_dump($str);
    }
  }
  if(!function_exists('grub')){
    function grub($nisn){

          $kode ='TRX-'; //4
          $th   =date('dmYHi'.$nisn); //4
          $CI   = get_instance();
          $CI->load->database();


      $var=$kode.$th;

      $ras = $CI->db->query("SELECT MAX(grub) ID FROM pembayaran WHERE grub LIKE '$var%'")->row();
      $idmax=$ras->ID;
      $NoUrut = (int) substr($idmax, 10, 2);
      $NoUrut++; //nomor urut +1
      $newId=$var.sprintf('%02s', $NoUrut);

      return $newId;
    }
  }
  if (!function_exists('send_mail'))
{
    function send_mail($penerima,$subjek,$isi,$name)
    {
        $CI = get_instance();
        $CI->load->database();
        $ras = $CI->db->query("SELECT * FROM ms_email WHERE id=1")->row();
        $from_email = $ras->email;
            $to_email = $penerima;

             $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => $from_email,
                    'smtp_pass' => $ras->password,
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
            );
            $CI->load->library('email', $config);
            // $this->load->library('email', $config);
            $CI->email->set_newline("\r\n");

             $CI->email->from($from_email, 'SD Muhammadiyah');
             $CI->email->to($to_email);
             $CI->email->subject($subjek);
             $CI->email->message($isi);
             $CI->email->attach(dirname(dirname(dirname(__FILE__)))."/upload/".$name);
             //Send mail
             if($CI->email->send()){
                echo "1";
             }else {
                echo $CI->email->print_debugger();
             }
    }
}
 function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}