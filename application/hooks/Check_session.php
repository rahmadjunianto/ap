<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Check_session
{
        public function __construct()
        {
                $this->CI = &get_instance();
                $this->CI->load->library('session');
        }

        public function validate()
        {
            $exp  =array('auth');
            $ctrl =$this->CI->uri->segment(1);
            $var  =$this->CI->session->userdata('kuncisdmm');
            $idp  =$this->CI->session->userdata('app_id_pengguna');
            $sess_token  =$this->CI->session->userdata('app_token');
            if(!in_array(strtolower($ctrl),$exp)){




                if ($var <>1 ) {
                    $this->CI->session->set_flashdata('notif',' <div class="note note-warning">Silahkan login terlebih dahulu</div>');
                    redirect('auth');
                    die();
                }

                // cek token
                $tk=$this->CI->db->query("SELECT token_login FROM ms_pengguna WHERE id_inc=$idp")->row();
                $token=$tk->token_login;
                if($sess_token != $token){
                    session_destroy();
                    // header('Location: index.php');
                    $this->CI->session->set_flashdata('notif',' <div class="note note-warning">Login di browser lain. </div>');
                    redirect('auth');
                    die();
               }
            }
        }
}