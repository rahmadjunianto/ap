<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log_book_petugas_model extends CI_Model
{

    public $table = 'log_book_petugas';
    public $id = 'log_book_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->result();
    }
    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where('log_book_id', $id);
        $this->db->delete($this->table);
    }

}

/* End of file Log_book_model.php */
/* Location: ./application/models/Log_book_model.php */