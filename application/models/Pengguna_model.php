<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengguna_model extends CI_Model
{

    public $table = 'ms_pengguna';
    public $id = 'id_inc';
    public $order = 'DESC';
    public $table2="(SELECT
	a.id_inc AS id_inc,
	a.nama_lengkap AS nama_lengkap,
	a.no_telepon AS no_telepon,
	a.email AS email,
	a.username AS username,
	group_concat( c.nama_role SEPARATOR ',' ) AS role
FROM
	ap.ms_pengguna a
	JOIN ap.ms_assign_role b ON a.id_inc = b.ms_pengguna_id
	JOIN ap.ms_role c ON c.id_inc = b.ms_role_id
GROUP BY
	a.id_inc,
	a.nama_lengkap,
	a.no_telepon,
	a.email,
	a.username)s";

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->select("count(1) jum",false);

        $this->db->where("(id_inc like '%$q%' or   nama_lengkap like '%$q%' or  no_telepon like '%$q%' or  email like '%$q%' or  username like '%$q%' )",'',false);
        $ee=$this->db->get($this->table)->row();
        return $ee->jum;
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {

    $this->db->select("id_inc,nama_lengkap,no_telepon,email,username",false);
    $this->db->limit($limit, $start);
    $this->db->where("(id_inc like '%$q%' or   nama_lengkap like '%$q%' or  no_telepon like '%$q%' or  email like '%$q%' or  username like '%$q%' )",'',false);

    return $this->db->get($this->table)->result();


    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Pengguna_model.php */
/* Location: ./application/models/Pengguna_model.php */