<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); class Mauth extends CI_Model {

    function __construct()
    {
        parent::__construct();
         $this->table = 'ms_pengguna';
    }

    function proseslogin($data=array()){

        $username=$data['username'];
        $password=$data['password'];
        $query=$this->db->query("SELECT a.*,ms_role_id
                                    FROM ms_pengguna a
                                    JOIN ms_assign_role b ON a.id_inc=b.ms_pengguna_id
                                    JOIN ms_role c ON c.id_inc=b.ms_role_id
                                    WHERE username=? and password=?",array($username,$password))->row();



        if(!empty($query) ){

                $token=getToken(40);
                $this->db->set('token_login',$token);
                $this->db->set('last_login','now()',false);
                $this->db->set('ip_login',get_client_ip());
                $this->db->where('username',$username);
                $this->db->where('password',$password);

                $this->db->update('ms_pengguna');


                set_userdata('kuncisdmm',1);
                set_userdata('app_token',$token);
                set_userdata('app_id_pengguna',$query->id_inc);
                set_userdata('app_nama',$query->nama_lengkap);
                set_userdata('app_username',$query->username);
                set_userdata('app_role',$query->ms_role_id);

            // set_userdata($ses);
            return 'true';
        }else{
            return 'false';
        }
    }


}

