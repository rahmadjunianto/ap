<div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-body">
                <form action="<?php echo $action; ?>" method="post"  role="form">
                <div class="box-body">
                
	
                    <div class="form-group">
                    <label for="panel">Panel  <sup style="color:red;">*</sup></label>
                    <input type="text" value="<?php echo $panel; ?>" class="form-control" name="panel" id="panel" placeholder="Panel">
                    <?php echo form_error('panel') ?>
                </div>
	
                    <div class="form-group">
                    <label for="jenis_panel">Jenis Panel  <sup style="color:red;">*</sup></label>
                    <input type="text" value="<?php echo $jenis_panel; ?>" class="form-control" name="jenis_panel" id="jenis_panel" placeholder="Jenis Panel">
                    <?php echo form_error('jenis_panel') ?>
                </div>
	    </div><input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	              <div class="box-footer">
              <button type="submit" class="btn btn-primary btn-sm">Submit</button>
              <?= anchor('ms_panel','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
            </div>

                </form>
            </div>
        </div>
    </div>
    </div>