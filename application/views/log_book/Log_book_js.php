<script>
    var datetimepickerOption = {
        format: 'HH:ii:ss',
        language: 'id',
        defaultDate: new Date(),
        weekStart: 1,
        autoclose: 1,
        startView: 1,
    }

    $('#kontentdiag').on('click', '#addButton', function (e) {

        $('#kontentdiag').append(
            '<tr class="barisdiag">' +
            '    <td><input autocomplete="off" type="time" required class="form-control jam" name="jam[]"></td>' +
            '    <td><input type="text" required class="form-control" name="uraian[]"></td>' +
            '    <td><input type="text" required class="form-control" name="tindak_lanjut[]"></td>' +
            '    <td><input autocomplete="off" type="time" required class="form-control jam" name="selesai[]"></td>' +
            '    <td>' +
            '        <select name="hasil[]" class="form-control">' +
            '            <option value="">Pilih</option>' +
            '            <option value="1">Normal</option>' +
            '            <option value="2">Tindakan Lanjut</option>' +
            '        </select>' +
            '    </td>' +
            '    <td><input type="text" required class="form-control" name="ket[]"></td>' +
            '    <td><button title="Hapus" type="button"' +
            '            class="btn btn-xs btn-danger remove_project_file"><i class="fa fa-trash"></i></button></td></tr>'
        );
        // $('.jam').datetimepicker(datetimepickerOption);
    });

    $('#kontentdiag').on('click', '.remove_project_file', function (e) {
        e.preventDefault();
        $(this).parents(".barisdiag").remove();
    });
    $('#petugasList').on('click', '.hapusPetugas', function (e) {
        e.preventDefault();
        $(this).parents(".barisdiag").remove();
    });
    $(document).on('click', '#addButtonPetugas', function (e) {
        $('#myModal').modal('show');
    });
    $(document).on('click', '.pilih', function (e) {
        id = $(this).attr('data-id');
        nama = $(this).attr('data-nama');
        $('#petugasList').append(
            '<tr class="barisdiag">' +
            '    <td>' + nama + '</td>' +
            '<input type="hidden" name="petugas_id[]" value="' + id + '"  >' +
            '    <td><button title="Hapus Petugas" type="button"' +
            '            class="btn btn-xs btn-danger hapusPetugas"><i class="fa fa-trash"></i></button></td></tr>'
        );
        $('#myModal').modal('hide');
    });

    function validateForm() {
        // alert()
        if ($('.hapusPetugas').length == 0) {
            alert("Petugas Harus Diisi");
            return false;
        }
    }
    $('.jam').datetimepicker({
        format: 'HH:ii:ss',
        language: 'id',
        defaultDate: new Date(),
        weekStart: 1,
        autoclose: 1,
        startView: 1,
    }).on('changeDate', function (e) {});

    function jam() {

        $('.jam').datetimepicker({
            format: 'HH:ii:ss',
            language: 'id',
            defaultDate: new Date(),
            weekStart: 1,
            autoclose: 1,
            startView: 1,
        }).on('changeDate', function (e) {});
    }

</script>