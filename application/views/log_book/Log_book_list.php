<div class="row">
    <div class="col-md-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="<?php echo site_url('log_book'); ?>" method="get">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Tanggal</label>
                                    <input autocomplete="off" type="text" class="form-control datepicker"
                                        placeholder="Tanggal" name="tanggal" value="<?= $tanggal?>">
                                </div>
                            </div>
                        <div class="col-md-3" style="padding-top:25px">
                            <div class="form-group">
                                <label></label>
                                <?php  if ($q <> '' || $tanggal <>'') {   ?>
                                <a href="<?php echo site_url('log_book'); ?>" class="btn btn-warning"><i
                                        class="fa fa-refresh"></i> Reset</a>
                                <?php   } ?>

                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                            </div>
                        </div>
                    </form>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Shift</th>
                            <th>Gangguan Permanen</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody><?php
        foreach ($log_book_data as $log_book)
        {
            ?>
                        <tr>
                            <td align="center" width="80px"><?php echo ++$start ?></td>
                            <td><?php echo date_indo($log_book->tanggal) ?></td>
                            <td><?php echo $log_book->shift==1 ? "M" : "PS" ?></td>
                            <td><?php echo $log_book->gangguan_permanen ?></td>
                            <td style="text-align:center" width="100px">
                                <?php
                echo anchor(site_url('log_book/cetak/'.acak($log_book->id)),'<i class="fa fa-print"></i>','target="_blank" class="btn btn-sm btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cetak data"');
                if($akses['is_update']==1){
				echo anchor(site_url('log_book/update/'.acak($log_book->id)),'<i class="fa fa-edit"></i>','class="btn btn-sm btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"'); }
				 if($akses['is_delete']==1){echo anchor(site_url('log_book/delete/'.acak($log_book->id)),'<i class="fa fa-trash"></i>','class="btn btn-sm btn-danger btn-xs" onclick="javasciprt: return confirm(\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus data"'); }
				?>
                            </td>
                        </tr>
                        <?php
              }
              ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <span class="pull-left">
                    <button type="button" class="btn btn-block btn-success btn-sm">Record :
                        <?php echo $total_rows ?></button>
                </span>
                <?php echo $pagination ?>
            </div>
        </div>
    </div>
</div>