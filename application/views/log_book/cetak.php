<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Log Book</title>
</head>
<style>
    body{
        font-size: 10px
    }
    table td{
        padding:5px
    }
</style>
<body>
    <table width="100%">
        <tr>
            <td width="80%">BIDANG FASILITAS BANDARA</td>
            <td rowspan="3"><img src="<?= base_url()?>/assets/ap.png" alt=""></td>
        </tr>
        <tr>
            <td>DIVISI ENERGY & POWER SUPPLY</td>
        </tr>
        <tr>
            <td>DINAS GARDU INDUK</td>
        </tr>
    </table>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td width="80%"></td>
            <td>HARI</td>
            <td> : <?php $x=explode(",",longdate_indo($header->tanggal)); echo $x[0] ?></td>
        </tr>
        <tr>
            <td width="80%"></td>
            <td>TANGGAL</td>
            <td> : <?= date_indo($header->tanggal)?></td>
        </tr>
        <tr>
            <td width="80%" align="center" >LAPORAN HARIAN DINAS OPERASIONAL TEKNIS</td>
            <td>SHIFT</td>
            <td> : <?php echo $header->shift==1 ? "M":"PS" ?></td>
        </tr>
    </table>

    <table border="1" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <th>NO</th>
            <th>JAM</th>
            <th>URAIAN KEJADIAN</th>
            <th colspan="9">TINDAK LANJUT</th>
            <th>SELESAI</th>
            <th>HASIL</th>
            <th>KET</th>
        </tr>
        <?php $no=1; foreach ($detail as $rk) { ?>
        <tr>
            <td width="3%" align="center"><?= $no++?></td>
            <td width="7%" align="center"><?= $rk->jam?></td>
            <td width="20%"><?= $rk->uraian?></td>
            <td colspan="9"><?= $rk->tindak_lanjut?></td>
            <td width="7%" align="center"><?= $rk->selesai?></td>
            <td><?= $rk->hasil==1 ? "Normal": "Tindak Lanjut"?></td>
            <td><?= $rk->ket?></td>
        </tr>
        <?php } ?>
        <tr>
            <td align="center" colspan="3"> GANGGUAN PERMANEN</td>
            <td align="center" colspan="2"> PRESSURE GAS</td>
            <td align="center" colspan="3"> METERING</td>
            <td align="center" colspan="3"> CONTROL MONITORING</td>
            <td align="center" colspan="2"> PETUGAS</td>
            <td align="center" >PARAF</td>
            <td align="center" >ASMAN</td>
        </tr>

        <tr>
            <td colspan="3" rowspan="8"><?= $header->gangguan_permanen?></td>
            <td>INCOMING 1</td>
            <td><?= $header->pg_incoming_1==1 ? "Normal":"Abnormal"?></td>
            <td>INCOMING</td>
            <td align="center">1</td>
            <td align="center">2</td>
            <td align="center">SAS</td>
            <td colspan="2"><?= $header->cm_sas ? "Normal": "Abnormal"?></td>
            <td valign="top" colspan="2" rowspan="8">

            <?php $no=1; foreach ($petugas_list as $rk) { ?>
                            <?= $no++?>.
                            <?= nama_petugas2($rk->petugas_id)?><br>
                    <?php } ?>

            </td>
            <td valign="top" rowspan="8">
                <?php  $no=1;for ($i=0; $i < count($petugas_list); $i++) {
                    echo $no++.". <br>";
                }?>
            </td>
            <td rowspan="3"></td>
        </tr>
        <tr>
            <td>INCOMING 2</td>
            <td><?= $header->pg_incoming_2==1 ? "Normal":"Abnormal"?></td>
            <td>DAYA</td>
            <td align="center"><?= $header->m_daya_1?> MVA</td>
            <td align="center"><?= $header->m_daya_2?> MVA</td>
            <td align="center">RCU</td>
            <td colspan="2"><?= $header->cm_rcu==1 ? "Normal": "Abnormal"?></td>
        </tr>
        <tr>
            <td>OUTGOING 1</td>
            <td><?= $header->pg_outgoing_1==1 ? "Normal":"Abnormal"?></td>
            <td>ARUS</td>
            <td align="center"><?= $header->m_arus_1?> A</td>
            <td align="center"><?= $header->m_arus_2?> A</td>
            <td align="center" colspan="3"></td>
        </tr>
        <tr>
            <td>OUTGOING 2</td>
            <td><?= $header->pg_outgoing_2==1 ? "Normal":"Abnormal"?></td>
            <td>TEGANGAN</td>
            <td align="center"><?= $header->m_tegangan_1?> KV</td>
            <td align="center"><?= $header->m_tegangan_2?> KV</td>
            <td align="center" colspan="2">TEMPERATURE</td>
            <td>TAP</td>
            <td>MECD</td>
        </tr>
        <tr>
            <td>COUPLER 1</td>
            <td><?= $header->pg_coupler==1 ? "Normal":"Abnormal"?></td>
            <td>COS PHI</td>
            <td align="center"><?= $header->m_cos_phi_1?></td>
            <td align="center"><?= $header->m_cos_phi_2?></td>
            <td align="center"> TRAFO 1</td>
            <td><?= $header->temp_t1?> &#8451;</td>
            <td><?= $header->tap_t1?></td>
            <td rowspan="3"></td>
        </tr>
        <tr>
            <td>BUS VT</td>
            <td><?= $header->pg_coupler==1 ? "Normal":"Abnormal"?></td>
            <td>FREQUENSI</td>
            <td align="center"><?= $header->m_frequensi_1?> HZ</td>
            <td align="center"><?= $header->m_frequensi_2?> HZ</td>
            <td align="center"> TRAFO 2</td>
            <td><?= $header->temp_t1?> &#8451;</td>
            <td><?= $header->tap_t1?></td>
        </tr>
        <tr>
            <td>SPARE 1</td>
            <td><?= $header->pg_spare_1==1 ? "Normal":"Abnormal"?></td>
            <td colspan="6"></td>
        </tr>
    </table>

</body>
</html>