<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <form action="<?php echo $action; ?>" onsubmit="return validateForm()" method="post" role="form">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="tanggal">Tanggal <sup style="color:red;">*</sup></label>
                                <input  autocomplete="off" required type="text" value="<?php echo $tanggal; ?>"
                                    class="form-control datepicker" name="tanggal" id="tanggal" placeholder="Tanggal">
                                <?php echo form_error('tanggal') ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="shift">Shift <sup style="color:red;">*</sup></label>
                                <select required name="shift" id="shift" class="form-control">
                                    <option value="">Pilih Shift</option>
                                    <option <?php if($shift==1){ echo "selected";}?> value="1">M</option>
                                    <option <?php if($shift==2){ echo "selected";}?> value="2">PS</option>
                                </select>
                                <?php echo form_error('shift') ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="shift">Gangguan Permanen <sup style="color:red;">*</sup></label>
                                <textarea required name="gangguan_permanen" id="gangguan_permanen" class="form-control"
                                    cols="3" rows="2"><?php echo $gangguan_permanen; ?></textarea>
                                <?php echo form_error('gangguan_permanen') ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="res">
                            <table style="font-size:10px" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="100px">Jam</th>
                                        <th>Uraian kegiatan</th>
                                        <th>Tindak Lanjut</th>
                                        <th width="100px">Selesai</th>
                                        <th>Hasil</th>
                                        <th>Ket</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="kontentdiag">
                                    <?php if($id!=null) { ?>
                                    <?php foreach ($detail as $rk ) { ?>
                                    <tr class="barisdiag">
                                        <td><input style="font-size:10px" autocomplete="off" type="time" required="" class="form-control"
                                                name="jam[]" value="<?= $rk->jam?>"></td>
                                        <td><input style="font-size:10px" type="text" required="" class="form-control" name="uraian[]"
                                                value="<?= $rk->uraian?>" </td> <td><input style="font-size:10px" type="text" required=""
                                                class="form-control" name="tindak_lanjut[]"
                                                value="<?= $rk->tindak_lanjut?>">
                                        </td>
                                        <td><input style="font-size:10px" autocomplete="off" type="time" required="" class="form-control"
                                                name="selesai[]" value="<?= $rk->selesai?>"></td>
                                        <td> <select name="hasil[]" class="form-control">
                                                <option value="">Pilih Hasil</option>
                                                <option <?php if($rk->hasil==1){ echo "selected";}?> value="1">Normal
                                                </option>
                                                <option <?php if($rk->hasil==2){ echo "selected";}?> value="2">Tindakan
                                                    Lanjut</option>
                                            </select> </td>
                                        <td><input style="font-size:10px" type="text" required="" class="form-control" name="ket[]"
                                                value="<?= $rk->ket?>"></td>
                                        <td><button title="Hapus" type="button"
                                                class="btn btn-xs btn-danger remove_project_file"><i
                                                    class="fa fa-trash"></i></button></td>
                                    </tr>
                                    <?php } ?>
                                    <?php } else { ?>
                                    <tr>
                                        <td><input style="font-size:10px" autocomplete="off" type="time" required class="form-control"
                                                name="jam[]"></td>
                                        <td><input style="font-size:10px" type="text" required class="form-control" name="uraian[]"></td>
                                        <td><input style="font-size:10px" type="text" required class="form-control" name="tindak_lanjut[]">
                                        </td>
                                        <td><input style="font-size:10px" autocomplete="off" type="time" required class="form-control"
                                                name="selesai[]"></td>
                                        <td>
                                            <select required name="hasil[]" class="form-control">
                                                <option value="">Pilih</option>
                                                <option value="1">Normal</option>
                                                <option value="2">Tindakan Lanjut</option>
                                            </select>
                                        </td>
                                        <td><input style="font-size:10px" type="text" required class="form-control" name="ket[]"></td>
                                        <td><button title="Tambah" id='addButton' type="button"
                                                class="btn btn-xs btn-success"><i class="fa fa-plus"></i></button></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <table style="font-size:10px" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="text-center">PRESSURE GAS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>INCOMING 1</th>
                                        <td>
                                            <select required name="pg_incoming_1" class="form-control">
                                                <option value="">Pilih</option>
                                                <option <?php if($pg_incoming_1==1){ echo "selected";}?> value="1">
                                                    Normal</option>
                                                <option <?php if($pg_incoming_1==2){ echo "selected";}?> value="2">
                                                    Abnormal</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>INCOMING 2</th>
                                        <td>
                                            <select required name="pg_incoming_2" class="form-control">
                                                <option value="">Pilih</option>
                                                <option <?php if($pg_incoming_2==1){ echo "selected";}?> value="1">
                                                    Normal</option>
                                                <option <?php if($pg_incoming_2==2){ echo "selected";}?> value="2">
                                                    Abnormal</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>OUTGOING 1</th>
                                        <td>
                                            <select required name="pg_outgoing_1" class="form-control">
                                                <option value="">Pilih</option>
                                                <option <?php if($pg_outgoing_1==1){ echo "selected";}?> value="1">
                                                    Normal</option>
                                                <option <?php if($pg_outgoing_1==2){ echo "selected";}?> value="2">
                                                    Abnormal</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>OUTGOING 2</th>
                                        <td>
                                            <select required name="pg_outgoing_2" class="form-control">
                                                <option value="">Pilih</option>
                                                <option <?php if($pg_outgoing_2==1){ echo "selected";}?> value="1">
                                                    Normal</option>
                                                <option <?php if($pg_outgoing_2==2){ echo "selected";}?> value="2">
                                                    Abnormal</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>COUPLER</th>
                                        <td>
                                            <select required name="pg_coupler" class="form-control">
                                                <option value="">Pilih</option>
                                                <option <?php if($pg_coupler==1){ echo "selected";}?> value="1">Normal
                                                </option>
                                                <option <?php if($pg_coupler==2){ echo "selected";}?> value="2">Abnormal
                                                </option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>BUS VT</th>
                                        <td>
                                            <select required name="pg_bust_vt" class="form-control">
                                                <option value="">Pilih</option>
                                                <option <?php if($pg_bust_vt==1){ echo "selected";}?> value="1">Normal
                                                </option>
                                                <option <?php if($pg_bust_vt==2){ echo "selected";}?> value="2">Abnormal
                                                </option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>SPARE 1</th>
                                        <td>
                                            <select required name="pg_spare_1" class="form-control">
                                                <option value="">Pilih</option>
                                                <option <?php if($pg_spare_1==1){ echo "selected";}?> value="1">Normal
                                                </option>
                                                <option <?php if($pg_spare_1==2){ echo "selected";}?> value="2">Abnormal
                                                </option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <table style="font-size:10px" class="table table-bordered">
                                <tr>
                                    <th colspan="3" class="text-center">METERING</th>
                                </tr>
                                <tr>
                                    <th>INCOMING</th>
                                    <th class="text-center">1</th>
                                    <th class="text-center">2</th>
                                </tr>
                                <tr>
                                    <th>DAYA</th>
                                    <th>
                                        <div class="input-group">
                                            <input style="font-size:10px" required class="form-control number" type="text" name="m_daya_1"
                                                value="<?= $m_daya_1?>">
                                            <span style="padding:2px;font-size:10px" class="input-group-addon">MVA</span>
                                        </div>
                                        </th>
                                    <th>
                                        <div class="input-group">
                                            <input style="font-size:10px" required class="form-control number" type="text" name="m_daya_2"
                                                value="<?= $m_daya_2?>">
                                            <span style="padding:2px;font-size:10px" class="input-group-addon">MVA</span>
                                        </div></th>
                                </tr>
                                <tr>
                                    <th>ARUS</th>
                                    <th>
                                        <div class="input-group">
                                            <input style="font-size:10px" required class="form-control number" type="text" name="m_arus_1"
                                                value="<?= $m_arus_1?>">
                                            <span style="padding:2px;font-size:10px" class="input-group-addon">A</span>
                                        </div></th>
                                    <th>
                                        <div class="input-group">
                                            <input style="font-size:10px" required class="form-control number" type="text" name="m_arus_2"
                                                value="<?= $m_arus_2?>">
                                            <span style="padding:2px;font-size:10px" class="input-group-addon">A</span>
                                        </div></th>
                                </tr>
                                <tr>
                                    <th>TEGANGAN</th>
                                    <th>
                                        <div class="input-group">
                                            <input style="font-size:10px" required class="form-control number" type="text" name="m_tegangan_1"
                                                value="<?= $m_tegangan_1?>">
                                            <span style="padding:2px;font-size:10px" class="input-group-addon">KV</span>
                                        </div></th>
                                    <th>
                                        <div class="input-group">
                                            <input style="font-size:10px" required class="form-control number" type="text" name="m_tegangan_2"
                                                value="<?= $m_tegangan_2?>">
                                            <span style="padding:2px;font-size:10px" class="input-group-addon">KV</span>
                                        </div></th>
                                </tr>
                                <tr>
                                    <th>COS PHI</th>
                                    <th><input style="font-size:10px" required class="form-control number" type="text" name="m_cos_phi_1"
                                            value="<?= $m_cos_phi_1?>"></th>
                                    <th><input style="font-size:10px" required class="form-control number" type="text" name="m_cos_phi_2"
                                            value="<?= $m_cos_phi_2?>"></th>
                                </tr>
                                <tr>
                                    <th>FREQUENSI</th>
                                    <th>
                                        <div class="input-group">
                                            <input style="font-size:10px" required class="form-control number" type="text" name="m_frequensi_1"
                                                value="<?= $m_frequensi_1?>">
                                            <span style="padding:2px;font-size:10px" class="input-group-addon">HZ</span>
                                        </div></th>
                                    <th>
                                        <div class="input-group">
                                            <input style="font-size:10px" required class="form-control number" type="text" name="m_frequensi_2"
                                                value="<?= $m_frequensi_2?>">
                                            <span style="padding:2px;font-size:10px" class="input-group-addon">HZ</span>
                                        </div></th>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <table style="font-size:10px" class="table table-bordered">
                                <tr>
                                    <th colspan="3">CONTROL MONITORING</td>
                                </tr>
                                <tr>
                                    <th>SAS</td>
                                    <td colspan="2">
                                        <select required name="cm_sas" class="form-control">
                                            <option value="">Pilih</option>
                                            <option <?php if($cm_sas==1){ echo "selected";}?> value="1">Normal</option>
                                            <option <?php if($cm_sas==2){ echo "selected";}?> value="2">Abnormal
                                            </option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>RCU</td>
                                    <td colspan="2">
                                        <select required name="cm_rcu" class="form-control">
                                            <option value="">Pilih</option>
                                            <option <?php if($cm_rcu==1){ echo "selected";}?> value="1">Normal</option>
                                            <option <?php if($cm_rcu==2){ echo "selected";}?> value="2">Abnormal
                                            </option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center" colspan="2">TEMPERATUR</td>
                                    <th>TAP</th>
                                </tr>
                                <tr>
                                    <th nowrap>TRAFO 1</td>
                                    <td>
                                        <!-- <input style="font-size:10px" required class="form-control number"
                                            type="text" name="temp_t1" value="<?= $temp_t1?>"> -->
                                        <div class="input-group">
                                            <input style="font-size:10px" required class="form-control number" type="text" name="temp_t1"
                                                value="<?= $temp_t1?>">
                                            <span style="padding:0px" class="input-group-addon">&#8451;</span>
                                        </div>
                                    </td>
                                    <td>
                                        <input style="font-size:10px" required class="form-control" type="text" name="tap_t1"
                                            value="<?= $tap_t1?>">
                                    </td>
                                </tr>
                                <tr>
                                    <th nowrap>TRAFO 2</td>
                                    <td>
                                        <div class="input-group">
                                            <input style="font-size:10px" required class="form-control number" type="text" name="temp_t2"
                                                value="<?= $temp_t2?>">
                                            <span style="padding:0px" class="input-group-addon">&#8451;</span>
                                        </div>
                                    </td>
                                    <td>
                                        <input style="font-size:10px" required class="form-control" type="text" name="tap_t2"
                                            value="<?= $tap_t1?>">
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <table style="font-size:10px" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Petugas</th>
                                        <th width="10%"><button title="Tambah Petugas" id='addButtonPetugas'
                                                type="button" class="btn btn-xs btn-success"><i
                                                    class="fa fa-plus"></i></button></th>
                                    </tr>
                                </thead>
                                <tbody id="petugasList">
                                    <?php foreach ($petugas_list as $rk) { ?>
                                    <tr class="barisdiag">
                                        <td><?= nama_petugas2($rk->petugas_id)?></td><input style="font-size:10px" type="hidden"
                                            name="petugas_id[]" value="<?= $rk->petugas_id?>">
                                        <td><button title="Hapus Petugas" type="button"
                                                class="btn btn-xs btn-danger hapusPetugas"><i
                                                    class="fa fa-trash"></i></button></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <input style="font-size:10px" class="form-control" type="hidden" name="id" value="<?php echo $id; ?>" />
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        <?= anchor('log_book','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">List Petugas</h4>
            </div>
            <div class="modal-body">
                <table id="example" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="width:70px ">Nama</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                    foreach ($petugas as $rk)
                    {
                        ?>
                        <tr style="cursor: pointer;" class="pilih" data-id="<?php echo $rk->id ?>"
                            data-nama="<?php echo $rk->nickname ?>">
                            <td><?php echo $rk->nickname ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<style>
    .res {

        width: auto;
        overflow-x: auto;
        white-space: nowrap;
    }
</style>