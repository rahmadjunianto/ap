<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <form action="<?php echo $action; ?>" method="post" role="form">
                    <div class="box-body">


                        <div class="form-group">
                            <label for="nip">NIP <sup style="color:red;">*</sup></label>
                            <input type="text" readonly value="<?php echo $nip; ?>" class="form-control carinip"
                                name="nip" id="nip" placeholder="NIP">
                            <?php echo form_error('nip') ?>
                        </div>

                        <div class="form-group">
                            <label for="nama_lengkap">Nama Pegawai <sup style="color:red;">*</sup></label>
                            <input type="text" value="<?php echo $nama_lengkap; ?>" class="form-control"
                                name="nama_lengkap" id="nama_lengkap" placeholder="Nama Pegawai">
                            <?php echo form_error('nama_lengkap') ?>
                        </div>
                        <?php if($nambah==true){?>
                        <div class="form-group">
                            <label for="username">Username <sup style="color:red;">*</sup></label>
                            <input type="text" value="<?php echo $username; ?>" class="form-control" name="username"
                                id="username" placeholder="Username">
                            <?php echo form_error('username') ?>
                        </div>
                        <div class="form-group">
                            <label for="password">Password <sup style="color:red;">*</sup></label>
                            <input type="text" value="<?php echo $password; ?>" class="form-control" name="password"
                                id="password" placeholder="Password">
                            <?php echo form_error('password') ?>
                        </div>

                        <?php } ?>
                        <div class="form-group">
                            <label for="no_telepon">No Telepon <sup style="color:red;">*</sup></label>
                            <input type="text" value="<?php echo $no_telepon; ?>" class="form-control" name="no_telepon"
                                id="no_telepon" placeholder="No Telepon">
                            <?php echo form_error('no_telepon') ?>
                        </div>
                        <div class="form-group">
                            <label for="email">Email <sup style="color:red;">*</sup></label>
                            <input type="text" value="<?php echo $email; ?>" class="form-control" name="email"
                                id="email" placeholder="Email">
                            <?php echo form_error('email') ?>
                        </div>
                        <div class="form-group">
                            <label for="email">Role <sup style="color:red;">*</sup></label>
                            <br>
                            <?php $a=1; foreach($role as $role){?>
                            <div class="checkbox">
                                <label><input type="checkbox"
                                        <?php if(in_array($role->id_inc, $assign) ){echo "checked";}?>
                                        value="<?= $role->id_inc ?>" name="ms_role_id[]"><?= $role->nama_role ?></label>
                            </div>

                            <?php $a++; } ?>
                        </div>
                    </div><input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" />
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        <?= anchor('pengguna','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">List Pegawai</h4>
            </div>
            <div class="modal-body">
                <table id="lookup" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID Petugas</th>
                            <th>Nama</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($pegawai as $pegawai)
                            {
                                ?>
                        <tr class="pilih" data-nip="<?php echo $pegawai->nip ?>"
                            data-nama_pegawai="<?php echo $pegawai->nm_pegawai ?>">
                            <td><?php echo $pegawai->nip ?></td>
                            <td><?php echo $pegawai->nm_pegawai ?></td>
                        </tr>
                        <?php
                            }
                            ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>