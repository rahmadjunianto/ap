
<script>

function readURL(input) {

if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
    $('#blah').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp").change(function() {
    readURL(this);
    $('.photo').show();
    $('#blah').show();
});
<?php if (empty($foto)) { ?>

$('.photo').hide();
$('#blah').hide();
<?php } ?>
</script>