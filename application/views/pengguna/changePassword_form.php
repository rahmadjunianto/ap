<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <form action="<?php echo $action; ?>" method="post" role="form">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="nama_lengkap">Password Lama <sup style="color:red;">*</sup></label>
                                    <input type="text" required value="<?= $lama?>" class="form-control" name="lama"
                                        id="nama_lengkap" placeholder="Password Lama">
                                    <?php echo form_error('lama') ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="nama_lengkap">Password Baru <sup style="color:red;">*</sup></label>
                                    <input type="text" required value="<?= $baru?>" class="form-control" name="baru"
                                        id="nama_lengkap" placeholder="Password Baru">
                                    <?php echo form_error('baru') ?>
                                </div>
                            </div>
                        </div>
                    </div><input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" />
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        <?= anchor('pengguna','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">List Pegawai</h4>
            </div>
            <div class="modal-body">
                <table id="lookup" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID Petugas</th>
                            <th>Nama</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($pegawai as $pegawai)
                            {
                                ?>
                        <tr class="pilih" data-nip="<?php echo $pegawai->nip ?>"
                            data-nama_pegawai="<?php echo $pegawai->nm_pegawai ?>">
                            <td><?php echo $pegawai->nip ?></td>
                            <td><?php echo $pegawai->nm_pegawai ?></td>
                        </tr>
                        <?php
                            }
                            ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>