<div class="row">
    <div class="col-sm-6">
        <div class="box">
            <div class="box-body">
                <form action="<?php echo $action; ?>" method="post" role="form" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="nama_lengkap">Foto <sup style="color:red;">*</sup></label>
                                    <input id="imgInp"  type='file' id="foto" name="foto" accept="image/*">
                                </div>
                            </div>
                        </div>
                    </div><input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" />
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-6 photo">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <img id="blah" src="<?= base_url().$foto ?>" alt="your image" width="500px" height="500px" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<style type="text/css">
/** {
    margin:0;
    padding:0;
    font-family: tahoma;
}*/
/*body {
    padding: 30px;
}*/
.div {
    width: 300px;
    height: 25px;
    background-color: white;
    box-shadow: 1px 2px 3px #ededed;
    position:relative;
    border: 1px solid #d8d8d8;
}
input[type='file'] {
    width:auto;
    height:25px;
    opacity:1
}

.button {
    cursor: pointer;
    display: block;
    width: 20px;
    background-color: purple;
    height:25px;
    color: white;
    position: absolute;
    right:0;
    top: 0;
    font-size: 11px;
    line-height:25px;
    text-align: center;
    -webkit-transition: 500ms all;
    -moz-transition: 500ms all;
    transition: 500ms all;
}

.button:hover {
    background-color: blue;
}
</style>