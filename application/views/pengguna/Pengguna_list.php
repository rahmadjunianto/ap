<div class="row">
    <div class="col-md-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="<?php echo site_url('pengguna'); ?>" method="get">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Pencarian</label>
                                <input type="text" class="form-control" placeholder="Pencarian" name="q"
                                    value="<?= $q?>">
                            </div>
                        </div>
                        <div class="col-md-3" style="padding-top:25px">
                            <div class="form-group">
                                <label></label>
                                <?php  if ($q <> '') {   ?>
                                <a href="<?php echo site_url('pengguna'); ?>" class="btn btn-warning"><i
                                        class="fa fa-refresh"></i> Reset</a>
                                <?php   } ?>

                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                            </div>
                        </div>
                    </form>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>No Telepon</th>
                            <th>Email</th>
                            <th>Username</th>
                            <!-- <th>Role</th> -->
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($pengguna_data as $pengguna)
                            {
                                ?>
                        <tr>
                            <td align="center" width="80px"><?php echo ++$start ?></td>
                            <td><?php echo $pengguna->nama_lengkap ?></td>
                            <td><?php echo $pengguna->no_telepon ?></td>
                            <td><?php echo $pengguna->email ?></td>
                            <td><?php echo $pengguna->username ?></td>
                            <!-- <td><?php echo $pengguna->role?></td> -->
                            <td style="text-align:center" width="100px">
                                <a href="#" class="badge badge-warning reset" data-id="<?=acak($pengguna->id_inc)?>"
                                    data-nama_lengkap="<?=$pengguna->nama_lengkap?>"
                                    data-no_telepon="<?=$pengguna->no_telepon?>" data-email="<?=$pengguna->email?>"
                                    data-username="<?=$pengguna->username?>"
                                    data-toggle="tooltip" data-placement="top" title=""
                                    data-original-title="Reset Password" aria-describedby="tooltip430364"><i
                                        class="fa fa-refresh"></i></a>
                                <?php if($akses['is_update']==1){
                        				echo anchor(site_url('pengguna/update/'.acak($pengguna->id_inc)),'<i class="fa fa-edit"></i>','class="badge badge-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"'); }
                        				 if($akses['is_delete']==1){echo anchor(site_url('pengguna/delete/'.acak($pengguna->id_inc)),'<i class="fa fa-trash"></i>','class="badge badge-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus data"'); }
                        				?>
                            </td>
                        </tr>
                        <?php
                            }
                            ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <span class="pull-left">
                    <button type="button" class="btn btn-block btn-success btn-sm">Record :
                        <?php echo $total_rows ?></button>
                </span>
                <?php echo $pagination ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Reset Password</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="<?php echo site_url('pengguna/reset') ?>" method="post" class="form-horizontal"
                            role="form">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="password" class="col-3 col-form-label">Nama Lengkap <sup
                                            style="color:red;">*</sup></label>
                                    <div class="col-8">
                                        <input type="text" readonly class="form-control" name="nama_lengkap"
                                            id="nama_lengkap" placeholder="Password" value="">
                                    </div>
                                    <input type="hidden" name="id" id="id" value="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="password" class="col-3 col-form-label">No Telepon <sup
                                            style="color:red;">*</sup></label>
                                    <div class="col-3">
                                        <input type="text" readonly class="form-control" name="no_telepon"
                                            id="no_telepon" placeholder="Password" value="">
                                    </div>
                                    <input type="hidden" name="id" id="id" value="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="password" class="col-3 col-form-label">Email <sup
                                            style="color:red;">*</sup></label>
                                    <div class="col-9">
                                        <input type="text" readonly class="form-control" name="email" id="email"
                                            placeholder="Password" value="">
                                    </div>
                                    <input type="hidden" name="id" id="id" value="">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="password" class="col-3 col-form-label">Username <sup
                                            style="color:red;">*</sup></label>
                                    <div class="col-9">
                                        <input type="text" readonly class="form-control" name="username" id="username"
                                            placeholder="Password" value="">
                                    </div>
                                    <input type="hidden" name="id" id="id" value="">
                                </div>
                            </div>
                            <!-- <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="password" class="col-3 col-form-label"> <sup
                                            style="color:red;">*</sup></label>
                                    <div class="col-9">
                                        <input type="text" readonly class="form-control" name="role" id="role"
                                            placeholder="Password" value="">
                                    </div>
                                    <input type="hidden" name="id" id="id" value="">
                                </div>
                            </div> -->

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="password" class="col-3 col-form-label">Password <sup
                                            style="color:red;">*</sup></label>
                                    <div class="col-9">
                                        <input type="password" class="form-control" name="password" id="password"
                                            placeholder="Password" value="" required>
                                        <?php echo form_error('password') ?>
                                    </div>
                                    <input type="hidden" name="id" id="idx" value="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group mb-0 justify-content-end">
                                    <label for="password" class="col-3 col-form-label"></label>
                                    <div class="col-9">
                                        <button type="submit" class="btn btn-success waves-effect waves-light"><i
                                                class="fa fa-save"></i> Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>