<div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-body">
                <form action="<?php echo $action; ?>" method="post"  role="form">
                <div class="box-body">


                    <div class="form-group">
                    <label for="nip">NIP  <sup style="color:red;">*</sup></label>
                    <input type="text" value="<?php echo $nip; ?>" class="form-control" name="nip" id="nip" placeholder="NIP">
                    <?php echo form_error('nip') ?>
                </div>
                    <div class="form-group">
                    <label for="nm_pegawai">Jabatan Pegawai  <sup style="color:red;">*</sup></label>
                    <input type="text" value="<?php echo $jabatan; ?>" class="form-control" name="jabatan" id="jabatan" placeholder="Jabatan Pegawai">
                    <?php echo form_error('jabatan') ?>
                </div>

                    <div class="form-group">
                    <label for="nm_pegawai">Nama Pegawai  <sup style="color:red;">*</sup></label>
                    <input type="text" value="<?php echo $nm_pegawai; ?>" class="form-control" name="nm_pegawai" id="nm_pegawai" placeholder="Nama Pegawai">
                    <?php echo form_error('nm_pegawai') ?>
                </div>
	    </div><input type="hidden" name="kd_pegawai" value="<?php echo $kd_pegawai; ?>" />
	              <div class="box-footer">
              <button type="submit" class="btn btn-primary btn-sm">Submit</button>
              <?= anchor('pegawai','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
            </div>

                </form>
            </div>
        </div>
    </div>
    </div>