<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <form action="<?php echo $action; ?>" method="post" role="form">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="nip">NIP <sup style="color:red;">*</sup></label>
                            <input type="text" readonly value="<?php echo $nip; ?>" class="form-control carinip"
                                name="nip" id="nip" placeholder="NIP">
                            <?php echo form_error('nip') ?>
                        </div>

                        <div class="form-group">
                            <label for="nama">Nama <sup style="color:red;">*</sup></label>
                            <input readonly type="text" value="<?php echo $nama; ?>" class="form-control" name="nama" id="nama"
                                placeholder="Nama">
                            <?php echo form_error('nama') ?>
                        </div>
                        <div class="form-group">
                            <label for="nama">Nickname <sup style="color:red;">*</sup></label>
                            <input type="text" value="<?php echo $nickname; ?>" class="form-control" name="nickname" id="nickname"
                                placeholder="Nickname">
                            <?php echo form_error('nickname') ?>
                        </div>
                    </div><input type="hidden" name="id" value="<?php echo $id; ?>" />
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        <?= anchor('petugas','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">List Pegawai</h4>
            </div>
            <div class="modal-body">
                <table id="lookup" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID Petugas</th>
                            <th>Nama</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($pegawai as $pegawai)
                            {
                                ?>
                        <tr class="pilih" data-nip="<?php echo $pegawai->nip ?>"
                            data-nama_pegawai="<?php echo $pegawai->nm_pegawai ?>">
                            <td><?php echo $pegawai->nip ?></td>
                            <td><?php echo $pegawai->nm_pegawai ?></td>
                        </tr>
                        <?php
                            }
                            ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>