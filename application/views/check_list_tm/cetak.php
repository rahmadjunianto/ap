<!DOCTYPE html>
<html>

<head>
    <title>FORM CHECKLIST HARIAN TM 20Kv BSH </title>
</head>
<style>
    .left {
        margin-left: 20px
    }
</style>

<body>
    <div style="font-size:10px">
    BIDANG FASILITAS BANDARA <br>
    DIVISI ENERGY & POWER SUPPLY <br>
    DINAS GARDU INDUK <br>

    Tanggal : <?=$header->tanggal?> <br>
    Pukul : <?= $header->pukul?><br>
    </div>
    <div class="res">
        <table border="1" cellspacing="0" cellpadding="0" width="100%">
            <thead>
                <tr>
                    <th style="background-color: burlywood" class="text-center">D</th>
                    <th colspan="2" style="background-color: burlywood" class="text-center">PANEL TM 20KV</th>
                    <th width="4.5%" class="text-center th">XCA</th>
                    <th width="4.5%" class="text-center th">XCB</th>
                    <th width="4.5%" class="text-center th">XCC</th>
                    <th width="4.5%" class="text-center th">MSA</th>
                    <th width="4.5%" class="text-center th">MSB</th>
                    <th width="4.5%" class="text-center th">MSC</th>
                    <th width="4.5%" class="text-center th">MSD</th>
                    <th width="4.5%" class="text-center th">MSE</th>
                    <th width="4.5%" class="text-center th">MSF</th>
                    <th width="4.5%" class="text-center th">MSG</th>
                    <th width="4.5%" class="text-center th">MSH</th>
                    <th width="4.5%" class="text-center th">MSI</th>
                    <th width="4.5%" class="text-center th">MSJ</th>
                    <th width="4.5%" class="text-center th">MSK</th>
                    <th width="4.5%" class="text-center th">MSL</th>
                    <th width="4.5%" class="text-center th">MSM</th>
                    <th width="4.5%" class="text-center th">MSN</th>
                    <th width="4.5%" class="text-center th">MSO</th>
                    <th width="4.5%" class="text-center th">MSP</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="22" height="20"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="text-center">1</td>
                    <td rowspan="2">CB</td>
                    <td>OPEN</td>

                    <td align="center">
                        <?php if($header->xca_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcb_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcc_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msa_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msb_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msc_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msd_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mse_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msf_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msg_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msh_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msi_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msj_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msk_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msl_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msm_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msn_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mso_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msp_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                </tr>
                <tr>
                    <td>CLOSE</td>
                    <td align="center">
                        <?php if($header->xca_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcb_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcc_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msa_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msb_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msc_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msd_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mse_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msf_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msg_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msh_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msi_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msj_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msk_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msl_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msm_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msn_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mso_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msp_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                </tr>               <tr>
                    <td colspan="22" height="20"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="text-center">2</td>
                    <td rowspan="2">DS <br> GROUNDING</td>
                    <td>OPEN</td>

                    <td align="center">
                        <?php if($header->xca_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcb_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcc_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msa_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msb_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msc_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msd_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mse_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msf_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msg_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msh_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msi_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msj_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msk_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msl_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msm_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msn_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mso_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msp_ds_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                </tr>
                <tr>
                    <td>CLOSE</td>
                    <td align="center">
                        <?php if($header->xca_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcb_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcc_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msa_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msb_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msc_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msd_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mse_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msf_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msg_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msh_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msi_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msj_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msk_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msl_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msm_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msn_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mso_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msp_ds_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                </tr>
                <tr>
                    <td colspan="22" height="20"></td>
                </tr> <tr>
                    <td rowspan="2" class="text-center">3</td>
                    <td rowspan="2"> GROUNDING</td>
                    <td>OPEN</td>

                    <td align="center">
                        <?php if($header->xca_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcb_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcc_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msa_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msb_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msc_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msd_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mse_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msf_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msg_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msh_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msi_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msj_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msk_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msl_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msm_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msn_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mso_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msp_gr_open==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                </tr>
                <tr>
                    <td>CLOSE</td>
                    <td align="center">
                        <?php if($header->xca_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcb_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcc_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msa_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msb_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msc_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msd_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mse_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msf_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msg_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msh_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msi_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msj_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msk_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msl_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msm_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msn_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mso_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msp_gr_close==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                </tr>
                <tr>
                    <td colspan="22" height="20"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="text-center">4</td>
                    <td rowspan="2">SPRING</td>
                    <td>CHARGE</td>
                    <td align="center">
                        <?php if($header->xca_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcb_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcc_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msa_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msb_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msc_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msd_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mse_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msf_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msg_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msh_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msi_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msj_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msk_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msl_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msm_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msn_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mso_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msp_charge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                </tr>
                <tr>
                    <td>DISCHARGE</td>
                    <td align="center">
                        <?php if($header->xca_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->xcb_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->xcc_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msa_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msb_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msc_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msd_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->mse_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msf_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msg_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msh_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msi_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msj_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msk_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msl_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msm_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msn_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->mso_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msp_discharge==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                </tr>
                <tr>
                    <td colspan="22" height="20"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="text-center">5</td>
                    <td rowspan="2">PROTEKSI</td>
                    <td>NORMAL</td>
                    <td align="center">
                        <?php if($header->xca_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcb_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->xcc_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msa_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msb_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msc_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msd_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mse_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msf_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msg_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msh_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msi_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msj_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msk_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msl_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msm_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msn_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->mso_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    <td align="center">
                        <?php if($header->msp_normal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                </tr>
                <tr>
                    <td>ABNORMAL</td>
                    <td align="center">
                        <?php if($header->xca_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->xcb_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->xcc_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msa_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msb_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msc_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msd_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->mse_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msf_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msg_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msh_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msi_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msj_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msk_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msl_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msm_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msn_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->mso_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                    <td align="center">
                        <?php if($header->msp_abnormal==1) { echo '<img src="assets/check.png" alt="" width="10px">';}?>
                    </td>
                    </td>
                </tr>
                <tr>
                    <td colspan="22" height="20"></td>
                </tr>
                <tr>
                    <td rowspan="4" class="text-center">6</td>
                    <td rowspan="4">METERING</td>
                    <td>ARUS (A)</td>
                    <td align="center"><?= $header->xca_arus?></td>
                    <td align="center"><?= $header->xcb_arus?></td>
                    <td align="center"><?= $header->xcc_arus?></td>
                    <td align="center"><?= $header->msa_arus?></td>
                    <td align="center"><?= $header->msb_arus?></td>
                    <td align="center"><?= $header->msc_arus?></td>
                    <td align="center"><?= $header->msd_arus?></td>
                    <td align="center"><?= $header->mse_arus?></td>
                    <td align="center"><?= $header->msf_arus?></td>
                    <td align="center"><?= $header->msg_arus?></td>
                    <td align="center"><?= $header->msh_arus?></td>
                    <td align="center"><?= $header->msi_arus?></td>
                    <td align="center"><?= $header->msj_arus?></td>
                    <td align="center"><?= $header->msk_arus?></td>
                    <td align="center"><?= $header->msl_arus?></td>
                    <td align="center"><?= $header->msm_arus?></td>
                    <td align="center"><?= $header->msn_arus?></td>
                    <td align="center"><?= $header->mso_arus?></td>
                    <td align="center"><?= $header->msp_arus?></td>
                </tr>
                <tr>
                    <td>TEGANGAN (KV)</td>
                    <td align="center"><?= $header->xca_tegangan?></td>
                    <td align="center"><?= $header->xcb_tegangan?></td>
                    <td align="center"><?= $header->xcc_tegangan?></td>
                    <td align="center"><?= $header->msa_tegangan?></td>
                    <td align="center"><?= $header->msb_tegangan?></td>
                    <td align="center"><?= $header->msc_tegangan?></td>
                    <td align="center"><?= $header->msd_tegangan?></td>
                    <td align="center"><?= $header->mse_tegangan?></td>
                    <td align="center"><?= $header->msf_tegangan?></td>
                    <td align="center"><?= $header->msg_tegangan?></td>
                    <td align="center"><?= $header->msh_tegangan?></td>
                    <td align="center"><?= $header->msi_tegangan?></td>
                    <td align="center"><?= $header->msj_tegangan?></td>
                    <td align="center"><?= $header->msk_tegangan?></td>
                    <td align="center"><?= $header->msl_tegangan?></td>
                    <td align="center"><?= $header->msm_tegangan?></td>
                    <td align="center"><?= $header->msn_tegangan?></td>
                    <td align="center"><?= $header->mso_tegangan?></td>
                    <td align="center"><?= $header->msp_tegangan?></td>
                </tr>
                <tr>
                    <td>DAYA (MVA)</td>
                    <td align="center"><?= $header->xca_daya?></td>
                    <td align="center"><?= $header->xcb_daya?></td>
                    <td align="center"><?= $header->xcc_daya?></td>
                    <td align="center"><?= $header->msa_daya?></td>
                    <td align="center"><?= $header->msb_daya?></td>
                    <td align="center"><?= $header->msc_daya?></td>
                    <td align="center"><?= $header->msd_daya?></td>
                    <td align="center"><?= $header->mse_daya?></td>
                    <td align="center"><?= $header->msf_daya?></td>
                    <td align="center"><?= $header->msg_daya?></td>
                    <td align="center"><?= $header->msh_daya?></td>
                    <td align="center"><?= $header->msi_daya?></td>
                    <td align="center"><?= $header->msj_daya?></td>
                    <td align="center"><?= $header->msk_daya?></td>
                    <td align="center"><?= $header->msl_daya?></td>
                    <td align="center"><?= $header->msm_daya?></td>
                    <td align="center"><?= $header->msn_daya?></td>
                    <td align="center"><?= $header->mso_daya?></td>
                    <td align="center"><?= $header->msp_daya?></td>
                </tr>
                <tr>
                    <td>FREKUENSI (HZ)</td>
                    <td align="center"><?= $header->xca_frekuensi?></td>
                    <td align="center"><?= $header->xcb_frekuensi?></td>
                    <td align="center"><?= $header->xcc_frekuensi?></td>
                    <td align="center"><?= $header->msa_frekuensi?></td>
                    <td align="center"><?= $header->msb_frekuensi?></td>
                    <td align="center"><?= $header->msc_frekuensi?></td>
                    <td align="center"><?= $header->msd_frekuensi?></td>
                    <td align="center"><?= $header->mse_frekuensi?></td>
                    <td align="center"><?= $header->msf_frekuensi?></td>
                    <td align="center"><?= $header->msg_frekuensi?></td>
                    <td align="center"><?= $header->msh_frekuensi?></td>
                    <td align="center"><?= $header->msi_frekuensi?></td>
                    <td align="center"><?= $header->msj_frekuensi?></td>
                    <td align="center"><?= $header->msk_frekuensi?></td>
                    <td align="center"><?= $header->msl_frekuensi?></td>
                    <td align="center"><?= $header->msm_frekuensi?></td>
                    <td align="center"><?= $header->msn_frekuensi?></td>
                    <td align="center"><?= $header->mso_frekuensi?></td>
                    <td align="center"><?= $header->msp_frekuensi?></td>
                </tr>
                <tr>
                    <td class="text-center">7</td>
                    <td>TCS</td>
                    <td>ALARM</td>
                    <td colspan="19"></td>
                </tr>
                <tr>
                    <td class="text-center">8</td>
                    <td colspan="2" class="text-center">KETERANGAN</td>
                    <td class="keterangan">INCOMING 1</td>
                    <td class="keterangan">INCOMING 2</td>
                    <td class="keterangan">INCOMING 3</td>
                    <td class="keterangan">AUX TRAFO </td>
                    <td class="keterangan">GH 127 A</td>
                    <td class="keterangan">OUT 1 </td>
                    <td class="keterangan">VT 1</td>
                    <td class="keterangan">COUPLER A1</td>
                    <td class="keterangan">COUPLER B2</td>
                    <td class="keterangan">COUPLER A2</td>
                    <td class="keterangan">COUPLER B2</td>
                    <td class="keterangan">VT 2</td>
                    <td class="keterangan">GH 127B </td>
                    <td class="keterangan">OUT 2</td>
                    <td class="keterangan">COUPLER A3</td>
                    <td class="keterangan">GH 128</td>
                    <td class="keterangan">OUT 2 </td>
                    <td class="keterangan">VT 3</td>
                    <td class="keterangan">COUPLERB3</td>
                </tr>
                <tr>
                    <td colspan="22" height="20"></td>
                </tr>
            </tbody>
            <tr>
                <th rowspan="5" valign="top">E</th>
                <th colspan="2">Battery Room</th>
                <th colspan="2" nowrap>BATTERY BANK 1 </th>
                <th></th>
                <th></th>
                <th colspan="2" nowrap>BATTERY BANK 2 </th>
                <th colspan="13" class="text-center">CATATAN </th>
            </tr>
            <tr>
                <th rowspan="2">110 V</th>
                <td>Status</td>
                <th colspan="2"><?php if($header->battery_bank_1_status_110==1){ echo "open";} else {echo "close";}?>
                </th>
                <th></th>
                <th></th>
                <th colspan="2"><?php if($header->battery_bank_2_status_110==1){ echo "open";} else {echo "close";}?>
                </th>
                <td valign="top" colspan="13" rowspan="4"><?= $header->catatan?></td>
            </tr>
            <tr>
                <td>Metering</td>
                <td colspan="2" align="center"><?=$header->battery_bank_1_meteran_110?></td>
                <td></td>
                <td></td>
                <td colspan="2" align="center"><?=$header->battery_bank_2_meteran_110?></td>
            </tr>
            <tr>
                <th rowspan="2">48 V</th>
                <td>Status</td>
                <th colspan="2"><?php if($header->battery_bank_1_status_48==1){ echo "open";} else {echo "close";}?>
                </th>
                <th></th>
                <th></th>
                <th colspan="2"><?php if($header->battery_bank_2_status_48==1){ echo "open";}else {echo "close";}?>
                </th>
            </tr>
            <tr>
                <td>Metering</td>
                <td colspan="2" align="center"><?= $header->battery_bank_1_meteran_48?></td>
                <td></td>
                <td></td>
                <td colspan="2" align="center"><?= $header->battery_bank_2_meteran_48?></td>
            </tr>
        </table>
    </div>
    <table border='0' width='100%' style="font-size:10px">
        <tbody>
            <tr>
                <td style="font-size:10px" align="left"> ASSISTANT MANAGER OF HIGH & MEDIUM VOLTAGE STATION
                    <br><br><br><br>
                    <img width="40" height="40" src="<?= base_url()?>/assets/qrcode/assisten/<?=qr_a($header->assistant_manager) ?>" alt=""><br>(<?= $header->assistant_manager?>)
                </td >
                <td style="font-size:10px" align="left">Tangerang,
                    <br>PETUGAS DINAS
                    <br>
                    <br>
                    <br>
                <br>
                <img width="40" height="40" src="<?= base_url()?>/assets/qrcode/petugas/<?=qr_b($header->petugas_dinas) ?>" alt="">
                    <br>(<?= $header->petugas_dinas?>)

                </td>
            </tr>
        </tbody>
    </table>
    <style>
        td {
            font-size: 10px
        }

        .keterangan {
            background-color: greenyellow;
            white-space: nowrap;
        }

        .th {
            background-color: yellow
        }

        .res {

            width: auto;
            overflow-x: auto;
            white-space: nowrap;
        }

        table td {
            padding: 5px;
        }
    </style>
</body>

</html>