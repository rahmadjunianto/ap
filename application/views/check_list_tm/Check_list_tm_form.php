<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <form action="<?php echo $action; ?>" method="post" role="form">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="panel"> ASSISTANT MANAGER OF HIGH & MEDIUM VOLTAGE STATION <sup
                                            style="color:red;">*</sup></label>
                                    <select required name="assistant_manager" id="assistant_manager"
                                        class="form-control">
                                        <option value="">Pilih</option>
                                        <?php foreach ($asisten_list as $rk) { ?>
                                        <option <?= $rk->nama==$assistant_manager ? "selected":""?>
                                            value="<?= $rk->nama?>"><?= $rk->nama?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="panel"> PETUGAS DINAS<sup style="color:red;">*</sup></label>
                                    <select required name="petugas_dinas" id="petugas_dinas" class="form-control">
                                        <option value="">Pilih</option>
                                        <?php foreach ($petugas_list as $rk) { ?>
                                        <option <?= $rk->nama==$petugas_dinas ? "selected":""?> value="<?= $rk->nama?>">
                                            <?= $rk->nama?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $id; ?>" />

                    <table border="1" width="100%" class="hoverTable">
                        <thead>
                            <tr>
                                <th style="background-color: burlywood" class="text-center">D</th>
                                <th colspan="2" style="background-color: burlywood" class="text-center">PANEL TM 20KV
                                </th>
                                <th width="45px" class="text-center th">XCA</th>
                                <th width="45px" class="text-center th">XCB</th>
                                <th width="45px" class="text-center th">XCC</th>
                                <th width="45px" class="text-center th">MSA</th>
                                <th width="45px" class="text-center th">MSB</th>
                                <th width="45px" class="text-center th">MSC</th>
                                <th width="45px" class="text-center th">MSD</th>
                                <th width="45px" class="text-center th">MSE</th>
                                <th width="45px" class="text-center th">MSF</th>
                                <th width="45px" class="text-center th">MSG</th>
                                <th width="45px" class="text-center th">MSH</th>
                                <th width="45px" class="text-center th">MSI</th>
                                <th width="45px" class="text-center th">MSJ</th>
                                <th width="45px" class="text-center th">MSK</th>
                                <th width="45px" class="text-center th">MSL</th>
                                <th width="45px" class="text-center th">MSM</th>
                                <th width="45px" class="text-center th">MSN</th>
                                <th width="45px" class="text-center th">MSO</th>
                                <th width="45px" class="text-center th">MSP</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="22" height="20"></td>
                            </tr>
                            <tr>
                                <td rowspan="2" class="text-center">1</td>
                                <td rowspan="2">CB</td>
                                <td>OPEN</td>
                                <td><input type="checkbox" <?php if($xca_open==1) { echo "checked";}?> value="1"
                                        name="xca_open"> </td>
                                <td><input type="checkbox" <?php if($xcb_open==1) { echo "checked";}?> value="1"
                                        name="xcb_open"> </td>
                                <td><input type="checkbox" <?php if($xcc_open==1) { echo "checked";}?> value="1"
                                        name="xcc_open"> </td>
                                <td><input type="checkbox" <?php if($msa_open==1) { echo "checked";}?> value="1"
                                        name="msa_open"> </td>
                                <td><input type="checkbox" <?php if($msb_open==1) { echo "checked";}?> value="1"
                                        name="msb_open"> </td>
                                <td><input type="checkbox" <?php if($msc_open==1) { echo "checked";}?> value="1"
                                        name="msc_open"> </td>
                                <td><input type="checkbox" <?php if($msd_open==1) { echo "checked";}?> value="1"
                                        name="msd_open"> </td>
                                <td><input type="checkbox" <?php if($mse_open==1) { echo "checked";}?> value="1"
                                        name="mse_open"> </td>
                                <td><input type="checkbox" <?php if($msf_open==1) { echo "checked";}?> value="1"
                                        name="msf_open"> </td>
                                <td><input type="checkbox" <?php if($msg_open==1) { echo "checked";}?> value="1"
                                        name="msg_open"> </td>
                                <td><input type="checkbox" <?php if($msh_open==1) { echo "checked";}?> value="1"
                                        name="msh_open"> </td>
                                <td><input type="checkbox" <?php if($msi_open==1) { echo "checked";}?> value="1"
                                        name="msi_open"> </td>
                                <td><input type="checkbox" <?php if($msj_open==1) { echo "checked";}?> value="1"
                                        name="msj_open"> </td>
                                <td><input type="checkbox" <?php if($msk_open==1) { echo "checked";}?> value="1"
                                        name="msk_open"> </td>
                                <td><input type="checkbox" <?php if($msl_open==1) { echo "checked";}?> value="1"
                                        name="msl_open"> </td>
                                <td><input type="checkbox" <?php if($msm_open==1) { echo "checked";}?> value="1"
                                        name="msm_open"> </td>
                                <td><input type="checkbox" <?php if($msn_open==1) { echo "checked";}?> value="1"
                                        name="msn_open"> </td>
                                <td><input type="checkbox" <?php if($mso_open==1) { echo "checked";}?> value="1"
                                        name="mso_open"> </td>
                                <td><input type="checkbox" <?php if($msp_open==1) { echo "checked";}?> value="1"
                                        name="msp_open"> </td>
                            </tr>
                            <tr>
                                <td>CLOSE</td>
                                <td><input type="checkbox" <?php if($xca_close==1) { echo "checked";}?> value="1"
                                        name="xca_close"></td>
                                <td><input type="checkbox" <?php if($xcb_close==1) { echo "checked";}?> value="1"
                                        name="xcb_close"></td>
                                <td><input type="checkbox" <?php if($xcc_close==1) { echo "checked";}?> value="1"
                                        name="xcc_close"></td>
                                <td><input type="checkbox" <?php if($msa_close==1) { echo "checked";}?> value="1"
                                        name="msa_close"></td>
                                <td><input type="checkbox" <?php if($msb_close==1) { echo "checked";}?> value="1"
                                        name="msb_close"></td>
                                <td><input type="checkbox" <?php if($msc_close==1) { echo "checked";}?> value="1"
                                        name="msc_close"></td>
                                <td><input type="checkbox" <?php if($msd_close==1) { echo "checked";}?> value="1"
                                        name="msd_close"></td>
                                <td><input type="checkbox" <?php if($mse_close==1) { echo "checked";}?> value="1"
                                        name="mse_close"></td>
                                <td><input type="checkbox" <?php if($msf_close==1) { echo "checked";}?> value="1"
                                        name="msf_close"></td>
                                <td><input type="checkbox" <?php if($msg_close==1) { echo "checked";}?> value="1"
                                        name="msg_close"></td>
                                <td><input type="checkbox" <?php if($msh_close==1) { echo "checked";}?> value="1"
                                        name="msh_close"></td>
                                <td><input type="checkbox" <?php if($msi_close==1) { echo "checked";}?> value="1"
                                        name="msi_close"></td>
                                <td><input type="checkbox" <?php if($msj_close==1) { echo "checked";}?> value="1"
                                        name="msj_close"></td>
                                <td><input type="checkbox" <?php if($msk_close==1) { echo "checked";}?> value="1"
                                        name="msk_close"></td>
                                <td><input type="checkbox" <?php if($msl_close==1) { echo "checked";}?> value="1"
                                        name="msl_close"></td>
                                <td><input type="checkbox" <?php if($msm_close==1) { echo "checked";}?> value="1"
                                        name="msm_close"></td>
                                <td><input type="checkbox" <?php if($msn_close==1) { echo "checked";}?> value="1"
                                        name="msn_close"></td>
                                <td><input type="checkbox" <?php if($mso_close==1) { echo "checked";}?> value="1"
                                        name="mso_close"></td>
                                <td><input type="checkbox" <?php if($msp_close==1) { echo "checked";}?> value="1"
                                        name="msp_close"></td>
                            </tr>
                            <tr>
                                <td colspan="22" height="20"></td>
                            </tr>
                            <tr>
                                <td rowspan="2" class="text-center">2</td>
                                <td rowspan="2">DS</td>
                                <td>OPEN</td>
                                <td><input type="checkbox" <?php if($xca_ds_open==1) { echo "checked";}?> value="1"
                                        name="xca_ds_open"> </td>
                                <td><input type="checkbox" <?php if($xcb_ds_open==1) { echo "checked";}?> value="1"
                                        name="xcb_ds_open"> </td>
                                <td><input type="checkbox" <?php if($xcc_ds_open==1) { echo "checked";}?> value="1"
                                        name="xcc_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msa_ds_open==1) { echo "checked";}?> value="1"
                                        name="msa_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msb_ds_open==1) { echo "checked";}?> value="1"
                                        name="msb_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msc_ds_open==1) { echo "checked";}?> value="1"
                                        name="msc_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msd_ds_open==1) { echo "checked";}?> value="1"
                                        name="msd_ds_open"> </td>
                                <td><input type="checkbox" <?php if($mse_ds_open==1) { echo "checked";}?> value="1"
                                        name="mse_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msf_ds_open==1) { echo "checked";}?> value="1"
                                        name="msf_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msg_ds_open==1) { echo "checked";}?> value="1"
                                        name="msg_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msh_ds_open==1) { echo "checked";}?> value="1"
                                        name="msh_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msi_ds_open==1) { echo "checked";}?> value="1"
                                        name="msi_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msj_ds_open==1) { echo "checked";}?> value="1"
                                        name="msj_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msk_ds_open==1) { echo "checked";}?> value="1"
                                        name="msk_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msl_ds_open==1) { echo "checked";}?> value="1"
                                        name="msl_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msm_ds_open==1) { echo "checked";}?> value="1"
                                        name="msm_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msn_ds_open==1) { echo "checked";}?> value="1"
                                        name="msn_ds_open"> </td>
                                <td><input type="checkbox" <?php if($mso_ds_open==1) { echo "checked";}?> value="1"
                                        name="mso_ds_open"> </td>
                                <td><input type="checkbox" <?php if($msp_ds_open==1) { echo "checked";}?> value="1"
                                        name="msp_ds_open"> </td>
                            </tr>
                            <tr>
                                <td>CLOSE</td>
                                <td><input type="checkbox" <?php if($xca_ds_close==1) { echo "checked";}?> value="1"
                                        name="xca_ds_close"></td>
                                <td><input type="checkbox" <?php if($xcb_ds_close==1) { echo "checked";}?> value="1"
                                        name="xcb_ds_close"></td>
                                <td><input type="checkbox" <?php if($xcc_ds_close==1) { echo "checked";}?> value="1"
                                        name="xcc_ds_close"></td>
                                <td><input type="checkbox" <?php if($msa_ds_close==1) { echo "checked";}?> value="1"
                                        name="msa_ds_close"></td>
                                <td><input type="checkbox" <?php if($msb_ds_close==1) { echo "checked";}?> value="1"
                                        name="msb_ds_close"></td>
                                <td><input type="checkbox" <?php if($msc_ds_close==1) { echo "checked";}?> value="1"
                                        name="msc_ds_close"></td>
                                <td><input type="checkbox" <?php if($msd_ds_close==1) { echo "checked";}?> value="1"
                                        name="msd_ds_close"></td>
                                <td><input type="checkbox" <?php if($mse_ds_close==1) { echo "checked";}?> value="1"
                                        name="mse_ds_close"></td>
                                <td><input type="checkbox" <?php if($msf_ds_close==1) { echo "checked";}?> value="1"
                                        name="msf_ds_close"></td>
                                <td><input type="checkbox" <?php if($msg_ds_close==1) { echo "checked";}?> value="1"
                                        name="msg_ds_close"></td>
                                <td><input type="checkbox" <?php if($msh_ds_close==1) { echo "checked";}?> value="1"
                                        name="msh_ds_close"></td>
                                <td><input type="checkbox" <?php if($msi_ds_close==1) { echo "checked";}?> value="1"
                                        name="msi_ds_close"></td>
                                <td><input type="checkbox" <?php if($msj_ds_close==1) { echo "checked";}?> value="1"
                                        name="msj_ds_close"></td>
                                <td><input type="checkbox" <?php if($msk_ds_close==1) { echo "checked";}?> value="1"
                                        name="msk_ds_close"></td>
                                <td><input type="checkbox" <?php if($msl_ds_close==1) { echo "checked";}?> value="1"
                                        name="msl_ds_close"></td>
                                <td><input type="checkbox" <?php if($msm_ds_close==1) { echo "checked";}?> value="1"
                                        name="msm_ds_close"></td>
                                <td><input type="checkbox" <?php if($msn_ds_close==1) { echo "checked";}?> value="1"
                                        name="msn_ds_close"></td>
                                <td><input type="checkbox" <?php if($mso_ds_close==1) { echo "checked";}?> value="1"
                                        name="mso_ds_close"></td>
                                <td><input type="checkbox" <?php if($msp_ds_close==1) { echo "checked";}?> value="1"
                                        name="msp_ds_close"></td>
                            </tr>
                            <tr>
                                <td colspan="22" height="20"></td>
                            </tr> <tr>
                                <td rowspan="2" class="text-center">3</td>
                                <td rowspan="2">GROUNDING</td>
                                <td>OPEN</td>
                                <td><input type="checkbox" <?php if($xca_gr_open==1) { echo "checked";}?> value="1"
                                        name="xca_gr_open"> </td>
                                <td><input type="checkbox" <?php if($xcb_gr_open==1) { echo "checked";}?> value="1"
                                        name="xcb_gr_open"> </td>
                                <td><input type="checkbox" <?php if($xcc_gr_open==1) { echo "checked";}?> value="1"
                                        name="xcc_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msa_gr_open==1) { echo "checked";}?> value="1"
                                        name="msa_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msb_gr_open==1) { echo "checked";}?> value="1"
                                        name="msb_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msc_gr_open==1) { echo "checked";}?> value="1"
                                        name="msc_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msd_gr_open==1) { echo "checked";}?> value="1"
                                        name="msd_gr_open"> </td>
                                <td><input type="checkbox" <?php if($mse_gr_open==1) { echo "checked";}?> value="1"
                                        name="mse_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msf_gr_open==1) { echo "checked";}?> value="1"
                                        name="msf_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msg_gr_open==1) { echo "checked";}?> value="1"
                                        name="msg_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msh_gr_open==1) { echo "checked";}?> value="1"
                                        name="msh_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msi_gr_open==1) { echo "checked";}?> value="1"
                                        name="msi_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msj_gr_open==1) { echo "checked";}?> value="1"
                                        name="msj_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msk_gr_open==1) { echo "checked";}?> value="1"
                                        name="msk_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msl_gr_open==1) { echo "checked";}?> value="1"
                                        name="msl_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msm_gr_open==1) { echo "checked";}?> value="1"
                                        name="msm_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msn_gr_open==1) { echo "checked";}?> value="1"
                                        name="msn_gr_open"> </td>
                                <td><input type="checkbox" <?php if($mso_gr_open==1) { echo "checked";}?> value="1"
                                        name="mso_gr_open"> </td>
                                <td><input type="checkbox" <?php if($msp_gr_open==1) { echo "checked";}?> value="1"
                                        name="msp_gr_open"> </td>
                            </tr>
                            <tr>
                                <td>CLOSE</td>
                                <td><input type="checkbox" <?php if($xca_gr_close==1) { echo "checked";}?> value="1"
                                        name="xca_gr_close"></td>
                                <td><input type="checkbox" <?php if($xcb_gr_close==1) { echo "checked";}?> value="1"
                                        name="xcb_gr_close"></td>
                                <td><input type="checkbox" <?php if($xcc_gr_close==1) { echo "checked";}?> value="1"
                                        name="xcc_gr_close"></td>
                                <td><input type="checkbox" <?php if($msa_gr_close==1) { echo "checked";}?> value="1"
                                        name="msa_gr_close"></td>
                                <td><input type="checkbox" <?php if($msb_gr_close==1) { echo "checked";}?> value="1"
                                        name="msb_gr_close"></td>
                                <td><input type="checkbox" <?php if($msc_gr_close==1) { echo "checked";}?> value="1"
                                        name="msc_gr_close"></td>
                                <td><input type="checkbox" <?php if($msd_gr_close==1) { echo "checked";}?> value="1"
                                        name="msd_gr_close"></td>
                                <td><input type="checkbox" <?php if($mse_gr_close==1) { echo "checked";}?> value="1"
                                        name="mse_gr_close"></td>
                                <td><input type="checkbox" <?php if($msf_gr_close==1) { echo "checked";}?> value="1"
                                        name="msf_gr_close"></td>
                                <td><input type="checkbox" <?php if($msg_gr_close==1) { echo "checked";}?> value="1"
                                        name="msg_gr_close"></td>
                                <td><input type="checkbox" <?php if($msh_gr_close==1) { echo "checked";}?> value="1"
                                        name="msh_gr_close"></td>
                                <td><input type="checkbox" <?php if($msi_gr_close==1) { echo "checked";}?> value="1"
                                        name="msi_gr_close"></td>
                                <td><input type="checkbox" <?php if($msj_gr_close==1) { echo "checked";}?> value="1"
                                        name="msj_gr_close"></td>
                                <td><input type="checkbox" <?php if($msk_gr_close==1) { echo "checked";}?> value="1"
                                        name="msk_gr_close"></td>
                                <td><input type="checkbox" <?php if($msl_gr_close==1) { echo "checked";}?> value="1"
                                        name="msl_gr_close"></td>
                                <td><input type="checkbox" <?php if($msm_gr_close==1) { echo "checked";}?> value="1"
                                        name="msm_gr_close"></td>
                                <td><input type="checkbox" <?php if($msn_gr_close==1) { echo "checked";}?> value="1"
                                        name="msn_gr_close"></td>
                                <td><input type="checkbox" <?php if($mso_gr_close==1) { echo "checked";}?> value="1"
                                        name="mso_gr_close"></td>
                                <td><input type="checkbox" <?php if($msp_gr_close==1) { echo "checked";}?> value="1"
                                        name="msp_gr_close"></td>
                            </tr>
                            <tr>
                                <td colspan="22" height="20"></td>
                            </tr>
                            <tr>
                                <td rowspan="2" class="text-center">4</td>
                                <td rowspan="2">SPRING</td>
                                <td>CHARGE</td>
                                <td><input type="checkbox" <?php if($xca_charge==1) { echo "checked";}?> value="1"
                                        name="xca_charge"></td>
                                <td><input type="checkbox" <?php if($xcb_charge==1) { echo "checked";}?> value="1"
                                        name="xcb_charge"></td>
                                <td><input type="checkbox" <?php if($xcc_charge==1) { echo "checked";}?> value="1"
                                        name="xcc_charge"></td>
                                <td><input type="checkbox" <?php if($msa_charge==1) { echo "checked";}?> value="1"
                                        name="msa_charge"></td>
                                <td><input type="checkbox" <?php if($msb_charge==1) { echo "checked";}?> value="1"
                                        name="msb_charge"></td>
                                <td><input type="checkbox" <?php if($msc_charge==1) { echo "checked";}?> value="1"
                                        name="msc_charge"></td>
                                <td><input type="checkbox" <?php if($msd_charge==1) { echo "checked";}?> value="1"
                                        name="msd_charge"></td>
                                <td><input type="checkbox" <?php if($mse_charge==1) { echo "checked";}?> value="1"
                                        name="mse_charge"></td>
                                <td><input type="checkbox" <?php if($msf_charge==1) { echo "checked";}?> value="1"
                                        name="msf_charge"></td>
                                <td><input type="checkbox" <?php if($msg_charge==1) { echo "checked";}?> value="1"
                                        name="msg_charge"></td>
                                <td><input type="checkbox" <?php if($msh_charge==1) { echo "checked";}?> value="1"
                                        name="msh_charge"></td>
                                <td><input type="checkbox" <?php if($msi_charge==1) { echo "checked";}?> value="1"
                                        name="msi_charge"></td>
                                <td><input type="checkbox" <?php if($msj_charge==1) { echo "checked";}?> value="1"
                                        name="msj_charge"></td>
                                <td><input type="checkbox" <?php if($msk_charge==1) { echo "checked";}?> value="1"
                                        name="msk_charge"></td>
                                <td><input type="checkbox" <?php if($msl_charge==1) { echo "checked";}?> value="1"
                                        name="msl_charge"></td>
                                <td><input type="checkbox" <?php if($msm_charge==1) { echo "checked";}?> value="1"
                                        name="msm_charge"></td>
                                <td><input type="checkbox" <?php if($msn_charge==1) { echo "checked";}?> value="1"
                                        name="msn_charge"></td>
                                <td><input type="checkbox" <?php if($mso_charge==1) { echo "checked";}?> value="1"
                                        name="mso_charge"></td>
                                <td><input type="checkbox" <?php if($msp_charge==1) { echo "checked";}?> value="1"
                                        name="msp_charge"></td>
                            </tr>
                            <tr>
                                <td>DISCHARGE</td>
                                <td><input type="checkbox" <?php if($xca_discharge==1) { echo "checked";}?> value="1"
                                        name="xca_discharge"></td>
                                <td><input type="checkbox" <?php if($xcb_discharge==1) { echo "checked";}?> value="1"
                                        name="xcb_discharge"></td>
                                <td><input type="checkbox" <?php if($xcc_discharge==1) { echo "checked";}?> value="1"
                                        name="xcc_discharge"></td>
                                <td><input type="checkbox" <?php if($msa_discharge==1) { echo "checked";}?> value="1"
                                        name="msa_discharge"></td>
                                <td><input type="checkbox" <?php if($msb_discharge==1) { echo "checked";}?> value="1"
                                        name="msb_discharge"></td>
                                <td><input type="checkbox" <?php if($msc_discharge==1) { echo "checked";}?> value="1"
                                        name="msc_discharge"></td>
                                <td><input type="checkbox" <?php if($msd_discharge==1) { echo "checked";}?> value="1"
                                        name="msd_discharge"></td>
                                <td><input type="checkbox" <?php if($mse_discharge==1) { echo "checked";}?> value="1"
                                        name="mse_discharge"></td>
                                <td><input type="checkbox" <?php if($msf_discharge==1) { echo "checked";}?> value="1"
                                        name="msf_discharge"></td>
                                <td><input type="checkbox" <?php if($msg_discharge==1) { echo "checked";}?> value="1"
                                        name="msg_discharge"></td>
                                <td><input type="checkbox" <?php if($msh_discharge==1) { echo "checked";}?> value="1"
                                        name="msh_discharge"></td>
                                <td><input type="checkbox" <?php if($msi_discharge==1) { echo "checked";}?> value="1"
                                        name="msi_discharge"></td>
                                <td><input type="checkbox" <?php if($msj_discharge==1) { echo "checked";}?> value="1"
                                        name="msj_discharge"></td>
                                <td><input type="checkbox" <?php if($msk_discharge==1) { echo "checked";}?> value="1"
                                        name="msk_discharge"></td>
                                <td><input type="checkbox" <?php if($msl_discharge==1) { echo "checked";}?> value="1"
                                        name="msl_discharge"></td>
                                <td><input type="checkbox" <?php if($msm_discharge==1) { echo "checked";}?> value="1"
                                        name="msm_discharge"></td>
                                <td><input type="checkbox" <?php if($msn_discharge==1) { echo "checked";}?> value="1"
                                        name="msn_discharge"></td>
                                <td><input type="checkbox" <?php if($mso_discharge==1) { echo "checked";}?> value="1"
                                        name="mso_discharge"></td>
                                <td><input type="checkbox" <?php if($msp_discharge==1) { echo "checked";}?> value="1"
                                        name="msp_discharge"></td>
                            </tr>
                            <tr>
                                <td colspan="22" height="20"></td>
                            </tr>
                            <tr>
                                <td rowspan="2" class="text-center">5</td>
                                <td rowspan="2">PROTEKSI</td>
                                <td>NORMAL</td>
                                <td><input type="checkbox" <?php if($xca_normal==1) { echo "checked";}?> value="1"
                                        name="xca_normal"></td>
                                <td><input type="checkbox" <?php if($xcb_normal==1) { echo "checked";}?> value="1"
                                        name="xcb_normal"></td>
                                <td><input type="checkbox" <?php if($xcc_normal==1) { echo "checked";}?> value="1"
                                        name="xcc_normal"></td>
                                <td><input type="checkbox" <?php if($msa_normal==1) { echo "checked";}?> value="1"
                                        name="msa_normal"></td>
                                <td><input type="checkbox" <?php if($msb_normal==1) { echo "checked";}?> value="1"
                                        name="msb_normal"></td>
                                <td><input type="checkbox" <?php if($msc_normal==1) { echo "checked";}?> value="1"
                                        name="msc_normal"></td>
                                <td><input type="checkbox" <?php if($msd_normal==1) { echo "checked";}?> value="1"
                                        name="msd_normal"></td>
                                <td><input type="checkbox" <?php if($mse_normal==1) { echo "checked";}?> value="1"
                                        name="mse_normal"></td>
                                <td><input type="checkbox" <?php if($msf_normal==1) { echo "checked";}?> value="1"
                                        name="msf_normal"></td>
                                <td><input type="checkbox" <?php if($msg_normal==1) { echo "checked";}?> value="1"
                                        name="msg_normal"></td>
                                <td><input type="checkbox" <?php if($msh_normal==1) { echo "checked";}?> value="1"
                                        name="msh_normal"></td>
                                <td><input type="checkbox" <?php if($msi_normal==1) { echo "checked";}?> value="1"
                                        name="msi_normal"></td>
                                <td><input type="checkbox" <?php if($msj_normal==1) { echo "checked";}?> value="1"
                                        name="msj_normal"></td>
                                <td><input type="checkbox" <?php if($msk_normal==1) { echo "checked";}?> value="1"
                                        name="msk_normal"></td>
                                <td><input type="checkbox" <?php if($msl_normal==1) { echo "checked";}?> value="1"
                                        name="msl_normal"></td>
                                <td><input type="checkbox" <?php if($msm_normal==1) { echo "checked";}?> value="1"
                                        name="msm_normal"></td>
                                <td><input type="checkbox" <?php if($msn_normal==1) { echo "checked";}?> value="1"
                                        name="msn_normal"></td>
                                <td><input type="checkbox" <?php if($mso_normal==1) { echo "checked";}?> value="1"
                                        name="mso_normal"></td>
                                <td><input type="checkbox" <?php if($msp_normal==1) { echo "checked";}?> value="1"
                                        name="msp_normal"></td>
                            </tr>
                            <tr>
                                <td>ABNORMAL</td>
                                <td><input type="checkbox" <?php if($xca_abnormal==1) { echo "checked";}?> value="1"
                                        name="xca_abnormal"></td>
                                <td><input type="checkbox" <?php if($xcb_abnormal==1) { echo "checked";}?> value="1"
                                        name="xcb_abnormal"></td>
                                <td><input type="checkbox" <?php if($xcc_abnormal==1) { echo "checked";}?> value="1"
                                        name="xcc_abnormal"></td>
                                <td><input type="checkbox" <?php if($msa_abnormal==1) { echo "checked";}?> value="1"
                                        name="msa_abnormal"></td>
                                <td><input type="checkbox" <?php if($msb_abnormal==1) { echo "checked";}?> value="1"
                                        name="msb_abnormal"></td>
                                <td><input type="checkbox" <?php if($msc_abnormal==1) { echo "checked";}?> value="1"
                                        name="msc_abnormal"></td>
                                <td><input type="checkbox" <?php if($msd_abnormal==1) { echo "checked";}?> value="1"
                                        name="msd_abnormal"></td>
                                <td><input type="checkbox" <?php if($mse_abnormal==1) { echo "checked";}?> value="1"
                                        name="mse_abnormal"></td>
                                <td><input type="checkbox" <?php if($msf_abnormal==1) { echo "checked";}?> value="1"
                                        name="msf_abnormal"></td>
                                <td><input type="checkbox" <?php if($msg_abnormal==1) { echo "checked";}?> value="1"
                                        name="msg_abnormal"></td>
                                <td><input type="checkbox" <?php if($msh_abnormal==1) { echo "checked";}?> value="1"
                                        name="msh_abnormal"></td>
                                <td><input type="checkbox" <?php if($msi_abnormal==1) { echo "checked";}?> value="1"
                                        name="msi_abnormal"></td>
                                <td><input type="checkbox" <?php if($msj_abnormal==1) { echo "checked";}?> value="1"
                                        name="msj_abnormal"></td>
                                <td><input type="checkbox" <?php if($msk_abnormal==1) { echo "checked";}?> value="1"
                                        name="msk_abnormal"></td>
                                <td><input type="checkbox" <?php if($msl_abnormal==1) { echo "checked";}?> value="1"
                                        name="msl_abnormal"></td>
                                <td><input type="checkbox" <?php if($msm_abnormal==1) { echo "checked";}?> value="1"
                                        name="msm_abnormal"></td>
                                <td><input type="checkbox" <?php if($msn_abnormal==1) { echo "checked";}?> value="1"
                                        name="msn_abnormal"></td>
                                <td><input type="checkbox" <?php if($mso_abnormal==1) { echo "checked";}?> value="1"
                                        name="mso_abnormal"></td>
                                <td><input type="checkbox" <?php if($msp_abnormal==1) { echo "checked";}?> value="1"
                                        name="msp_abnormal"></td>
                            </tr>
                            <tr>
                                <td colspan="22" height="20"></td>
                            </tr>
                            <tr>
                                <td rowspan="4" class="text-center">6</td>
                                <td rowspan="4">METERING</td>
                                <td>ARUS (A)</td>
                                <td><input class="number" type="text" value="<?= $xca_arus?>" name="xca_arus"></td>
                                <td><input class="number" type="text" value="<?= $xcb_arus?>" name="xcb_arus"></td>
                                <td><input class="number" type="text" value="<?= $xcc_arus?>" name="xcc_arus"></td>
                                <td><input class="number" type="text" value="<?= $msa_arus?>" name="msa_arus"></td>
                                <td><input class="number" type="text" value="<?= $msb_arus?>" name="msb_arus"></td>
                                <td><input class="number" type="text" value="<?= $msc_arus?>" name="msc_arus"></td>
                                <td><input class="number" type="text" value="<?= $msd_arus?>" name="msd_arus"></td>
                                <td><input class="number" type="text" value="<?= $mse_arus?>" name="mse_arus"></td>
                                <td><input class="number" type="text" value="<?= $msf_arus?>" name="msf_arus"></td>
                                <td><input class="number" type="text" value="<?= $msg_arus?>" name="msg_arus"></td>
                                <td><input class="number" type="text" value="<?= $msh_arus?>" name="msh_arus"></td>
                                <td><input class="number" type="text" value="<?= $msi_arus?>" name="msi_arus"></td>
                                <td><input class="number" type="text" value="<?= $msj_arus?>" name="msj_arus"></td>
                                <td><input class="number" type="text" value="<?= $msk_arus?>" name="msk_arus"></td>
                                <td><input class="number" type="text" value="<?= $msl_arus?>" name="msl_arus"></td>
                                <td><input class="number" type="text" value="<?= $msm_arus?>" name="msm_arus"></td>
                                <td><input class="number" type="text" value="<?= $msn_arus?>" name="msn_arus"></td>
                                <td><input class="number" type="text" value="<?= $mso_arus?>" name="mso_arus"></td>
                                <td><input class="number" type="text" value="<?= $msp_arus?>" name="msp_arus"></td>
                            </tr>
                            <tr>
                                <td>TEGANGAN (KV)</td>
                                <td><input class="number" type="text" value="<?= $xca_tegangan?>" name="xca_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $xcb_tegangan?>" name="xcb_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $xcc_tegangan?>" name="xcc_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msa_tegangan?>" name="msa_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msb_tegangan?>" name="msb_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msc_tegangan?>" name="msc_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msd_tegangan?>" name="msd_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $mse_tegangan?>" name="mse_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msf_tegangan?>" name="msf_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msg_tegangan?>" name="msg_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msh_tegangan?>" name="msh_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msi_tegangan?>" name="msi_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msj_tegangan?>" name="msj_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msk_tegangan?>" name="msk_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msl_tegangan?>" name="msl_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msm_tegangan?>" name="msm_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msn_tegangan?>" name="msn_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $mso_tegangan?>" name="mso_tegangan">
                                </td>
                                <td><input class="number" type="text" value="<?= $msp_tegangan?>" name="msp_tegangan">
                                </td>
                            </tr>
                            <tr>
                                <td>DAYA (MVA)</td>
                                <td><input class="number" type="text" value="<?= $xca_daya?>" name="xca_daya"></td>
                                <td><input class="number" type="text" value="<?= $xcb_daya?>" name="xcb_daya"></td>
                                <td><input class="number" type="text" value="<?= $xcc_daya?>" name="xcc_daya"></td>
                                <td><input class="number" type="text" value="<?= $msa_daya?>" name="msa_daya"></td>
                                <td><input class="number" type="text" value="<?= $msb_daya?>" name="msb_daya"></td>
                                <td><input class="number" type="text" value="<?= $msc_daya?>" name="msc_daya"></td>
                                <td><input class="number" type="text" value="<?= $msd_daya?>" name="msd_daya"></td>
                                <td><input class="number" type="text" value="<?= $mse_daya?>" name="mse_daya"></td>
                                <td><input class="number" type="text" value="<?= $msf_daya?>" name="msf_daya"></td>
                                <td><input class="number" type="text" value="<?= $msg_daya?>" name="msg_daya"></td>
                                <td><input class="number" type="text" value="<?= $msh_daya?>" name="msh_daya"></td>
                                <td><input class="number" type="text" value="<?= $msi_daya?>" name="msi_daya"></td>
                                <td><input class="number" type="text" value="<?= $msj_daya?>" name="msj_daya"></td>
                                <td><input class="number" type="text" value="<?= $msk_daya?>" name="msk_daya"></td>
                                <td><input class="number" type="text" value="<?= $msl_daya?>" name="msl_daya"></td>
                                <td><input class="number" type="text" value="<?= $msm_daya?>" name="msm_daya"></td>
                                <td><input class="number" type="text" value="<?= $msn_daya?>" name="msn_daya"></td>
                                <td><input class="number" type="text" value="<?= $mso_daya?>" name="mso_daya"></td>
                                <td><input class="number" type="text" value="<?= $msp_daya?>" name="msp_daya"></td>
                            </tr>
                            <tr>
                                <td>FREKUENSI (HZ)</td>
                                <td><input class="number" type="text" value="<?= $xca_frekuensi?>" name="xca_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $xcb_frekuensi?>" name="xcb_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $xcc_frekuensi?>" name="xcc_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msa_frekuensi?>" name="msa_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msb_frekuensi?>" name="msb_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msc_frekuensi?>" name="msc_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msd_frekuensi?>" name="msd_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $mse_frekuensi?>" name="mse_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msf_frekuensi?>" name="msf_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msg_frekuensi?>" name="msg_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msh_frekuensi?>" name="msh_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msi_frekuensi?>" name="msi_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msj_frekuensi?>" name="msj_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msk_frekuensi?>" name="msk_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msl_frekuensi?>" name="msl_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msm_frekuensi?>" name="msm_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msn_frekuensi?>" name="msn_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $mso_frekuensi?>" name="mso_frekuensi">
                                </td>
                                <td><input class="number" type="text" value="<?= $msp_frekuensi?>" name="msp_frekuensi">
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">7</td>
                                <td>TCS</td>
                                <td>ALARM</td>
                            </tr>
                            <tr>
                                <td class="text-center">8</td>
                                <td colspan="2" class="text-center">KETERANGAN</td>
                                <td width="40px" nowrap class="keterangan">INCOMING 1</td>
                                <td width="40px" nowrap class="keterangan">INCOMING 2</td>
                                <td width="40px" nowrap class="keterangan">INCOMING 3</td>
                                <td width="40px" nowrap class="keterangan">AUX TRAFO </td>
                                <td width="40px" nowrap class="keterangan">GH 127 A</td>
                                <td width="40px" nowrap class="keterangan">OUT 1 </td>
                                <td width="40px" nowrap class="keterangan">VT 1</td>
                                <td width="40px" nowrap class="keterangan">COUPLER A1</td>
                                <td width="40px" nowrap class="keterangan">COUPLER B2</td>
                                <td width="40px" nowrap class="keterangan">COUPLER A2</td>
                                <td width="40px" nowrap class="keterangan">COUPLER B2</td>
                                <td width="40px" nowrap class="keterangan">VT 2</td>
                                <td width="40px" nowrap class="keterangan">GH 127B </td>
                                <td width="40px" nowrap class="keterangan">OUT 2</td>
                                <td width="40px" nowrap class="keterangan">COUPLER A3</td>
                                <td width="40px" nowrap class="keterangan">GH 128</td>
                                <td width="40px" nowrap class="keterangan">OUT 2 </td>
                                <td width="40px" nowrap class="keterangan">VT 3</td>
                                <td width="40px" nowrap class="keterangan">COUPLERB3</td>
                            </tr>
                            <tr>
                                <td colspan="22" height="20"></td>
                            </tr>
                        </tbody>
                        <tr>
                            <th rowspan="5" valign="top">E</th>
                            <th colspan="2">Battery Room</th>
                            <th colspan="2">BATTERY BANK 1 </th>
                            <th></th>
                            <th></th>
                            <th colspan="2" nowrap>BATTERY BANK 2 </th>
                            <th colspan="13" class="text-center">CATATAN </th>
                        </tr>
                        <tr>
                            <th rowspan="2">110 V</th>
                            <td>Status</td>
                            <th colspan="2">
                                <select name="battery_bank_1_status_110" id="">
                                    <option <?php if($battery_bank_1_status_110==0){ echo "selected";}?> value="0">Close
                                    </option>
                                    <option <?php if($battery_bank_1_status_110==1){ echo "selected";}?> value="1">Open
                                    </option>
                                </select>
                            </th>
                            <th></th>
                            <th></th>
                            <th colspan="2">
                                <select name="battery_bank_2_status_110" id="">
                                    <option <?php if($battery_bank_2_status_110==0){ echo "selected";}?> value="0">Close
                                    </option>
                                    <option <?php if($battery_bank_2_status_110==1){ echo "selected";}?> value="1">Open
                                    </option>
                                </select>
                            </th>
                            <th colspan="13" rowspan="4">
                                <textarea style="width: 100%; height: 100%; border: none" name="catatan"><?= $catatan?>
    </textarea></th>
                        </tr>
                        <tr>
                            <td>Metering</td>
                            <td colspan="2"><input class="number" type="text" value="<?= $battery_bank_1_meteran_110?>"
                                    name="battery_bank_1_meteran_110"></td>
                            <td></td>
                            <td></td>
                            <td colspan="2"><input class="number" type="text" value="<?= $battery_bank_2_meteran_110?>"
                                    name="battery_bank_2_meteran_110"></td>
                        </tr>
                        <tr>
                            <th rowspan="2">48 V</th>
                            <td>Status</td>
                            <th colspan="2">
                                <select name="battery_bank_1_status_48" id="">
                                    <option <?php if($battery_bank_1_status_48==0){ echo "selected";}?> value="0">Close
                                    </option>
                                    <option <?php if($battery_bank_1_status_48==1){ echo "selected";}?> value="1">Open
                                    </option>
                                </select>
                            </th>
                            <th></th>
                            <th></th>
                            <th colspan="2">
                                <select name="battery_bank_2_status_48" id="">
                                    <option <?php if($battery_bank_2_status_48==0){ echo "selected";}?> value="0">Close
                                    </option>
                                    <option <?php if($battery_bank_2_status_48==1){ echo "selected";}?> value="1">Open
                                    </option>
                                </select>
                            </th>
                        </tr>
                        <tr>
                            <td>Metering</td>
                            <td colspan="2"><input class="number" type="text" value="<?= $battery_bank_1_meteran_48?>"
                                    name="battery_bank_1_meteran_48"></td>
                            <td></td>
                            <td></td>
                            <td colspan="2"><input class="number" type="text" value="<?= $battery_bank_2_meteran_48?>"
                                    name="battery_bank_2_meteran_48"></td>
                        </tr>
                    </table>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        <?= anchor('check_list_tm','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    td {
        font-size: 10px
    }

    .keterangan {
        background-color: greenyellow
    }

    .th {
        background-color: yellow
    }

    table td {
        position: relative;
    }

    table td input {
        position: absolute;
        display: block;
        top: 0;
        left: 0;
        margin: 0;
        height: 100%;
        width: 100%;
        border: none;
        padding: 2px;
        box-sizing: border-box;
    }

    select {
        border: 0;
        width: 100%;
    }

    /* .hoverTable{
                width:100%;
                border-collapse:collapse;
        }
        .hoverTable td{
                padding:7px; border:#4e95f4 1px solid;
        } */
    /* Define the default color for all the table rows */
    /* .hoverTable tr{
                background: #b8d1f3;
        } */
    /* Define the hover highlight color for the table row */
    /* .hoverTable tr:hover {
          background-color: #ffff99;
    } */
</style>
<script>
    function isNumberKey(evt) {
        var e = evt || window.event; //window.event is safer, thanks @ThiefMaster
        var charCode = e.which || e.keyCode;
        if (charCode > 31 && (charCode < 47 || charCode > 57))
            return false;
        if (e.shiftKey) return false;
        return true;
    }
</script>