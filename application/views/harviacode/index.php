<div class="row">
  <div class="col-md-12">
    <div class="box">
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
        <div class="col-sm-4">
        <div class="card-box">
        	<h4 class="m-t-0 m-b-30 header-title">Reguler Table</h4>
            <form role="form" method="post" action="<?= base_url().'harviacode/proses'?>">

                <div class="form-group">
                    <label>Table <sup style="color:red;">*</sup></label>
                        <select id="table_name" name="table_name" class="form-control" onchange="setname()">
                        		<option value="">Silahkan Pilih</option>
                        		<?php foreach($list_table as $list_table){?>
                        			<option value="<?= $list_table->TABLE_NAME ?>"><?= $list_table->TABLE_NAME ?></option>
                        		<?php } ?>
                        </select>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-3 col-form-label">Controller</label>
                    <div class="col-9">
                        <input type="text" class="form-control" id="controller" name="controller" placeholder="Nama controller">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-3 col-form-label">Model</label>
                    <div class="col-9">
                        <input type="text" class="form-control" id="model" name="model" placeholder="Nama model">
                    </div>
                </div>
                <div class="form-group mb-0 justify-content-end">
                    <div class="col-9">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Proses</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card-box">
        <?php if(!empty($this->session->flashdata('hasil'))){ ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php  foreach($this->session->flashdata('hasil') as $h ){
                    echo '<p>' . $h . '</p>';
                } ?>
            </div>
        <?php } ?>

                <p><strong>About :</strong></p>
                <p>
                    Codeigniter CRUD Generator is a simple tool to auto generate model, controller and view from your table. This tool will boost your
                    writing code. This CRUD generator will make a complete CRUD operation, pagination, search, form*, form validation, export to excel, and export to word.
                    This CRUD Generator using bootstrap 3 style. You still need to modify the result code for more customization.
                </p>
                <small>* generate textarea and text input only</small>
                <p>
                    Please visit and like <a target="_blank" href="http://harviacode.com"><b>harviacode.com</b></a> for more info and PHP tutorials.
                </p>

                <p><strong>FAQ :</strong></p>
                <ul>
                    <li>Select table show no data. Make sure you have correct database configuration on application/config/database.php and load database library on autoload.</li>
                    <li>Error chmod on mac and linux. Please change your application folder and harviacode folder chmod to 777 </li>
                    <li>Error 404 when click Create, Read, Update, Delete or Next Page. Make sure your mod_rewrite is active
                        and you can access http://localhost/yourproject/welcome. The problem is on htaccess. Still have problem?
                        please go to google and search how to remove index.php codeigniter.
                    </li>
                    <li>Error cannot Read, Update, Delete. Make sure your table have primary key.</li>
                </ul>
                <br>

                <p><strong>Update</strong></p>

                <ul>
                    <li>V.1.4 - 26 November 2016
                        <ul>
                            <li>Change to serverside datatables using ignited datatables</li>
                        </ul>
                    </li>
                    <li>V.1.3.1 - 05 April 2016
                        <ul>
                            <li>Put view files into folder</li>
                        </ul>
                    </li>
                    <li>V.1.3 - 09 December 2015
                        <ul>
                            <li>Zero Config for database connection</li>
                            <li>Fix bug searching</li>
                            <li>Fix field name label</li>
                            <li>Add select table from database</li>
                            <li>Add generate all table</li>
                            <li>Select target folder from setting menu</li>
                            <li>Remove support for Codeigniter 2</li>
                        </ul>
                    </li>
                    <li>V.1.2 - 25 June 2015
                        <ul>
                            <li>Add custom target folder</li>
                            <li>Add export to excel</li>
                            <li>Add export to word</li>
                        </ul>
                    </li>
                    <li>V.1.1 - 21 May 2015
                        <ul>
                            <li>Add custom controller name and custom model name</li>
                            <li>Add client side datatables</li>
                        </ul>
                    </li>
                </ul>

                <p><strong>&COPY; 2015 <a target="_blank" href="http://harviacode.com">harviacode.com</a></strong></p>

        </div>
    </div>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>