<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Check List GIS</title>
</head>
<style>
    body {
        font-size: 10px
    }

    table td {
        padding: 5px
    }
</style>

<body>
    <table border="0" cellspacing="0" cellpadding="0" width="100%" >

        <tr>
            <td align="left">1</td>
        </tr>
    </table><br>
    <table width="100%">
        <tr>
            <td rowspan="3" align="right"><img src="<?= base_url()?>/assets/ap.png" alt=""></td>
        </tr>
        <!-- <tr>
            <td>DIVISI ENERGY & POWER SUPPLY</td>
        </tr>
        <tr>
            <td>DINAS GARDU INDUK</td>
        </tr> -->
    </table>
    <br>
    <table border="0" cellspacing="0" cellpadding="0" width="100%" >
        <tr>
            <td style="font-size:8px"  width="20%">BIDANG FASILITAS BANDARA</td>
            <td style="border: 1px solid black;" width="60%" align="center" rowspan="3">
                <h2>FORM CHECKLIST HARIAN GIS 150Kv BSH</h2>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="font-size:8px">DIVISI ENERGY & POWER SUPPLY</td>
            <td style="font-size:8px">TANGGAL</td>
            <td style="font-size:8px"> : <?= date_indo($header->tanggal)?></td>
        </tr>
        <tr>
            <td style="font-size:8px">DINAS GARDU INDUK </td>
            <td style="font-size:8px">PUKUL</td>
            <td style="font-size:8px"> : <?php echo $header->pukul ?></td>
        </tr>
    </table><br>
    <table border="1" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <th  class="text-center" style="background-color:#6ac4eb  ;font-size:8px" width="3%">NO</th>
            <th class="text-center" style="background-color:#6ac4eb ;font-size:8px" colspan="2">KOMPONEN YANG DIPERIKSA</th>
            <th class="text-center" style="background-color:#6ac4eb ;font-size:8px" colspan="15">KONDISI PERALATAN</th>
        </tr>
        <tr>
            <td colspan="18" height="20"></td>
        </tr>
        <tr>
            <th class="text-center" style="background-color:#6ac4eb font-size:8px">A</th>
            <th class="text-center" style="background-color:#6ac4eb font-size:8px" colspan="2">KONDISI LINGKUNGAN</th>
            <td colspan="15" valign="top">
            </td>
        </tr>
        <tr>
            <th rowspan="4" valign="top" class="text-center">1</th>
            <th rowspan="4" valign="top" width="10%" align="left">INDOOR</th>
            <td width="12%">SUHU AMBIENT</td>
            <td colspan="15"><?=$header->suhu_indoor?></td>
        </tr>
        <tr>
            <td>KELEMBABAN UDARA</td>
            <td colspan="15"><?=$header->kelembaban_indoor?></td>
        </tr>
        <tr>
            <td>BENDA ASING</td>
            <td colspan="15"><?=$header->benda_asing_indoor?></td>
        </tr>
        <tr>
            <td height="20"></td>
        </tr>
        <tr>
            <th rowspan="4" valign="top" class="text-center">2</th>
            <th rowspan="4" valign="top" align="left">OUTDOOR</th>
            <td>SUHU AMBIENT</td>
            <td colspan="15"><?=$header->suhu_outdoor?></td>
        </tr>
        <tr>
            <td>KELEMBABAN UDARA</td>
            <td colspan="15"><?=$header->kelembaban_outdoor?></td>
        </tr>
        <tr>
            <td>BENDA ASING</td>
            <td colspan="15"><?=$header->benda_asing_outdoor?></td>
        </tr>
        <tr>
            <td height="20"></td>
        </tr>
        <tr>
            <th class="text-center" style="background-color:#6ac4eb">B</th>
            <th colspan="2" style="background-color:#6ac4eb">KOMPARTEMEN BAY 150KV</th>
            <th colspan="2" class="kuning text-center">PHT 1</th>
            <th colspan="2" class="kuning text-center">PHT 2</th>
            <th colspan="2" class="kuning text-center">OUT 1</th>
            <th colspan="2" class="kuning text-center">OUT 2</th>
            <th colspan="2" class="kuning text-center">OUT 3</th>
            <th colspan="2" class="kuning text-center">COUPLER </th>
            <th colspan="2" class="kuning text-center">BUS VT</th>
            <th rowspan="2" class="kuning text-center">KETERANGAN</th>
        </tr>
        <tr>
            <th valign="top" class="text-center" rowspan="7">1</th>
            <th colspan="2" align="left">MANOMETER</th>
            <th width="4%" class="text-center">Q1</th>
            <th width="4%" class="text-center">Q2</th>
            <th width="4%" class="text-center">Q1</th>
            <th width="4%" class="text-center">Q2</th>
            <th width="4%" class="text-center">Q1</th>
            <th width="4%" class="text-center">Q2</th>
            <th width="4%" class="text-center">Q1</th>
            <th width="4%" class="text-center">Q2</th>
            <th width="4%" class="text-center">Q1</th>
            <th width="4%" class="text-center">Q2</th>
            <th width="4%" class="text-center">Q1</th>
            <th width="4%" class="text-center">Q2</th>
            <th width="4%" class="text-center">Q1</th>
            <th width="4%" class="text-center">Q2</th>
        </tr>
        <tr>
            <th align="left">(BAR/20°C) </th>
            <td>PMS BUS</td>
            <td><?= $header->pht1_q1_pms_bus?></td>
            <td><?= $header->pht1_q2_pms_bus?></td>
            <td><?= $header->pht2_q1_pms_bus?></td>
            <td><?= $header->pht2_q2_pms_bus?></td>
            <td><?= $header->out1_q1_pms_bus?></td>
            <td><?= $header->out1_q2_pms_bus?></td>
            <td><?= $header->out2_q1_pms_bus?></td>
            <td><?= $header->out2_q2_pms_bus?></td>
            <td><?= $header->out3_q1_pms_bus?></td>
            <td><?= $header->out3_q2_pms_bus?></td>
            <td><?= $header->coupler_q1_pms_bus?></td>
            <td><?= $header->coupler_q2_pms_bus?></td>
            <td><?= $header->bus_vt_q1_pms_bus?></td>
            <td><?= $header->bus_vt_q2_pms_bus?></td>
            <td rowspan="20" valign="top"><?= $header->keterangan_1?>
            </td>
        </tr>
        <tr>
            <th>Tekanan Gas SF6</th>
            <td>PMT</td>
            <td colspan="2"><?= $header->pht1_q1_pmt?></td>
            <!-- <td><?= $header->pht1_q2_pmt?></td> -->
            <td colspan="2"><?= $header->pht2_q1_pmt?></td>
            <!-- <td><?= $header->pht2_q2_pmt?></td> -->
            <td colspan="2"><?= $header->out1_q1_pmt?></td>
            <!-- <td><?= $header->out1_q2_pmt?></td> -->
            <td colspan="2"><?= $header->out2_q1_pmt?></td>
            <!-- <td><?= $header->out2_q2_pmt?></td> -->
            <td colspan="2"><?= $header->out3_q1_pmt?></td>
            <!-- <td><?= $header->out3_q2_pmt?></td> -->
            <td colspan="2"><?= $header->coupler_q1_pmt?></td>
            <!-- <td><?= $header->coupler_q2_pmt?></td> -->
            <td style="background-color:#33332e"></td>
            <td style="background-color:#33332e"></td>
        </tr>
        <tr>
            <th rowspan="4"></th>
            <td>Ceiling End</td>
            <td colspan="2"><?= $header->pht1_q1_ceilingend?></td>
            <!-- <td><?= $header->pht1_q2_ceilingend?></td> -->
            <td colspan="2"><?= $header->pht2_q1_ceilingend?></td>
            <!-- <td><?= $header->pht2_q2_ceilingend?></td> -->
            <td colspan="2"><?= $header->out1_q1_ceilingend?></td>
            <!-- <td><?= $header->out1_q2_ceilingend?></td> -->
            <td colspan="2"><?= $header->out2_q1_ceilingend?></td>
            <!-- <td><?= $header->out2_q2_ceilingend?></td> -->
            <td colspan="2"><?= $header->out3_q1_ceilingend?></td>
            <!-- <td><?= $header->out3_q2_ceilingend?></td> -->
            <td style="background-color:#33332e"></td>
            <td style="background-color:#33332e"></td>
            <td style="background-color:#33332e"></td>
            <td style="background-color:#33332e"></td>
        </tr>
        <tr>
            <td>VT LINE</td>
            <td colspan="2"><?= $header->pht1_q1_vt_line?></td>
            <!-- <td><?= $header->pht1_q2_vt_line?></td> -->
            <td colspan="2"><?= $header->pht2_q1_vt_line?></td>
            <!-- <td><?= $header->pht2_q2_vt_line?></td> -->
            <td style="background-color:#33332e"></td>
            <td style="background-color:#33332e"></td>
            <td style="background-color:#33332e"></td>
            <td style="background-color:#33332e"></td>
            <td style="background-color:#33332e"></td>
            <td style="background-color:#33332e"></td>
            <td style="background-color:#33332e"></td>
            <td style="background-color:#33332e"></td>
            <td style="background-color:#33332e"></td>
            <td style="background-color:#33332e"></td>
        </tr>
        <tr>
            <td>ABNORMAL</td>
            <td colspan="14" rowspan="2"></td>
        </tr>
        <tr>
            <td height="20"></td>
        </tr>

        <tr>
            <th valign="top" rowspan="10">2</th>
            <th>POSISI (OPEN / CLOSE)</th>
            <td>PMS BUS</td>
            <td><?=$header->pht1_q1_pms_bus_posisi==1?"Open":"close"?>
            </td>
            <td><?=$header->pht1_q2_pms_bus_posisi==1?"Open":"close"?>
            </td>
            <td><?=$header->pht2_q1_pms_bus_posisi==1?"Open":"close"?>
            </td>
            <td><?=$header->pht2_q2_pms_bus_posisi==1?"Open":"close"?>
            </td>
            <td><?=$header->out1_q1_pms_bus_posisi==1?"Open":"close"?>
            </td>
            <td><?=$header->out1_q2_pms_bus_posisi==1?"Open":"close"?>
            </td>
            <td><?=$header->out2_q1_pms_bus_posisi==1?"Open":"close"?>
            </td>
            <td><?=$header->out2_q2_pms_bus_posisi==1?"Open":"close"?>
            </td>
            <td><?=$header->out3_q1_pms_bus_posisi==1?"Open":"close"?>
            </td>
            <td><?=$header->out3_q2_pms_bus_posisi==1?"Open":"close"?>
            </td>
            <td><?=$header->coupler_q1_pms_bus_posisi==1?"Open":"close"?>
            </td>
            <td><?=$header->coupler_q2_pms_bus_posisi==1?"Open":"close"?>
            </td>
            <td>NA
            </td>
            <td>NA
            </td>
        </tr>
        <tr>
            <th>Open = O </th>
            <td>PMT</td>
            <td colspan="2"><?=$header->pht1_q1_pmt_posisi==1?"Open":"close"?>
            </td>
            <td colspan="2"><?=$header->pht2_q1_pmt_posisi==1?"Open":"close"?>
            </td>
            <!-- <td><?=$header->pht2_q2_pmt_posisi==1?"Open":"close"?>
            </td> -->
            <td colspan="2"><?=$header->out1_q1_pmt_posisi==1?"Open":"close"?>
            </td>
            <!-- <td><?=$header->out1_q2_pmt_posisi==1?"Open":"close"?>
            </td> -->
            <td colspan="2"><?=$header->out2_q1_pmt_posisi==1?"Open":"close"?>
            </td>
            <!-- <td><?=$header->out2_q2_pmt_posisi==1?"Open":"close"?>
            </td> -->
            <td colspan="2"><?=$header->out3_q1_pmt_posisi==1?"Open":"close"?>
            </td>
            <!-- <td><?=$header->out3_q2_pmt_posisi==1?"Open":"close"?>
            </td> -->
            <td colspan="2"><?=$header->coupler_q1_pmt_posisi==1?"Open":"close"?>
            </td>
            <!-- <td><?=$header->coupler_q2_pmt_posisi==1?"Open":"close"?>
            </td> -->
            <td>NA
            </td>
            <td>NA
            </td>
        </tr>
        <tr>
            <th rowspan="5" valign="top">Close = C</th>
            <td rowspan="3" valign="top">COUNTER PMT</td>
            <td colspan="2"> R <?=$header->pht1_counter_pmt_r?>
                </div>
            </td>
            <td colspan="2"> R <?=$header->pht2_counter_pmt_r?>
                </div>
            </td>
            <td colspan="2"> R <?=$header->out1_counter_pmt_r?>
                </div>
            </td>
            <td colspan="2"> R <?=$header->out2_counter_pmt_r?>
                </div>
            </td>
            <td colspan="2"> R <?=$header->out3_counter_pmt_r?>
                </div>
            </td>
            <td colspan="2"> R <?=$header->coupler_counter_pmt_r?>
                </div>
            </td>
            <td colspan="2"> R <?=$header->bus_vt_counter_pmt_r?>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2"> S <?=$header->pht1_counter_pmt_s?>
                </div>
            </td>
            <td colspan="2"> S <?=$header->pht2_counter_pmt_s?>
                </div>
            </td>
            <td colspan="2"> S <?=$header->out1_counter_pmt_s?>
                </div>
            </td>
            <td colspan="2"> S <?=$header->out2_counter_pmt_s?>
                </div>
            </td>
            <td colspan="2"> S <?=$header->out3_counter_pmt_s?>
                </div>
            </td>
            <td colspan="2"> S <?=$header->coupler_counter_pmt_s?>
                </div>
            </td>
            <td colspan="2"> S <?=$header->bus_vt_counter_pmt_s?>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2"> T <?=$header->pht1_counter_pmt_t?>
                </div>
            </td>
            <td colspan="2"> T <?=$header->pht2_counter_pmt_t?>
                </div>
            </td>
            <td colspan="2"> T <?=$header->out1_counter_pmt_t?>
                </div>
            </td>
            <td colspan="2"> T <?=$header->out2_counter_pmt_t?>
                </div>
            </td>
            <td colspan="2"> T <?=$header->out3_counter_pmt_t?>
                </div>
            </td>
            <td colspan="2"> T <?=$header->coupler_counter_pmt_t?>
                </div>
            </td>
            <td colspan="2"> T <?=$header->bus_vt_counter_pmt_t?>
                </div>
            </td>
        </tr>
        <tr>
            <td>PMS LINE</td>
            <td colspan="2"><?= $header->pms_line_pht1==0 ? "CLose":"Open"?>
            <td  colspan="2" ><?= $header->pms_line_pht2==0 ? "CLose":"Open"?>
            <td colspan="10" style="background-color:#33332e"></td>
        </tr>
        <tr>
            <td>EARTHING BUSBAR</td>
            <th colspan="3" class="text-center">Q 15</th>
            <td colspan="3"><?= $header->q15==0 ? "CLose":"Open"?>
            </td>
            <th colspan="4" class="text-center">Q 25</th>
            <td colspan="4"><?= $header->q25==0 ? "CLose":"Open"?>
            </td>
        </tr>
        <tr>
            <th class="text-center">Q51 &Q52</th>
            <td>EARTHING PMT</td>
            <td colspan="2"><?= $header->pht1_earthing_pmt==0 ? "CLose":"Open"?>
            </td>
            <td colspan="2"><?= $header->pht2_earthing_pmt==0 ? "CLose":"Open"?>
            </td>
            <td colspan="2"><?= $header->out1_earthing_pmt==0 ? "CLose":"Open"?>
            </td>
            <td colspan="2"><?= $header->out2_earthing_pmt==0 ? "CLose":"Open"?>
            </td>
            <td colspan="2"><?= $header->out3_earthing_pmt==0 ? "CLose":"Open"?>
            </td>
            <td colspan="2"><?= $header->coupler_earthing_pmt==0 ? "CLose":"Open"?>
            </td>
            <td colspan="2" style="background-color:#33332e"></td>
        </tr>
        <tr>
            <th class="text-center">Q8</th>
            <td>EARTHING LINE</td>
            <td colspan="2"><?= $header->pht1_earthing_line==0 ? "CLose":"Open"?>
            </td>
            <td colspan="2"><?= $header->pht2_earthing_line==0 ? "CLose":"Open"?>
            </td>
            <td colspan="10" style="background-color:#33332e"></td>
        </tr>
        <tr>
            <td colspan="16" height="20"></td>
        </tr>
        <tr>
            <th rowspan="4" valign="top">3</th>
            <th rowspan="3" valign="top">STATUS AC/DC</th>
            <td>DC HEALTH 110V</td>
            <td colspan="7">
                <div>
                    <span style="float: left; margin-right:10px">Rectifier 1</span>
                    <span><?=$header->dh110_r1?></span>
                    <span style="margin-left:10px">Volt</span>
                </div>
            </td>
            <td colspan="7">
                <div>
                    <span style="float: left; margin-right:10px">Rectifier 2</span>
                    <span><?=$header->dh110_r2?></span>
                    <span style="margin-left:10px">Volt</span>
                </div>
            </td>
        </tr>
        <tr>
            <td>DC HEALTH 48V</td>
            <td colspan="7">
                <div>
                    <span style="float: left; margin-right:10px">Rectifier 1</span>
                    <span><?=$header->dh48_r1?></span>
                    <span style="margin-left:10px">Volt</span>
                </div>
            </td>
            <td colspan="7">
                <div>
                    <span style="float: left; margin-right:10px">Rectifier 2</span>
                    <span><?=$header->dh48_r2?></span>
                    <span style="margin-left:10px">Volt</span>
                </div>
            </td>
        </tr>
        <tr>
            <td>LOCAL / REMOTE</td>
            <td colspan="7"><?= $header->dh110_lr==0 ? "Local":"Remote"?>
            </td>
            <td colspan="7"><?= $header->dh48_lr==0 ? "Local":"Remote"?>
            </td>
        </tr>
        <tr>
            <td colspan="16" height="0"></td>
        </tr></table>
<div style="page-break-after: always;"></div>
<table border="0" cellspacing="0" cellpadding="0" width="100%" >

<tr>
    <td align="left">2</td>
</tr>
</table><br>
    <table border="1" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <th style="background-color:#6ac4eb">C</th>
            <th style="background-color:#6ac4eb" colspan="2">TRAFO 150KV 60MVA</th>
            <th class="kuning" colspan="2">TRAFO 1</th>
            <th class="kuning" colspan="2">OLTC 1</th>
            <th class="kuning" colspan="2">TRAFO 2</th>
            <th class="kuning" colspan="2">OLTC 2</th>
            <th class="kuning" colspan="2">TRAFO PS</th>
            <th class="kuning" colspan="5">KETERANGAN</th>
        </tr>
        <tr>
            <th rowspan="5" valign="top">1</th>
            <td>MINYAK TRAFO</td>
            <td>LEVEL</td>
            <td colspan="2"><?= $header->trafo1_level==0 ? "Normal":"Abnormal"?>
            </td>
            <td colspan="2"><?= $header->oltc1_level==0 ? "Normal":"Abnormal"?>
            </td>
            <td colspan="2"><?= $header->trafo2_level==0 ? "Normal":"Abnormal"?>
            </td>
            <td colspan="2"><?= $header->oltc2_level==0 ? "Normal":"Abnormal"?>
            </td>
            <td colspan="2"><?= $header->trafo_ps_level==0 ? "Normal":"Abnormal"?>
            </td>
            <td rowspan="15" colspan="5" valign="top"><?= $header->keterangan_2?>
            </td>
        </tr>
        <tr>
            <td>N = NORMAL</td>
            <th style="background-color:#6ac4eb">SUHU</th>
            <td colspan="2" style="background-color:#6ac4eb"></td>
            <td colspan="2"><?= $header->oltc1_suhu?> &#8451;</td>
            <td colspan="2" style="background-color:#6ac4eb"></td>
            <td colspan="2"><?= $header->oltc2_suhu?> &#8451;</td>
            <td colspan="2"><?= $header->trafo_ps_suhu?> &#8451;</td>
        </tr>
        <tr>
            <td rowspan="3" valign="top">TN=TIDAK NORMAL</td>
            <td>OIL TEMP</td>
            <td colspan="2">
                <?= $header->trafo1_oil?> &#8451;</td>
            <td rowspan="12" style="background-color:#6ac4eb" colspan="2"></td>
            <td colspan="2">
                <?= $header->trafo2_oil?> &#8451;</td>
            <td rowspan="12" style="background-color:#6ac4eb" colspan="4"></td>
        </tr>
        <tr>
            <td>WINDING HV</td>
            <td colspan="2">
                <?= $header->trafo1_hv?> &#8451;</td>
            <td colspan="2">
                <?= $header->trafo2_hv?> &#8451;</td>
        </tr>
        <tr>
            <td>WINDING LV</td>
            <td colspan="2">
                <?= $header->trafo1_lv?> &#8451;</td>
            <td colspan="2">
                <?= $header->trafo2_lv?> &#8451;</td>
        </tr>
        <tr>
            <th>2</th>
            <td colspan="2">POSISI TAP</td>
            <td colspan="2"><?= $header->trafo1_posisi_tap ?></td>
            <td colspan="2"><?= $header->trafo2_posisi_tap ?></td>
        </tr>
        <tr>
            <th>3</th>
            <td colspan="2">COUNTER OLTC</td>
            <td colspan="2"><?= $header->trafo1_counter_oltc ?></td>
            <td colspan="2"><?= $header->trafo2_counter_oltc ?></td>
        </tr>
        <tr>
            <th rowspan="7" valign="top">4</th>
            <td>PROTEKSI</td>
            <td>BUCHOLZ</td>
            <td colspan="2"><?= $header->trafo1_bucholz==0 ? "Normal":"Abnormal"?>
            </td>
            <td colspan="2">
<?= $header->trafo2_bucholz==0 ? "Normal":"Abnormal"?>
            </td>
        </tr>
        <tr>
            <td>N = NORMAL</td>
            <td>JANSEN</td>
            <td colspan="2"><?= $header->trafo1_jansen==0 ? "Normal":"Abnormal"?>
            </td>
            <td colspan="2">
<?= $header->trafo2_jansen==0 ? "Normal":"Abnormal"?>
            </td>
        </tr>
        <tr>
            <td rowspan="5" valign="top">TN = TIDAK NORMAL</td>
            <td>TERMAL</td>
            <td colspan="2"><?= $header->trafo1_termal==0 ? "Normal":"Abnormal"?>
            </td>
            <td colspan="2">
<?= $header->trafo2_termal==0 ? "Normal":"Abnormal"?>
            </td>
        </tr>
        <tr>
            <td>SUDDEN PRESS</td>
            <td colspan="2"><?= $header->trafo1_sudden_press==0 ? "Normal":"Abnormal"?>
            </td>
            <td colspan="2">
<?= $header->trafo2_sudden_press==0 ? "Normal":"Abnormal"?>
            </td>
        </tr>
        <tr>
            <td>FIRE PREVENTION (BAR)</td>
            <td colspan="2"><?= $header->trafo1_fire==0 ? "Normal":"Abnormal"?>
            </td>
            <td colspan="2">
<?= $header->trafo2_fire==0 ? "Normal":"Abnormal"?>
            </td>
        </tr>
        <tr>
            <td>NGR (ARUS)</td>
            <td colspan="2"><?= $header->trafo1_ngr==0 ? "Normal":"Abnormal"?>
            </td>
            <td colspan="2">
<?= $header->trafo2_ngr==0 ? "Normal":"Abnormal"?>
            </td>
        </tr>
        <tr>
            <td>DC HEALTH</td>
            <td colspan="2"><?= $header->trafo1_dc==0 ? "Normal":"Abnormal"?>
            </td>
            <td colspan="2">
<?= $header->trafo2_dc==0 ? "Normal":"Abnormal"?>
            </td>
        </tr>
    </table>
    <table border='0' width='100%'>
    <tbody>
        <tr>
            <td align="left"> ASSISTANT MANAGER OF HIGH & MEDIUM VOLTAGE STATION
                <br><br><br><br>
                <img width="80" height="80" src="<?= base_url()?>/assets/qrcode/assisten/<?=qr_a($header->assistant_manager) ?>" alt=""><br>(<?= $header->assistant_manager?>)
            </td>
            <td align="left">Tangerang, <?= date_indo($header->tanggal)?>
                <br>PETUGAS DINAS
                <br>
                <br>
                <br>
                <br>
                <img width="80" height="80" src="<?= base_url()?>/assets/qrcode/petugas/<?=qr_b($header->petugas_dinas) ?>" alt="">
                <br>(<?= $header->petugas_dinas?>)

            </td>
        </tr>
    </tbody>
</table>

</body>
<style>
    .kuning {
        background-color: #f4fc03
    }

    td,
    th {
        font-size: 9px;
        padding: 3px
    }
</style>

</html>