<style>
    .kuning {
        background-color: #f4fc03
    }

    table td {
        position: relative;
    }

    td,
    th {
        font-size: 10px;
        padding: 3px
    }

    table td .input-full {
        position: absolute;
        display: block;
        top: 0;
        left: 0;
        margin: 0;
        height: 100%;
        width: 100%;
        border: none;
        padding: 2px;
        box-sizing: border-box;
        box-shadow: 0 0 3px #CC0000;
    }

    .input-border {
        box-shadow: 0 0 3px #CC0000;
    }

    .input {
        position: absolute;
        display: block;
        top: 0;
        left: 0;
        margin: 0;
        height: 100%;
        width: 100%;
        border: none;
        padding: 2px;
        box-sizing: border-box;
        box-shadow: 0 0 3px #CC0000;
    }

    select {
        border: 0;
        width: 100%;
    }

    textarea {
        width: 100%;
        box-shadow: 0 0 3px #CC0000;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        position: absolute;
        resize: none;
        -webkit-box-sizing: border-box;
        /* <=iOS4, <= Android  2.3 */
        -moz-box-sizing: border-box;
        /* FF1+ */
        box-sizing: border-box;
        /* Chrome, IE8, Opera, Safari 5.1*/
        padding: 2px;
    }

    .input-container {
        position: relative;
        width: 150px;
    }

    .input-container input {
        width: 100%;
    }

    .input-container .unit {
        position: absolute;
        display: block;
        top: 3px;
        right: -3px;
        background-color: grey;
        color: #ffffff;
        padding-left: 5px;
        width: 45px;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <form action="<?php echo $action; ?>" method="post" role="form">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="panel"> ASSISTANT MANAGER <sup style="color:red;">*</sup></label>
                                    <select required name="assistant_manager" id="assistant_manager"
                                        class="form-control">
                                        <option value="">Pilih</option>
                                        <?php foreach ($asisten_list as $rk) { ?>
                                        <option <?= $rk->nama==$assistant_manager ? "selected":""?>
                                            value="<?= $rk->nama?>"><?= $rk->nama?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="panel"> PETUGAS DINAS<sup style="color:red;">*</sup></label>
                                    <select required name="petugas_dinas" id="petugas_dinas" class="form-control">
                                        <option value="">Pilih</option>
                                        <?php foreach ($petugas_list as $rk) { ?>
                                        <option <?= $rk->nama==$petugas_dinas ? "selected":""?> value="<?= $rk->nama?>">
                                            <?= $rk->nama?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <table border="1" width="100%" class="hoverTable">
                            <tr>
                                <th class="text-center" style="background-color:#6ac4eb ">NO</th>
                                <th class="text-center" style="background-color:#6ac4eb" colspan="2">KOMPONEN YANG
                                    DIPERIKSA</th>
                                <th class="text-center" style="background-color:#6ac4eb" colspan="15">KONDISI PERALATAN
                                </th>
                            </tr>
                            <tr>
                                <td colspan="18" height="20"></td>
                            </tr>
                            <tr>
                                <th class="text-center" style="background-color:#6ac4eb ">A</th>
                                <th class="text-center" style="background-color:#6ac4eb " colspan="2">KONDISI LINGKUNGAN
                                </th>
                                <td colspan="15">
                                </td>
                            </tr>
                            <tr>
                                <th rowspan="4" valign="top" class="text-center">1</th>
                                <th rowspan="4" valign="top">INDOOR</th>
                                <td>SUHU AMBIENT</td>
                                <td colspan="15"><input type="text" value="<?php echo $suhu_indoor; ?>" class="input-full" name="suhu_indoor"></td>
                            </tr>
                            <tr>
                                <td>KELEMBABAN UDARA</td>
                                <td colspan="15"><input type="text" value="<?php echo $kelembaban_indoor; ?>" class="input-full" name="kelembaban_indoor"></td>
                            </tr>
                            <tr>
                                <td>BENDA ASING</td>
                                <td colspan="15"><input type="text" value="<?php echo $benda_asing_indoor; ?>" class="input-full" name="benda_asing_indoor"></td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <th rowspan="4" valign="top" class="text-center">2</th>
                                <th rowspan="4" valign="top">OUTDOOR</th>
                                <td>SUHU AMBIENT</td>
                                <td colspan="15"><input type="text" value="<?php echo $suhu_outdoor; ?>" class="input-full" name="suhu_outdoor"></td>
                            </tr>
                            <tr>
                                <td>KELEMBABAN UDARA</td>
                                <td colspan="15"><input type="text" value="<?php echo $kelembaban_outdoor; ?>" class="input-full" name="kelembaban_outdoor"></td>
                            </tr>
                            <tr>
                                <td>BENDA ASING</td>
                                <td colspan="15"><input type="text" value="<?php echo $benda_asing_outdoor; ?>" class="input-full" name="benda_asing_outdoor"></td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <th class="text-center" style="background-color:#6ac4eb">B</th>
                                <th colspan="2" style="background-color:#6ac4eb">KOMPARTEMEN BAY 150KV</th>
                                <th colspan="2" class="kuning text-center">PHT 1</th>
                                <th colspan="2" class="kuning text-center">PHT 2</th>
                                <th colspan="2" class="kuning text-center">OUT 1</th>
                                <th colspan="2" class="kuning text-center">OUT 2</th>
                                <th colspan="2" class="kuning text-center">OUT 3</th>
                                <th colspan="2" class="kuning text-center">COUPLER </th>
                                <th colspan="2" class="kuning text-center">BUS VT</th>
                                <th rowspan="2" class="kuning text-center">KETERANGAN</th>
                            </tr>
                            <tr>
                                <th valign="top" class="text-center" rowspan="7">1</th>
                                <th colspan="2">MANOMETER</th>
                                <th width="6%" class="text-center">Q1</th>
                                <th width="6%" class="text-center">Q2</th>
                                <th width="6%" class="text-center">Q1</th>
                                <th width="6%" class="text-center">Q2</th>
                                <th width="6%" class="text-center">Q1</th>
                                <th width="6%" class="text-center">Q2</th>
                                <th width="6%" class="text-center">Q1</th>
                                <th width="6%" class="text-center">Q2</th>
                                <th width="6%" class="text-center">Q1</th>
                                <th width="6%" class="text-center">Q2</th>
                                <th width="6%" class="text-center">Q1</th>
                                <th width="6%" class="text-center">Q2</th>
                                <th width="6%" class="text-center">Q1</th>
                                <th width="6%" class="text-center">Q2</th>
                            </tr>
                            <tr>
                                <th width="8%">(BAR/20°C) </th>
                                <td width="8%">PMS BUS</td>
                                <td><input class="input-full number" type="text" value="<?= $pht1_q1_pms_bus?>"
                                        name="pht1_q1_pms_bus"></td>
                                <td><input class="input-full number" type="text" value="<?= $pht1_q2_pms_bus?>"
                                        name="pht1_q2_pms_bus"></td>
                                <td><input class="input-full number" type="text" value="<?= $pht2_q1_pms_bus?>"
                                        name="pht2_q1_pms_bus"></td>
                                <td><input class="input-full number" type="text" value="<?= $pht2_q2_pms_bus?>"
                                        name="pht2_q2_pms_bus"></td>
                                <td><input class="input-full number" type="text" value="<?= $out1_q1_pms_bus?>"
                                        name="out1_q1_pms_bus"></td>
                                <td><input class="input-full number" type="text" value="<?= $out1_q2_pms_bus?>"
                                        name="out1_q2_pms_bus"></td>
                                <td><input class="input-full number" type="text" value="<?= $out2_q1_pms_bus?>"
                                        name="out2_q1_pms_bus"></td>
                                <td><input class="input-full number" type="text" value="<?= $out2_q2_pms_bus?>"
                                        name="out2_q2_pms_bus"></td>
                                <td><input class="input-full number" type="text" value="<?= $out3_q1_pms_bus?>"
                                        name="out3_q1_pms_bus"></td>
                                <td><input class="input-full number" type="text" value="<?= $out3_q2_pms_bus?>"
                                        name="out3_q2_pms_bus"></td>
                                <td><input class="input-full number" type="text" value="<?= $coupler_q1_pms_bus?>"
                                        name="coupler_q1_pms_bus"></td>
                                <td><input class="input-full number" type="text" value="<?= $coupler_q2_pms_bus?>"
                                        name="coupler_q2_pms_bus"></td>
                                <td><input class="input-full number" type="text" value="<?= $bus_vt_q1_pms_bus?>"
                                        name="bus_vt_q1_pms_bus"></td>
                                <td><input class="input-full number" type="text" value="<?= $bus_vt_q2_pms_bus?>"
                                        name="bus_vt_q2_pms_bus"></td>
                                <td rowspan="20">
                                    <textarea name="keterangan_1"><?= $keterangan_1?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <th>Tekanan Gas SF6</th>
                                <td>PMT</td>
                                <td colspan="2"><input class="input-full number" type="text" value="<?= $pht1_q1_pmt?>"
                                        name="pht1_q1_pmt"></td>
                                <td colspan="2"><input class="input-full number" type="text" value="<?= $pht2_q1_pmt?>"
                                        name="pht2_q1_pmt"></td>
                                <td colspan="2"><input class="input-full number" type="text" value="<?= $out1_q1_pmt?>"
                                        name="out1_q1_pmt"></td>
                                <td colspan="2"><input class="input-full number" type="text" value="<?= $out2_q1_pmt?>"
                                        name="out2_q1_pmt"></td>
                                <td colspan="2"><input class="input-full number" type="text" value="<?= $out3_q1_pmt?>"
                                        name="out3_q1_pmt"></td>
                                <td colspan="2"><input class="input-full number" type="text"
                                        value="<?= $coupler_q1_pmt?>" name="coupler_q1_pmt"></td>
                                <td style="background-color:#33332e"></td>
                                <td style="background-color:#33332e"></td>
                            </tr>
                            <tr>
                                <th rowspan="4"></th>
                                <td>Ceiling End</td>
                                <td colspan="2"><input class="input-full number" type="text"
                                        value="<?= $pht1_q1_ceilingend?>" name="pht1_q1_ceilingend"></td>
                                <td colspan="2"><input class="input-full number" type="text"
                                        value="<?= $pht2_q1_ceilingend?>" name="pht2_q1_ceilingend"></td>
                                <td colspan="2"><input class="input-full number" type="text"
                                        value="<?= $out1_q1_ceilingend?>" name="out1_q1_ceilingend"></td>
                                <td colspan="2"><input class="input-full number" type="text"
                                        value="<?= $out2_q1_ceilingend?>" name="out2_q1_ceilingend"></td>
                                <td colspan="2"><input class="input-full number" type="text"
                                        value="<?= $out3_q1_ceilingend?>" name="out3_q1_ceilingend"></td>
                                <td style="background-color:#33332e"></td>
                                <td style="background-color:#33332e"></td>
                                <td style="background-color:#33332e"></td>
                                <td style="background-color:#33332e"></td>
                            </tr>
                            <tr>
                                <td>VT LINE</td>
                                <td colspan="2"><input class="input-full number" type="text"
                                        value="<?= $pht1_q1_vt_line?>" name="pht1_q1_vt_line"></td>
                                <td colspan="2"><input class="input-full number" type="text"
                                        value="<?= $pht2_q1_vt_line?>" name="pht2_q1_vt_line"></td>
                                <td style="background-color:#33332e"></td>
                                <td style="background-color:#33332e"></td>
                                <td style="background-color:#33332e"></td>
                                <td style="background-color:#33332e"></td>
                                <td style="background-color:#33332e"></td>
                                <td style="background-color:#33332e"></td>
                                <td style="background-color:#33332e"></td>
                                <td style="background-color:#33332e"></td>
                                <td style="background-color:#33332e"></td>
                                <td style="background-color:#33332e"></td>
                            </tr>
                            <tr>
                                <td>ABNORMAL</td>
                                <td colspan="14" rowspan="2"></td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <th valign="top" rowspan="10">2</th>
                                <th>POSISI (OPEN / CLOSE)</th>
                                <td>PMS BUS</td>
                                <td>
                                    <select name="pht1_q1_pms_bus_posisi" id="">
                                        <option <?php if($pht1_q1_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($pht1_q1_pms_bus_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="pht1_q2_pms_bus_posisi" id="">
                                        <option <?php if($pht1_q2_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($pht1_q2_pms_bus_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="pht2_q1_pms_bus_posisi" id="">
                                        <option <?php if($pht2_q1_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($pht2_q1_pms_bus_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="pht2_q2_pms_bus_posisi" id="">
                                        <option <?php if($pht2_q2_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($pht2_q2_pms_bus_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="out1_q1_pms_bus_posisi" id="">
                                        <option <?php if($out1_q1_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($out1_q1_pms_bus_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="out1_q2_pms_bus_posisi" id="">
                                        <option <?php if($out1_q2_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($out1_q2_pms_bus_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="out2_q1_pms_bus_posisi" id="">
                                        <option <?php if($out2_q1_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($out2_q1_pms_bus_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="out2_q2_pms_bus_posisi" id="">
                                        <option <?php if($out2_q2_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($out2_q2_pms_bus_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="out3_q1_pms_bus_posisi" id="">
                                        <option <?php if($out3_q1_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($out3_q1_pms_bus_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="out3_q2_pms_bus_posisi" id="">
                                        <option <?php if($out3_q2_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($out3_q2_pms_bus_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="coupler_q1_pms_bus_posisi" id="">
                                        <option <?php if($coupler_q1_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($coupler_q1_pms_bus_posisi==1){ echo "selected";}?> value="1">
                                            Open</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="coupler_q2_pms_bus_posisi" id="">
                                        <option <?php if($coupler_q2_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($coupler_q2_pms_bus_posisi==1){ echo "selected";}?> value="1">
                                            Open</option>
                                    </select>
                                </td>
                                <td>
                                    NA
                                    <!-- <select name="bus_vt_q1_pms_bus_posisi" id="">
                                        <option <?php if($bus_vt_q1_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($bus_vt_q1_pms_bus_posisi==1){ echo "selected";}?> value="1">
                                            Open</option>
                                    </select> -->
                                </td>
                                <td>
                                    NA
                                    <!-- <select name="bus_vt_q2_pms_bus_posisi" id="">
                                        <option <?php if($bus_vt_q2_pms_bus_posisi==0){ echo "selected";}?> value="0">
                                            Close</option>
                                        <option <?php if($bus_vt_q2_pms_bus_posisi==1){ echo "selected";}?> value="1">
                                            Open</option>
                                    </select> -->
                                </td>
                            </tr>
                            <tr>
                                <th>Open = O </th>
                                <td>PMT</td>
                                <td colspan="2">
                                    <select name="pht1_q1_pmt_posisi" id="">
                                        <option <?php if($pht1_q1_pmt_posisi==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($pht1_q1_pmt_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">
                                    <select name="pht2_q1_pmt_posisi" id="">
                                        <option <?php if($pht2_q1_pmt_posisi==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($pht2_q1_pmt_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <!-- <td>
                                    <select name="pht2_q2_pmt_posisi" id="">
                                        <option <?php if($pht2_q2_pmt_posisi==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($pht2_q2_pmt_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td> -->
                                <td colspan="2">
                                    <select name="out1_q1_pms_bus_posisi" id="">
                                        <option <?php if($out1_q1_pmt_posisi==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($out1_q1_pmt_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <!-- <td>
                                    <select name="out1_q2_pmt_posisi" id="">
                                        <option <?php if($out1_q2_pmt_posisi==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($out1_q2_pmt_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td> -->
                                <td colspan="2">
                                    <select name="out2_q1_pmt_posisi" id="">
                                        <option <?php if($out2_q1_pmt_posisi==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($out2_q1_pmt_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <!-- <td>
                                    <select name="out2_q2_pmt_posisi" id="">
                                        <option <?php if($out2_q2_pmt_posisi==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($out2_q2_pmt_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td> -->
                                <td colspan="2">
                                    <select name="out3_q1_pmt_posisi" id="">
                                        <option <?php if($out3_q1_pmt_posisi==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($out3_q1_pmt_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <!-- <td>
                                    <select name="out3_q2_pmt_posisi" id="">
                                        <option <?php if($out3_q2_pmt_posisi==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($out3_q2_pmt_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td> -->
                                <td colspan="2">
                                    <select name="coupler_q1_pmt_posisi" id="">
                                        <option <?php if($coupler_q1_pmt_posisi==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($coupler_q1_pmt_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <!-- <td>
                                    <select name="coupler_q2_pmt_posisi" id="">
                                        <option <?php if($coupler_q2_pmt_posisi==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($coupler_q2_pmt_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td> -->
                                <td> NA
                                    <!-- <select name="bus_vt_q1_pmt_posisi" id="">
                                        <option <?php if($bus_vt_q1_pmt_posisi==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($bus_vt_q1_pmt_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select> -->
                                </td>
                                <td> NA
                                    <!-- <select name="bus_vt_q2_pmt_posisi" id="">
                                        <option <?php if($bus_vt_q2_pmt_posisi==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($bus_vt_q2_pmt_posisi==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select> -->
                                </td>
                            </tr>
                            <tr>
                                <th rowspan="5" valign="top">Close = C</th>
                                <td rowspan="3" valign="top">COUNTER PMT</td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">R</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="pht1_counter_pmt_r"
                                                value="<?=$pht1_counter_pmt_r?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">R</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="pht2_counter_pmt_r"
                                                value="<?=$pht2_counter_pmt_r?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">R</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="out1_counter_pmt_r"
                                                value="<?=$out1_counter_pmt_r?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">R</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="out2_counter_pmt_r"
                                                value="<?=$out2_counter_pmt_r?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">R</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="out3_counter_pmt_r"
                                                value="<?=$out3_counter_pmt_r?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">R</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="coupler_counter_pmt_r"
                                                value="<?=$coupler_counter_pmt_r?>" class="input-border"
                                                size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">R</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="bus_vt_counter_pmt_r"
                                                value="<?=$bus_vt_counter_pmt_r?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">S</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="pht1_counter_pmt_s"
                                                value="<?=$pht1_counter_pmt_s?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">S</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="pht2_counter_pmt_s"
                                                value="<?=$pht2_counter_pmt_s?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">S</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="out1_counter_pmt_s"
                                                value="<?=$out1_counter_pmt_s?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">S</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="out2_counter_pmt_s"
                                                value="<?=$out2_counter_pmt_s?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">S</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="out3_counter_pmt_s"
                                                value="<?=$out3_counter_pmt_s?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">S</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="coupler_counter_pmt_s"
                                                value="<?=$coupler_counter_pmt_s?>" class="input-border"
                                                size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">S</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="bus_vt_counter_pmt_s"
                                                value="<?=$bus_vt_counter_pmt_s?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">T</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="pht1_counter_pmt_t"
                                                value="<?=$pht1_counter_pmt_t?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">T</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="pht2_counter_pmt_t"
                                                value="<?=$pht2_counter_pmt_t?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">T</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="out1_counter_pmt_t"
                                                value="<?=$out1_counter_pmt_t?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">T</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="out2_counter_pmt_t"
                                                value="<?=$out2_counter_pmt_t?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">T</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="out3_counter_pmt_t"
                                                value="<?=$out3_counter_pmt_t?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">T</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="coupler_counter_pmt_t"
                                                value="<?=$coupler_counter_pmt_t?>" class="input-border"
                                                size="4"></span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span style="float: left;">T</span>
                                        <span style="float: right;"><input type="text"
                                                onkeypress="return isNumberKey(event)" name="bus_vt_counter_pmt_t"
                                                value="<?=$bus_vt_counter_pmt_t?>" class="input-border" size="4"></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>PMS LINE</td>
                                <td colspan="2">
                                    <select name="pms_line_pht1" id="">
                                        <option <?php if($pms_line_pht1==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($pms_line_pht1==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">
                                    <select name="pms_line_pht2" id="">
                                        <option <?php if($pms_line_pht2==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($pms_line_pht2==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td style="background-color:#33332e" colspan="10"></td>
                            </tr>
                            <tr>
                                <td>EARTHING BUSBAR</td>
                                <th colspan="3" class="text-center">Q 15</th>
                                <td colspan="3">

                                    <select name="q15" id="">
                                        <option <?php if($q15==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($q15==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <th colspan="4" class="text-center">Q 25</th>
                                <td colspan="4">

                                    <select name="q25" id="">
                                        <option <?php if($q25==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($q25==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">Q51 &Q52</th>
                                <td>EARTHING PMT</td>
                                <td>
                                    <select name="pht1_earthing_pmt" id="">
                                        <option <?php if($pht1_earthing_pmt==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($pht1_earthing_pmt==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="pht1_earthing_pmt_q2" id="">
                                        <option <?php if($pht1_earthing_pmt_q2==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($pht1_earthing_pmt_q2==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="pht2_earthing_pmt" id="">
                                        <option <?php if($pht2_earthing_pmt==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($pht2_earthing_pmt==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="pht2_earthing_pmt_q2" id="">
                                        <option <?php if($pht2_earthing_pmt_q2==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($pht2_earthing_pmt_q2==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="out1_earthing_pmt" id="">
                                        <option <?php if($out1_earthing_pmt==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($out1_earthing_pmt==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="out1_earthing_pmt_q2" id="">
                                        <option <?php if($out1_earthing_pmt_q2==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($out1_earthing_pmt_q2==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="out2_earthing_pmt" id="">
                                        <option <?php if($out2_earthing_pmt==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($out2_earthing_pmt==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="out2_earthing_pmt_q2" id="">
                                        <option <?php if($out2_earthing_pmt_q2==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($out2_earthing_pmt_q2==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="out3_earthing_pmt" id="">
                                        <option <?php if($out3_earthing_pmt==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($out3_earthing_pmt==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="out3_earthing_pmt_q2" id="">
                                        <option <?php if($out3_earthing_pmt_q2==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($out3_earthing_pmt_q2==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="coupler_earthing_pmt" id="">
                                        <option <?php if($coupler_earthing_pmt==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($coupler_earthing_pmt==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select name="coupler_earthing_pmt_q2" id="">
                                        <option <?php if($coupler_earthing_pmt_q2==0){ echo "selected";}?> value="0">
                                            Close
                                        </option>
                                        <option <?php if($coupler_earthing_pmt_q2==1){ echo "selected";}?> value="1">
                                            Open
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2" style="background-color:#33332e"></td>
                            </tr>
                            <tr>
                                <th class="text-center">Q8</th>
                                <td>EARTHING LINE</td>
                                <td colspan="2">
                                    <select name="pht1_earthing_line" id="">
                                        <option <?php if($pht1_earthing_line==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($pht1_earthing_line==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">
                                    <select name="pht2_earthing_line" id="">
                                        <option <?php if($pht2_earthing_line==0){ echo "selected";}?> value="0">Close
                                        </option>
                                        <option <?php if($pht2_earthing_line==1){ echo "selected";}?> value="1">Open
                                        </option>
                                    </select>
                                </td>
                                <td colspan="10" style="background-color:#33332e"></td>
                            </tr>
                            <tr>
                                <td colspan="16" height="20"></td>
                            </tr>
                            <tr>
                                <th rowspan="4" valign="top">3</th>
                                <th rowspan="3" valign="top">STATUS AC/DC</th>
                                <td>DC HEALTH 110V</td>
                                <td colspan="7">
                                    <div>
                                        <span style="float: left; margin-right:10px">Rectifier 1</span>
                                        <span><input type="text" name="dh110_r1" value="<?=$dh110_r1?>"
                                                class="input-borde numberr" size="4"></span>
                                        <span style="margin-left:10px">Volt</span>
                                    </div>
                                </td>
                                <td colspan="7">
                                    <div>
                                        <span style="float: left; margin-right:10px">Rectifier 2</span>
                                        <span><input type="text" name="dh110_r2" value="<?=$dh110_r2?>"
                                                class="input-borde numberr" size="4"></span>
                                        <span style="margin-left:10px">Volt</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>DC HEALTH 48V</td>
                                <td colspan="7">
                                    <div>
                                        <span style="float: left; margin-right:10px">Rectifier 1</span>
                                        <span><input type="text" name="dh48_r1" value="<?=$dh48_r1?>"
                                                class="input-border number" size="4"></span>
                                        <span style="margin-left:10px">Volt</span>
                                    </div>
                                </td>
                                <td colspan="7">
                                    <div>
                                        <span style="float: left; margin-right:10px">Rectifier 2</span>
                                        <span><input type="text" name="dh48_r2" value="<?=$dh48_r2?>"
                                                class="input-border number" size="4"></span>
                                        <span style="margin-left:10px">Volt</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>LOCAL / REMOTE</td>
                                <td colspan="7">
                                    <select name="dh110_lr" id="">
                                        <option <?php if($dh110_lr==0){ echo "selected";}?> value="0">Local
                                        </option>
                                        <option <?php if($dh110_lr==1){ echo "selected";}?> value="1">Remote
                                        </option>
                                    </select>
                                </td>
                                <td colspan="7">
                                    <select name="dh48_lr" id="">
                                        <option <?php if($dh48_lr==0){ echo "selected";}?> value="0">Local
                                        </option>
                                        <option <?php if($dh48_lr==1){ echo "selected";}?> value="1">Remote
                                        </option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="16" height="20"></td>
                            </tr>
                            <tr>
                                <th style="background-color:#6ac4eb">C</th>
                                <th style="background-color:#6ac4eb" colspan="2">TRAFO 150KV 60MVA</th>
                                <th class="kuning" colspan="2">TRAFO 1</th>
                                <th class="kuning" colspan="2">OLTC 1</th>
                                <th class="kuning" colspan="2">TRAFO 2</th>
                                <th class="kuning" colspan="2">OLTC 2</th>
                                <th class="kuning" colspan="2">TRAFO PS</th>
                                <th class="kuning" colspan="5">KETERANGAN</th>
                            </tr>
                            <tr>
                                <th rowspan="5" valign="top">1</th>
                                <td>MINYAK TRAFO</td>
                                <td>LEVEL</td>
                                <td colspan="2">

                                    <select name="trafo1_level" id="">
                                        <option <?php if($trafo1_level==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo1_level==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">

                                    <select name="oltc1_level" id="">
                                        <option <?php if($oltc1_level==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($oltc1_level==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">

                                    <select name="trafo2_level" id="">
                                        <option <?php if($trafo2_level==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo2_level==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">

                                    <select name="oltc2_level" id="">
                                        <option <?php if($oltc2_level==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($oltc2_level==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">

                                    <select name="trafo_ps_level" id="">
                                        <option <?php if($trafo_ps_level==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo_ps_level==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                                <td rowspan="15" colspan="5">
                                    <textarea name="keterangan_2"><?= $keterangan_2?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>N = NORMAL</td>
                                <th style="background-color:#6ac4eb">SUHU</th>
                                <td colspan="2" style="background-color:#6ac4eb"></td>
                                <td colspan="2">
                                    <div>
                                        <span><input type="text" name="oltc1_suhu" value="<?=$oltc1_suhu?>"
                                                class="input-border number" size="4"></span>
                                        <span style="margin-left:10px">&#8451;</span>
                                    </div>
                                </td>
                                <td colspan="2" style="background-color:#6ac4eb"></td>
                                <td colspan="2">
                                    <div>
                                        <span><input type="text" name="oltc2_suhu" value="<?=$oltc2_suhu?>"
                                                class="input-border number" size="4"></span>
                                        <span style="margin-left:10px">&#8451;</span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span><input type="text" name="trafo_ps_suhu" value="<?=$trafo_ps_suhu?>"
                                                class="input-border number" size="4"></span>
                                        <span style="margin-left:10px">&#8451;</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="3" valign="top">TN=TIDAK NORMAL</td>
                                <td>OIL TEMP</td>
                                <td colspan="2">
                                    <div>
                                        <span><input type="text" name="trafo1_oil" value="<?=$trafo1_oil?>"
                                                class="input-border number" size="4"></span>
                                        <span style="margin-left:10px">&#8451;</span>
                                    </div>
                                </td>
                                <td rowspan="12" style="background-color:#6ac4eb" colspan="2"></td>
                                <td colspan="2">
                                    <div>
                                        <span><input type="text" name="trafo2_oil" value="<?=$trafo2_oil?>"
                                                class="input-border number" size="4"></span>
                                        <span style="margin-left:10px">&#8451;</span>
                                    </div>
                                </td>
                                <td rowspan="12" style="background-color:#6ac4eb" colspan="4"></td>
                            </tr>
                            <tr>
                                <td>WINDING HV</td>
                                <td colspan="2">
                                    <div>
                                        <span><input type="text" name="trafo1_hv" value="<?=$trafo1_hv?>"
                                                class="input-border number" size="4"></span>
                                        <span style="margin-left:10px">&#8451;</span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span><input type="text" name="trafo2_hv" value="<?=$trafo2_hv?>"
                                                class="input-border number" size="4"></span>
                                        <span style="margin-left:10px">&#8451;</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>WINDING LV</td>
                                <td colspan="2">
                                    <div>
                                        <span><input type="text" name="trafo1_lv" value="<?=$trafo1_lv?>"
                                                class="input-border number" size="4"></span>
                                        <span style="margin-left:10px">&#8451;</span>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <span><input type="text" name="trafo2_lv" value="<?=$trafo2_lv?>"
                                                class="input-border number" size="4"></span>
                                        <span style="margin-left:10px">&#8451;</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>2</th>
                                <td colspan="2">POSISI TAP</td>
                                <td colspan="2"><input class="input-full" type="text" value="<?= $trafo1_posisi_tap?>"
                                        name="trafo1_posisi_tap"></td>
                                <td colspan="2"><input class="input-full" type="text" value="<?= $trafo2_posisi_tap?>"
                                        name="trafo2_posisi_tap"></td>
                            </tr>
                            <tr>
                                <th>3</th>
                                <td colspan="2">COUNTER OLTC</td>
                                <td colspan="2"><input class="input-full" type="text" value="<?= $trafo1_counter_oltc?>"
                                        name="trafo1_counter_oltc"></td>
                                <td colspan="2"><input class="input-full" type="text" value="<?= $trafo2_counter_oltc?>"
                                        name="trafo2_counter_oltc"></td>
                            </tr>
                            <tr>
                                <th rowspan="7" valign="top">4</th>
                                <td>PROTEKSI</td>
                                <td>BUCHOLZ</td>
                                <td colspan="2">
                                    <select name="trafo1_bucholz" id="">
                                        <option <?php if($trafo1_bucholz==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo1_bucholz==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">

                                    <select name="trafo2_bucholz" id="">
                                        <option <?php if($trafo2_bucholz==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo2_bucholz==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>N = NORMAL</td>
                                <td>JANSEN</td>
                                <td colspan="2">
                                    <select name="trafo1_jansen" id="">
                                        <option <?php if($trafo1_jansen==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo1_jansen==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">

                                    <select name="trafo2_jansen" id="">
                                        <option <?php if($trafo2_jansen==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo2_jansen==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="5" valign="top">TN = TIDAK NORMAL</td>
                                <td>TERMAL</td>
                                <td colspan="2">
                                    <select name="trafo1_termal" id="">
                                        <option <?php if($trafo1_termal==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo1_termal==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">

                                    <select name="trafo2_termal" id="">
                                        <option <?php if($trafo2_termal==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo2_termal==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>SUDDEN PRESS</td>
                                <td colspan="2">
                                    <select name="trafo1_sudden_press" id="">
                                        <option <?php if($trafo1_sudden_press==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo1_sudden_press==1){ echo "selected";}?> value="1">
                                            Abnormal
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">

                                    <select name="trafo2_sudden_press" id="">
                                        <option <?php if($trafo2_sudden_press==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo2_sudden_press==1){ echo "selected";}?> value="1">
                                            Abnormal
                                        </option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>FIRE PREVENTION (BAR)</td>
                                <td colspan="2">
                                    <select name="trafo1_fire" id="">
                                        <option <?php if($trafo1_fire==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo1_fire==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">

                                    <select name="trafo2_fire" id="">
                                        <option <?php if($trafo2_fire==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo2_fire==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>NGR (ARUS)</td>
                                <td colspan="2">
                                    <select name="trafo1_ngr" id="">
                                        <option <?php if($trafo1_ngr==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo1_ngr==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">

                                    <select name="trafo2_ngr" id="">
                                        <option <?php if($trafo2_ngr==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo2_ngr==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>DC HEALTH</td>
                                <td colspan="2">
                                    <select name="trafo1_dc" id="">
                                        <option <?php if($trafo1_dc==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo1_dc==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                                <td colspan="2">

                                    <select name="trafo2_dc" id="">
                                        <option <?php if($trafo2_dc==0){ echo "selected";}?> value="0">Normal
                                        </option>
                                        <option <?php if($trafo2_dc==1){ echo "selected";}?> value="1">Abnormal
                                        </option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div><input type="hidden" name="id" value="<?php echo $id; ?>" />
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        <?= anchor('check_list_gis','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>