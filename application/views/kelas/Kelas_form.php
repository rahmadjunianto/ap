<div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-body">
                <form action="<?php echo $action; ?>" method="post"  role="form">
                <div class="box-body">


                    <div class="form-group">
                    <label for="nama_kelas">Nama Kelas  <sup style="color:red;">*</sup></label>
                    <input type="text" value="<?php echo $nama_kelas; ?>" class="form-control" name="nama_kelas" id="nama_kelas" placeholder="Nama Kelas">
                    <?php echo form_error('nama_kelas') ?>
                </div>

                    <div class="form-group">
                    <label for="keterangan">Keterangan  <sup style="color:red;">*</sup></label>
                    <input type="text" value="<?php echo $keterangan; ?>" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan">
                    <?php echo form_error('keterangan') ?>
                </div>
	    </div><input type="hidden" name="kd_kelas" value="<?php echo $kd_kelas; ?>" />
	              <div class="box-footer">
              <button type="submit" class="btn btn-primary btn-sm">Submit</button>
              <?= anchor('kelas','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
            </div>

                </form>
            </div>
        </div>
    </div>
    </div>