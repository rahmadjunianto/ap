<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <form action="<?php echo $action; ?>" method="post" role="form">
                    <div class="box-body">


                        <div class="form-group">
                            <label for="nama_kelas">Nama Kelas <sup style="color:red;">*</sup></label>
                            <input type="text" value="<?php echo $nama_kelas; ?>" class="form-control" name="nama_kelas"
                                id="nama_kelas" placeholder="Nama Kelas">
                            <?php echo form_error('nama_kelas') ?>
                        </div>

                        <div class="form-group">
                            <label for="keterangan">Keterangan <sup style="color:red;">*</sup></label>
                            <input type="text" value="<?php echo $keterangan; ?>" class="form-control" name="keterangan"
                                id="keterangan" placeholder="Keterangan">
                            <?php echo form_error('keterangan') ?>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th rowspan="3">NO</th>
                                    <th rowspan="3">Panel</th>
                                    <th>Tegangan (KV)</th>
                                    <th>Arus (A)</th>
                                    <th>Daya</th>
                                    <th rowspan="3">Frekuensi</th>
                                    <th rowspan="3">Cos</th>
                                </tr>
                                <tr>
                                    <th rowspan="2">L1</th>
                                    <th rowspan="2">L2</th>
                                    <th rowspan="2">L3</th>
                                    <th colspan="2">Terima</th>
                                    <th colspan="2">Kirim</th>
                                </tr>
                                <tr>
                                    <th>Energi <br>(KWH)</th>
                                    <th>Reaktif <br>(KvaRH)</th>
                                    <th>Energi <br>(KWH)</th>
                                    <th>Reaktif <br>(KvaRH)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Penghantar 1 <br>
                                        GI cengkareng 1
                                    </td>
                                    <td>
                                        <input type="text" value="" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan">

                                    </td>
                                    <td>
                                        <input type="text" value="" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan">

                                    </td>
                                    <td>
                                        <input type="text" value="" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan">

                                    </td>
                                    <td>
                                        <input type="text" value="" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan">

                                    </td>
                                    <td>
                                        <input type="text" value="" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan">

                                    </td>
                                    <td>
                                        <input type="text" value="" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan">

                                    </td>
                                    <td>
                                        <input type="text" value="" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan">

                                    </td>
                                    <td>
                                        <input type="text" value="" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan">

                                    </td>
                                    <td>
                                        <input type="text" value="" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan">

                                    </td>
                                    <td>
                                        <input type="text" value="" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan">

                                    </td>
                                    <td>
                                        <input type="text" value="" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan">

                                    </td>
                                    <td>
                                        <input type="text" value="" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan">

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <input type="hidden" name="kd_kelas" value="<?php echo $kd_kelas; ?>" />
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        <?= anchor('kelas','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>