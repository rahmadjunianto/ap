<div class="row">
    <div class="col-md-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="<?php echo site_url('jadwal_dinas'); ?>" method="get">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal</label>
                                <input autocomplete="off" type="text" class="form-control datepicker"
                                    placeholder="Tanggal" name="tanggal" value="<?= $tanggal?>">
                            </div>
                        </div>
                        <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="pegawai_id">Pegawai</label>
                                    <select  name="pegawai" id="pegawai" class="form-control">
                                        <option value="">Pilih</option>
                                        <?php foreach ($pegawai_list as $rk) { ?>
                                        <option <?= $rk->kd_pegawai==$pegawai ? "selected":""?> value="<?= $rk->kd_pegawai?>">
                                            <?= $rk->nm_pegawai?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        <div class="col-md-3" style="padding-top:25px">
                            <div class="form-group">
                                <label></label>
                                <?php  if ($q <> ''||$tanggal <> ''||$pegawai <> '') {   ?>
                                <a href="<?php echo site_url('jadwal_dinas'); ?>" class="btn btn-warning"><i
                                        class="fa fa-refresh"></i> Reset</a>
                                <?php   } ?>

                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                            </div>
                        </div>
                    </form>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Pegawai</th>
                            <th>Tanggal</th>
                            <th>Keterangan</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody><?php
        foreach ($jadwal_dinas_data as $jadwal_dinas)
        {
            ?>
                        <tr>
                            <td align="center" width="80px"><?php echo ++$start ?></td>
                            <td><?php echo nama_pegawai($jadwal_dinas->pegawai_id) ?></td>
                            <td><?php echo date_indo($jadwal_dinas->tanggal) ?></td>
                            <td><?php echo $keterangan[$jadwal_dinas->keterangan] ?></td>
                            <td style="text-align:center" width="100px">
                                <?php if($akses['is_update']==1){
				echo anchor(site_url('jadwal_dinas/update/'.acak($jadwal_dinas->id)),'<i class="fa fa-edit"></i>','class="btn btn-sm btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"'); }
				 if($akses['is_delete']==1){echo anchor(site_url('jadwal_dinas/delete/'.acak($jadwal_dinas->id)),'<i class="fa fa-trash"></i>','class="btn btn-sm btn-danger btn-xs" onclick="javasciprt: return confirm(\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus data"'); }
				?>
                            </td>
                        </tr>
                        <?php
              }
              ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <span class="pull-left">
                    <button type="button" class="btn btn-block btn-success btn-sm">Record :
                        <?php echo $total_rows ?></button>
                </span>
                <?php echo $pagination ?>
            </div>
        </div>
    </div>
</div>