<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <form action="<?php echo $action; ?>" method="post" role="form">
                    <div class="box-body">

                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="pegawai_id">Pegawai <sup style="color:red;">*</sup></label>
                                    <select required name="pegawai_id" id="pegawai_id" class="form-control">
                                        <option value="">Pilih</option>
                                        <?php foreach ($pegawai_list as $rk) { ?>
                                        <option <?= $rk->kd_pegawai==$pegawai_id ? "selected":""?> value="<?= $rk->kd_pegawai?>">
                                            <?= $rk->nm_pegawai?></option>
                                        <?php } ?>
                                    </select>
                                    <?php echo form_error('pegawai_id') ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="tanggal">Tanggal <sup style="color:red;">*</sup></label>
                                    <input autocomplete="off" type="text" value="<?php echo $tanggal; ?>"
                                        class="form-control datepicker" name="tanggal" id="tanggal"
                                        placeholder="Tanggal">
                                    <?php echo form_error('tanggal') ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="keterangan">Keterangan <sup style="color:red;">*</sup></label>
                                    <select required name="keterangan" class="form-control">
                                        <option value="">Pilih</option>
                                        <option <?php if($keterangan=='D'){ echo "selected";}?> value="D">Dinas Pagi Staf (D)
                                        </option>
                                        <option <?php if($keterangan=='PS'){ echo "selected";}?> value="PS">Dinas Pagi-Siang Ops (PS)
                                        </option>
                                        <option <?php if($keterangan=='M'){ echo "selected";}?> value="M">Dinas Malam Ops (M)
                                        </option>
                                    </select>
                                    <?php echo form_error('keterangan') ?>
                                </div>
                            </div>
                        </div>
                    </div><input type="hidden" name="id" value="<?php echo $id; ?>" />
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        <?= anchor('jadwal_dinas','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>