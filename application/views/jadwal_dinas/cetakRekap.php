<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jadwal Dinas</title>
</head>
<?php
use Carbon\Carbon;
?>
<style>
table.table-bordered{
    border:1px solid black;
    margin-top:20px;
  }
table.table-bordered > thead > tr > th{
    border:1px solid black;
}
table.table-bordered > tbody > tr > td{
    border:1px solid black;
}
</style>
<style>
    body {
        font-size: 10px
    }

    table td {
        padding: 5px
    }
</style>
<body>
<div class="table-responsive">
<table width="100%">
        <tr>
            <td align="center" width="90%">HIGH & MEDIUM VOLTAGE STATION <br> Bulan <?= $bulanTahun?></td>
            <td rowspan="3" align="right" width="10%"><img src="<?= base_url()?>/assets/ap.png" alt=""></td>
        </tr>
    </table>
    <br>
    <table border="1" cellspacing="0" cellpadding="0" width="100%">
                    <thead>
                        <tr>
                            <th rowspan="2" style="background-color: yellow">No</th>
                            <th rowspan="2" style="background-color: yellow">NAMA</th>
                            <th rowspan="2" style="background-color: yellow">JAB</th>
                            <th rowspan="2" style="background-color: yellow">NIP</th>
                            <th colspan="<?=$jmlHari?>" class="text-center">Tanggal</th>
                            <th rowspan="2">Jumlah</th>
                            <th rowspan="2">Keterangan</th>
                        </tr>
                        <tr>
                        <?php foreach ($period as $rk => $value){
                            if ($value->isWeekend()==true) {
                                echo '<th bgcolor="grey">'.Carbon::parse($value)->format('d').'</th>';
                            } else {
                                echo '<th>'.Carbon::parse($value)->format('d').'</th>';
                            }

                        }?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $ps=[];$m=0;  $no=1; foreach ($q as $rk) {?>

                            <tr>
                                <td><?=$no++;?></td>
                                <td nowrap><?=$rk->nm_pegawai?></td>
                                <td><?=$rk->jabatan?></td>
                                <td><?=$rk->nip?></td>
                                <?php $masuk=0; foreach ($period as $rr => $value) {
                                    $v="tgl".Carbon::parse($value)->format('d');
                                    if ($value->isWeekend()==true) {
                                        echo '<td bgcolor="grey">'.$rk->$v.'</td>';
                                    } else {
                                        echo '<td>'.$rk->$v.'</td>';
                                    }
                                    if($rk->$v<>''){
                                        $masuk+=1;
                                    }
                                } ?>
                                <td class="text-center"><?=$masuk?></td>
                                <td></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="4" class="text-right">Jumlah PS</td>
                            <?php foreach ($period as $rr => $value) {
                                    $v="tgl".Carbon::parse($value)->format('d');
                                    foreach ($q2 as $rk) {
                                        echo  $rk->$v<>''?'<td>'.$rk->$v.'</td>':'<td>0</td>';
                                    }
                            } ?>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="4" class="text-right">Jumlah M</td>
                            <?php foreach ($period as $rr => $value) {
                                    $v="tgl".Carbon::parse($value)->format('d');
                                    foreach ($q3 as $rk) {
                                        echo  $rk->$v<>''?'<td>'.$rk->$v.'</td>':'<td>0</td>';
                                    }
                            } ?>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <th colspan="3" align="left">Keterangan</th>
                    <td></td>
                    <td></td>
                    <td align="center">Tangerang, <?=date_indo(date('Y-m-d'))?></td>
                </tr>
                <tr>
                    <td  width="4%">D</td>
                    <td  width="20%">Dinas Pagi Staf</td>
                    <td width="10%">07.30 - 16.30</td>
                    <td width="25%" align="center">MANAGER OF ENERGY & POWER SUPPLY</td>
                    <td ></td>
                    <td align="center">Plt. ASSISTANT MANAGER OF HV & MV STATION</td>
                </tr>
                <tr>
                    <td>PS</td>
                    <td>Dinas Pagi Siang Ops</td>
                    <td>07.30 - 16.30</td>
                </tr>
                <tr>
                    <td>M</td>
                    <td>Dinas Malam Ops</td>
                    <td>07.30 - 16.30</td>
                </tr>
                <tr>
                    <td style="background-color: yellow"></td>
                    <td colspan="2">Cuti</td>
                </tr>
                <tr>
                    <td style="background-color: green"></span></td>
                    <td colspan="2">Revisi</td>
                    <td align="center">Sumantri Widodo</td>
                    <td></td>
                    <td align="center">M. FAJAR SIDIQ</td>
                </tr>
                <tr>
                    <td style="background-color: blue"></td>
                    <td colspan="2">Dinas Luar</td>
                </tr>
            </table>

</body>


</html>