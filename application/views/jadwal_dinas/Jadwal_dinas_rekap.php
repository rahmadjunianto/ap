<?php
use Carbon\Carbon;
?>
<style>
table.table-bordered{
    border:1px solid black;
    margin-top:20px;
  }
table.table-bordered > thead > tr > th{
    border:1px solid black;
}
table.table-bordered > tbody > tr > td{
    border:1px solid black;
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="<?php echo site_url('jadwal_dinas/rekap'); ?>" method="get">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Bulan Tahun</label>
                                <input autocomplete="off" type="text" class="form-control bulanTahun"
                                    placeholder="bulanTahun" name="bulanTahun" value="<?= $bulanTahun?>">
                            </div>
                        </div>
                        <div class="col-md-3" style="padding-top:25px">
                            <div class="form-group">
                                <label></label>
                                <?php  if ($bulanTahun <> '') {   ?>
                                <a href="<?php echo site_url('jadwal_dinas/rekap'); ?>" class="btn btn-warning"><i
                                        class="fa fa-refresh"></i> Reset</a>
                                <a target="_blank" href="<?php echo $cetak ?>" class="btn btn-danger"><i
                                        class="fa fa-file-pdf-o "></i> Cetak</a>
                                <?php   } ?>

                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                            </div>
                        </div>
                    </form>
                </div>

            <?php if($bulanTahun<>'') {  ?>


            <div class="table-responsive">
                <table class="table table-bordered table-striped"  >
                    <thead>
                        <tr>
                            <th rowspan="2" style="background-color: yellow">No</th>
                            <th rowspan="2" style="background-color: yellow">NAMA</th>
                            <th rowspan="2" style="background-color: yellow">JAB</th>
                            <th rowspan="2" style="background-color: yellow">NIP</th>
                            <th colspan="<?=$jmlHari?>" class="text-center">Tanggal</th>
                            <th rowspan="2">Jumlah</th>
                            <th rowspan="2">Keterangan</th>
                        </tr>
                        <tr>
                        <?php foreach ($period as $rk => $value){
                            if ($value->isWeekend()==true) {
                                echo '<th bgcolor="grey">'.Carbon::parse($value)->format('d').'</th>';
                            } else {
                                echo '<th>'.Carbon::parse($value)->format('d').'</th>';
                            }

                        }?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $ps=[];$m=0;  $no=1; foreach ($q as $rk) {?>

                            <tr>
                                <td><?=$no++;?></td>
                                <td nowrap><?=$rk->nm_pegawai?></td>
                                <td><?=$rk->jabatan?></td>
                                <td><?=$rk->nip?></td>
                                <?php $masuk=0; foreach ($period as $rr => $value) {
                                    $v="tgl".Carbon::parse($value)->format('d');
                                    if ($value->isWeekend()==true) {
                                        echo '<td bgcolor="grey">'.$rk->$v.'</td>';
                                    } else {
                                        echo '<td>'.$rk->$v.'</td>';
                                    }
                                    if($rk->$v<>''){
                                        $masuk+=1;
                                    }
                                } ?>
                                <td class="text-center"><?=$masuk?></td>
                                <td></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="4" class="text-right">Jumlah PS</td>
                            <?php foreach ($period as $rr => $value) {
                                    $v="tgl".Carbon::parse($value)->format('d');
                                    foreach ($q2 as $rk) {
                                        echo  $rk->$v<>''?'<td>'.$rk->$v.'</td>':'<td>0</td>';
                                    }
                            } ?>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="4" class="text-right">Jumlah M</td>
                            <?php foreach ($period as $rr => $value) {
                                    $v="tgl".Carbon::parse($value)->format('d');
                                    foreach ($q3 as $rk) {
                                        echo  $rk->$v<>''?'<td>'.$rk->$v.'</td>':'<td>0</td>';
                                    }
                            } ?>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <table class="table">
                <tr>
                    <th colspan="3">Keterangan</th>
                </tr>
                <tr>
                    <td  width="2%">D</td>
                    <td  width="20%">Dinas Pagi Staf</td>
                    <td>07.30 - 16.30</td>
                </tr>
                <tr>
                    <td>PS</td>
                    <td>Dinas Pagi Siang Ops</td>
                    <td>07.30 - 16.30</td>
                </tr>
                <tr>
                    <td>M</td>
                    <td>Dinas Malam Ops</td>
                    <td>07.30 - 16.30</td>
                </tr>
                <tr>
                    <td ><span style="display: block;
                    width: 15px;
                    height: 15px; background: yellow"></span></td>
                    <td colspan="2">Cuti</td>
                </tr>
                <tr>
                    <td ><span style="display: block;
                    width: 15px;
                    height: 15px; background: green"></span></td>
                    <td colspan="2">Revisi</td>
                </tr>
                <tr>
                    <td ><span style="display: block;
                    width: 15px;
                    height: 15px; background: blue  "></span></td>
                    <td colspan="2">Dinas Luar</td>
                </tr>
            </table>
            <?php } ?>

            </div>

        </div>
    </div>
</div>
<script src="<?= base_url() ?>adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<script>
$('.breadcrumb').hide()
</script>
