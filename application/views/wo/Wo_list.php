<div class="row">
<div class="col-md-12">
  <div class="box">
    <!-- /.box-header -->
    <div class="box-body">
    <div class="row">
          <form action="<?php echo site_url('wo'); ?>"  method="get">
              <div class="col-md-3">
                  <div class="form-group">
                      <label>Tanggal</label>
                      <input autocomplete="off" type="text" class="form-control datepicker"  placeholder="Tanggal" name="tanggal" value="<?= $tanggal?>">
                  </div>
              </div>
              <div class="col-md-3">
                  <div class="form-group">
                      <label>Nomor</label>
                      <input type="text" class="form-control"  placeholder="Nomor" name="q" value="<?= $q?>">
                  </div>
              </div>
              <div class="col-md-3" style="padding-top:25px">
                  <div class="form-group">
                      <label ></label>
                      <?php  if ($tanggal <> '' || $q) {   ?>
                        <a href="<?php echo site_url('wo'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                      <?php   } ?>

                      <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                  </div>
              </div>
          </form>
      </div>
      <table class="table table-bordered">
      <thead>
          <tr>
              <th>No</th>
		<th>Tanggal</th>
		<th>Nomor</th>
		<th></th>
          </tr>
        </thead>
        <tbody><?php
        foreach ($wo_data as $wo)
        {
            ?>
            <tr>
			<td align="center" width="80px"><?php echo ++$start ?></td>
			<td><?php echo date_indo($wo->tanggal) ?></td>
			<td><?php echo $wo->nomor ?></td>
			<td style="text-align:center" width="100px">
                <?php
                echo anchor(site_url('wo/cetak/'.acak($wo->id)),'<i class="fa fa-print"></i>','target="_blank" class="btn btn-sm btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cetak data"');
                if($akses['is_update']==1){
				echo anchor(site_url('wo/update/'.acak($wo->id)),'<i class="fa fa-edit"></i>','class="btn btn-sm btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"'); }
				 if($akses['is_delete']==1){echo anchor(site_url('wo/delete/'.acak($wo->id)),'<i class="fa fa-trash"></i>','class="btn btn-sm btn-danger btn-xs" onclick="javasciprt: return confirm(\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus data"'); }
				?>
			</td>
		</tr>
                  <?php
              }
              ?>
              </tbody>
      </table>
    </div>
    <div class="box-footer clearfix">
      <span class="pull-left">
      <button type="button" class="btn btn-block btn-success btn-sm">Record : <?php echo $total_rows ?></button>
      </span>
            <?php echo $pagination ?>
      </div>
  </div>
</div>
</div>