<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <form action="<?php echo $action; ?>" method="post" role="form" onsubmit="return validateForm()">
                    <div class="box-body">

                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="tanggal">Tanggal <sup style="color:red;">*</sup></label>
                                    <input autocomplete="off" type="text" value="<?php echo $tanggal; ?>"
                                        class="form-control datepicker" name="tanggal" id="tanggal"
                                        placeholder="Tanggal">
                                    <?php echo form_error('tanggal') ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="klasifikasi">Klasifikasi <sup style="color:red;">*</sup></label>
                                    <select required name="klasifikasi" class="form-control">
                                        <option value="">Pilih</option>
                                        <option <?php if($klasifikasi==1){ echo "selected";}?> value="1">Preventive
                                        </option>
                                        <option <?php if($klasifikasi==2){ echo "selected";}?> value="2">Predictive
                                        </option>
                                        <option <?php if($klasifikasi==3){ echo "selected";}?> value="2">Corrective
                                        </option>
                                    </select>
                                    <?php echo form_error('klasifikasi') ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="lokasi_pekerjaan">Lokasi Pekerjaan <sup
                                            style="color:red;">*</sup></label>
                                    <input type="text" value="<?php echo $lokasi_pekerjaan; ?>" class="form-control"
                                        name="lokasi_pekerjaan" id="lokasi_pekerjaan" placeholder="Lokasi Pekerjaan">
                                    <?php echo form_error('lokasi_pekerjaan') ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="jenis_pekerjaan">Jenis Pekerjaan <sup style="color:red;">*</sup></label>
                                    <input type="text" value="<?php echo $jenis_pekerjaan; ?>" class="form-control"
                                        name="jenis_pekerjaan" id="jenis_pekerjaan" placeholder="Jenis Pekerjaan">
                                    <?php echo form_error('jenis_pekerjaan') ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="uraian_pekerjaan">Uraian Pekerjaan <sup
                                            style="color:red;">*</sup></label>
                                    <textarea class="form-control" rows="3" name="uraian_pekerjaan"
                                        id="uraian_pekerjaan"
                                        placeholder="Uraian Pekerjaan"><?php echo $uraian_pekerjaan; ?></textarea>
                                    <?php echo form_error('uraian_pekerjaan') ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="catatan">Catatan <sup style="color:red;">*</sup></label>
                                    <textarea class="form-control" rows="3" name="catatan" id="catatan"
                                        placeholder="Catatan"><?php echo $catatan; ?></textarea>
                                    <?php echo form_error('catatan') ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="mulai">Mulai <sup style="color:red;">*</sup></label>
                                    <input type="text" value="<?php echo $mulai; ?>" class="form-control jam"
                                        name="mulai" id="mulai" placeholder="Mulai">
                                    <?php echo form_error('mulai') ?>

                                    <label for="selesai">Selesai <sup style="color:red;">*</sup></label>
                                    <input type="text" value="<?php echo $selesai; ?>" class="form-control jam"
                                        name="selesai" id="selesai" placeholder="Selesai">
                                    <?php echo form_error('selesai') ?>
                                </div>
                            </div>
                            <div class="col-sm-4">


                                <table class="table table-bordered">
                                    <thead id="kontentdiaga">
                                        <tr>
                                            <td>Material</td>
                                            <td>Jumlah</td>
                                            <td><button title="Tambah" id='addButton' type="button"
                                                    class="btn btn-xs btn-success"><i class="fa fa-plus"></i></td>
                                        </tr>
                                    </thead>
                                    <tbody id="kontentdiag">

                                        <?php if(isset($material_list)) {foreach ($material_list as $rk) { ?>

                                        <tr class="barisdiag">
                                            <td><input type="text" name="material[]" required="" class="form-control"
                                                    id="" value="<?= $rk->material?>"></td>
                                            <td><input type="text" onkeypress="return isNumberKey(event)"
                                                    name="jumlah[]" value="<?= $rk->jumlah?>" required="" class="form-control" id=""></td>
                                            <td><button title="Hapus" type="button" class="btn btn-xs btn-danger remove_project_file"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        <?php }} ?>
                                    </tbody>
                                </table>

                            </div>
                            <div class="col-sm-4">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Teknisi</th>
                                            <th width="10%"><button title="Tambah Teknisi" id='addButtonPetugas'
                                                    type="button" class="btn btn-xs btn-success"><i
                                                        class="fa fa-plus"></i></button></th>
                                        </tr>
                                    </thead>
                                    <tbody id="petugasList">
                                        <?php if(isset($teknisi_list)) {foreach ($teknisi_list as $rk) { ?>
                                        <tr class="barisdiag">
                                            <td><?= nama_teknisi($rk->teknisi_id)?></td><input type="hidden"
                                                name="teknisi_id[]" value="<?= $rk->teknisi_id?>">
                                            <td><button title="Hapus Teknisi" type="button"
                                                    class="btn btn-xs btn-danger hapusPetugas"><i
                                                        class="fa fa-trash"></i></button></td>
                                        </tr>
                                        <?php }} ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                    </div><input type="hidden" name="id" value="<?php echo $id; ?>" />
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        <?= anchor('wo','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">List Teknisi</h4>
            </div>
            <div class="modal-body">
                <table id="example" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="width:70px ">Nama</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                    foreach ($teknisi as $rk)
                    {
                        ?>
                        <tr style="cursor: pointer;" class="pilih" data-id="<?php echo $rk->id ?>"
                            data-nama="<?php echo $rk->nama ?>">
                            <td><?php echo $rk->nama ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>