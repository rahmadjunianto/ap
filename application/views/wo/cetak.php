<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wo</title>
</head>
<style>
    body {
        font-size: 10px
    }

    table td {
        padding: 5px
    }
     table td.no-border-right {
        border-top: solid 1px #FFF;
        color: red;
 }
</style>

<body>
    <div style="border: 1px solid black; padding: 5px; min-width:100px; display: inline-block">
        <div style="border: 1px solid black; padding: 5px; min-width:100px; display: inline-block">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>BIDANG FASILITAS BANDARA</td>
                    <td rowspan="2" align="right"><img src="<?= base_url()?>/assets/ap.png" alt=""></td>
                </tr>
                <tr>
                    <td>DIVISI ENERGY & POWER SUPPLY</td>
                </tr>
            </table>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <th colspan="5" style="font-size:14px">PERINTAH KERJA</th>
                </tr>
            </table>
            <table border="1" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <th  style="font-size:14px">TANGGAL</th>
                    <th colspan="2" style="font-size:14px">NOMOR</th>
                    <th  style="font-size:14px">KLASIFIKASI</th>
                    <th  style="font-size:14px; width:150px" >LOKASI PEKERJAAN</th>
                </tr>

                <tr>
                    <td align="center"><?= date_indo($wo->tanggal)?></td>
                    <td colspan="2" align="center"><?= $wo->nomor?></td>
                    <td align="center"><?= $klasifikasi[$wo->klasifikasi]?></td>
                    <td align="left"><?= $wo->lokasi_pekerjaan?></td>
                </tr>
                <tr>
                    <th  style="font-size:14px" colspan="4">WORK ORDER (JENIS PEKERJAAN)</th>
                    <th  style="font-size:14px">NAMA TEKNISI</th>
                </tr>
                <tr>
                    <td colspan="4"><?= $wo->jenis_pekerjaan?></td>
                    <td><?php $no=1; foreach ($teknisi as $rk) { ?>
                        <?= $no++.". ".nama_teknisi($rk->teknisi_id)."<br>";?>
                    <?php } ?></td>
                </tr>
                <tr>
                    <th colspan="5" style="font-size:14px">URAIAN PEKERJAAN</th>
                </tr>
                <tr>
                    <td colspan="5" align="left"><?= $wo->uraian_pekerjaan?></td>
                </tr>
                <tr>
                    <th colspan="5" style="font-size:14px">PELAKSANAAN</th>
                </tr>
                <tr>
                    <th  style="font-size:14px">MATERIAL YANG DIPAKAI</th>
                    <th  style="font-size:14px">JUMLAH</th>
                    <th colspan="2" style="font-size:14px">TANGGAL DAN JAM</th>
                    <th  style="font-size:14px">PARAF</th>
                </tr>
                <tr>
                    <td valign="top" rowspan="5"><?php $no=1; foreach ($material as $rk) { ?>
                        <?= $rk->material."<br>";?>
                    <?php } ?></td>
                    <td valign="top" rowspan="5"><?php $no=1; foreach ($material as $rk) { ?>
                        <?= $rk->jumlah."<br>";?>
                    <?php } ?></td>
                    <td rowspan="5" style="font-size:14px">Pukul</td>
                    <td>Mulai</td>
                    <td>Teknisi</td>
                </tr>
                <tr>
                    <td style="border-bottom: solid 1px #FFF;"><?= $wo->mulai?></td>
                    <td rowspan="2"></td>
                </tr>
                <tr>
                    <td class="no-border-right"></td>
                </tr>
                <tr>
                    <td >Selesai</td>
                    <td>PTO</td>
                </tr>
                <tr>
                    <td><?= $wo->selesai?></td>
                    <td height="50" style="white-space: pre-line"><br></td>
                </tr>
                <tr>
                    <th  style="font-size:14px" colspan="4">CATATAN</th>
                    <th  style="font-size:14px" style = "width:80px; word-wrap: break-word">ASST OF HIGH & MEDIUM VOLTAGE STATION</th>
                </tr>
                <tr>
                    <td rowspan="5" style="font-size:14px" colspan="4" align="left" valign="top"><?=$wo->catatan?></td>
                    <td height="50" style="white-space: pre-line"><br></td>
                </tr>
                <tr>
                    <td  style="font-size:14px" style = "width:80px; word-wrap: break-word">ENERGY & POWER SUPPLY MANAGER</td>
                </tr>
                <tr>
                    <td height="50" style="white-space: pre-line"><br></td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>