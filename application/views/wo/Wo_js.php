<script>
    $('#kontentdiaga').on('click', '#addButton', function (e) {

        $('#kontentdiag').append(

            '<tr class="barisdiag">'+
            '    <td><input type="text" name="material[]" required="" class="form-control" id=""></td>'+
            '    <td><input type="text" onkeypress="return isNumberKey(event)" name="jumlah[]" required="" class="form-control" id=""></td>'+

            '    <td><button title="Hapus" type="button"' +
            '            class="btn btn-xs btn-danger remove_project_file"><i class="fa fa-trash"></i></button></td></tr>'        );
        // $('.jam').datetimepicker(datetimepickerOption);
    });
    $(document).on('click', '#addButtonPetugas', function (e) {
        $('#myModal').modal('show');
    });
    $(document).on('click', '.pilih', function (e) {
        id = $(this).attr('data-id');
        nama = $(this).attr('data-nama');
        $('#petugasList').append(
            '<tr class="barisdiag">' +
            '    <td>' + nama + '</td>' +
            '<input type="hidden" name="teknisi_id[]" value="' + id + '"  >' +
            '    <td><button title="Hapus Petugas" type="button"' +
            '            class="btn btn-xs btn-danger hapusPetugas"><i class="fa fa-trash"></i></button></td></tr>'
        );
        $('#myModal').modal('hide');
    });
    function validateForm() {
        // alert()
        if ($('.hapusPetugas').length == 0) {
            alert("Teknisi Harus Diisi");
            return false;
        }
    }
    $('#kontentdiag').on('click', '.remove_project_file', function (e) {
        e.preventDefault();
        $(this).parents(".barisdiag").remove();
    });
</script>