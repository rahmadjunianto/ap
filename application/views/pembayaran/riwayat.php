<table style="font-size:16px;font-weight: normal">
    <tr>
        <td>NISN &nbsp;&nbsp;</td>
        <td>:&nbsp;&nbsp;</td>
        <td> <?= $tagihan->nisn ?>&nbsp;&nbsp;</td>
    </tr>
    <tr>
        <td>Nama Siswa &nbsp;&nbsp;</td>
        <td>:&nbsp;&nbsp;</td>
        <td> <?= $tagihan->nm_siswa ?>&nbsp;&nbsp;</td>
    </tr>
    <tr>
        <td>Nama Tagihan &nbsp;&nbsp;</td>
        <td>:&nbsp;&nbsp;</td>
        <td> <?= $tagihan->nm_tagihan ?>&nbsp;&nbsp;</td>
    </tr>
    <tr>
        <td>Nilai Tagihan Awal &nbsp;&nbsp;</td>
        <td>:&nbsp;&nbsp;</td>
        <td> <?= angka($tagihan->nominal) ?>&nbsp;&nbsp;</td>
    </tr>
</table><br>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th style="width:20px ">No</th>
            <th style="width:90px ">Tanggal Bayar</th>
            <!-- <th style ="width:90px " >Kategori Tagihan</th>
            <th style ="width:90px " >Tagihan</th> -->
            <th style="width:60px ">Nominal</th>
            <th style="width:390px ">Keterangan</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1;$nominal=0;
                            foreach ($riwayat as $rk)
                            {
                                ?>
        <tr>
            <td><?= $no++;?></td>
            <td><?php echo date_indo(substr($rk->tanggal_bayar,0,10)) ?></td>
            <td><?php echo angka($rk->nominal) ?></td>
            <td><?php echo $rk->keterangan ?></td>
        </tr>
        <?php $nominal+=$rk->nominal;
                            }
                            ?>
        <tr>
            <input type="hidden" id="total_bayar" value="<?= $nominal?>">
            <td colspan="2">Total Bayar</td>
            <td colspan="2"><span><?= angka($nominal) ?></span></td>
        </tr>
        <tr>
            <td colspan="2">Kurang Bayar</td>
            <td colspan="2"><span id="kurang_bayar"><?= angka($tagihan->nominal-$nominal) ?></span></td>
        </tr>
    </tbody>
</table>