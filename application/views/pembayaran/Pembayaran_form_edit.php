<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <form action="<?php echo $action; ?>" method="post" role="form" id="frm">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Siswa</h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group ">
                                <label for="kd_tagihan">NISN<sup id="sup"
                                        style="color:red;display: none;">*</sup></label>
                                <div class="input-group carinip" style="cursor: zoom-in;">
                                    <span class="input-group-addon"><i class="fa fa-search "></i></span>
                                    <input readonly="" style="cursor: zoom-in;" type="text" id="nisn"
                                        class="carinip form-control" placeholder="NISN" value="<?php echo $nisn; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="nominal">Nama <sup style="color:red;">*</sup></label>
                                <input readonly type="text" value="<?php echo $nama; ?>" class="form-control"
                                    name="nama" id="nama" placeholder="Nama">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="nominal">Kelas <sup style="color:red;">*</sup></label>
                                <input readonly type="text" value="<?php echo $kelas; ?>" class="form-control"
                                    name="kelas" id="kelas" placeholder="Kelas">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="nominal">Tanggal <sup style="color:red;">*</sup></label>
                                <input type="text" value="<?php echo $tanggal_bayar; ?>" class="form-control datepicker"
                                    name="tanggal_bayar" id="tanggal_bayar" placeholder="Tanggal">
                            </div>
                        </div>
                    </div>
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Tagihan</h3>
                    </div>
                    <div id="tagihan_list">
                        <table id="tagihan" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <!-- <th style ="width:70px " >Kategori Tagihan</th> -->
                                    <th style="width:50px ">Tagihan</th>
                                    <th style="width:60px ">Nominal</th>
                                    <th style="width:200px ">Keterangan</th>
                                    <th style="width:50px ">Nominal</th>
                                    <th style="width:50px ">Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($pembayaran!=null){ ?>

                                <?php $nominal=0;
                            foreach ($pembayaran as $rk)
                            {


                                $cek=$this->db->query("select total_tagihan($rk->kd_piutang) as total_tagihan")->row();

                                ?>


                                <tr style="cursor: pointer;" class="pilih_tagihan"
                                    data-nominal="<?php echo $rk->nominal_piutang-$cek->total_tagihan+$rk->nominal?>"
                                    data-status="<?php echo $rk->status ?>"
                                    data-kd_piutang="<?php echo $rk->kd_piutang ?>">
                                    <!-- <td style="vertical-align:middle"><?php echo kategori_tagihan($rk->kd_tagihan) ?></td> -->
                                    <td style="vertical-align:middle"><?php echo $rk->nm_tagihan ?></td>
                                    <td style="vertical-align:middle">
                                        <span><?php echo angka($rk->nominal_piutang-$cek->total_tagihan+$rk->nominal) ?></span>
                                    </td>
                                    <td style="vertical-align:middle"><?php echo $rk->ket ?></td>
                                    <td>
                                        <input type="text" onkeyup="formatangka(this);" class="form-control nominal "
                                            name="nominal[]" style="text-align:right;" value="<?= angka($rk->nominal)?>"
                                            placeholder="Nominal">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="keterangan[]"
                                            placeholder="Keterangan" value="<?= $rk->keterangan ?>">
                                        <input type="hidden" name="kd_piutang[]" id="kd_piutang"
                                            value="<?php echo $rk->kd_piutang; ?>" />
                                        <input type="hidden" name="id[]" id="id" value="<?php echo $rk->id; ?>" />
                                    </td>
                                </tr>
                                <?php $nominal+=$rk->nominal;
                            }
                            ?>
                                <tr>
                                    <td colspan="3" align="right">Total</td>
                                    <td id="jml_bayar" style="text-align:right;padding-right:20px">
                                        <?= angka($nominal) ?></td>
                                    <td></td>
                                </tr>

                                <?php } else { ?>
                                <tr>
                                    <td colspan="6" align="center">Tidak Ada Tagihan</td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <div class="modal fade" id="myCicilan" tabindex="-1" role="dialog"
                            aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog" style="width:800px">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Riwayat Cicilan</h4>
                                    </div>
                                    <div class="modal-body" id="isi_riwayat">

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- <input type="hidden" name="kd_piutang" id="kd_piutang" value="<?php echo $kd_piutang; ?>" /> -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    $(document).on('keyup', '.nominal', function (e) {
        total = $(this).parent().parent().attr('data-nominal');
        jml = $(this).val().replace(/[^\d]/g, "");
        if (parseInt(jml) > parseInt(total)) {
            alert('Jumlah Bayar tidak boleh lebih dari Rp ' + ribuan(total))
            $(this).val('')
        }

        var x = 0;
        $('.nominal').each(function (index, element) {
            if ($(element).val() == '') {
                n = 0
            } else {
                n = $(element).val().replace(/[^\d]/g, "")
            }
            x = x + parseFloat(n);
        });
        $('#jml_bayar').html(ribuan(x));
    });
    // $(document).on('click', '.lihat_cicilan', function (e) {

    //     kd_piutang=$(this).attr('data-kd_piutang');

    //     $.ajax({
    //                 type: "POST",
    //                 url: "<?php echo base_url().'Pembayaran/getRiwayat'?>",
    //                 data: { kd_piutang: kd_piutang},
    //                 cache: false,
    //                 success: function(msga){
    //                         // alert(msga);
    //                         $("#isi_riwayat").html(msga);
    //                         $('#myCicilan').modal('show');
    //                     }
    //             });
    // });
</script>
<style>
    .aktif {
        background: gold !important;
    }
</style>