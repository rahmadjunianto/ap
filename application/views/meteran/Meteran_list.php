<div class="row">
    <div class="col-md-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="<?php echo site_url('meteran'); ?>" method="get">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal</label>
                                <input autocomplete="off" type="text" class="form-control datepicker"
                                    placeholder="Tanggal" name="tanggal" value="<?= $tanggal?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Assistant Manager</label>
                                <select name="assistant_manager" id="assistant_manager" class="form-control">
                                    <option value="">Pilih</option>
                                    <?php foreach ($asisten_list as $rk) { ?>
                                    <option <?= $rk->nama==$assistant_manager ? "selected":""?> value="<?= $rk->nama?>">
                                        <?= $rk->nama?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Petugas Dinas</label>
                                <select name="petugas_dinas" id="petugas_dinas" class="form-control">
                                    <option value="">Pilih</option>
                                    <?php foreach ($petugas_list as $rk) { ?>
                                    <option <?= $rk->nama==$petugas_dinas ? "selected":""?> value="<?= $rk->nama?>">
                                        <?= $rk->nama?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3" style="padding-top:25px">
                            <div class="form-group">
                                <label></label>
                                <?php  if ($q <> ''||$tanggal<>''|$petugas_dinas<>''|$assistant_manager<>'') {   ?>
                                <a href="<?php echo site_url('meteran'); ?>" class="btn btn-warning"><i
                                        class="fa fa-refresh"></i> Reset</a>
                                <?php   } ?>

                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                            </div>
                        </div>
                    </form>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Pukul</th>
                            <th>Assistant Manager</th>
                            <th> Petugas Dinas</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody><?php
        foreach ($meteran_data as $meteran)
        {
            ?>
                        <tr>
                            <td align="center" width="80px"><?php echo ++$start ?></td>
                            <td><?php echo date_indo($meteran->tanggal) ?></td>
                            <td><?php echo $meteran->pukul ?></td>
                            <td><?php echo $meteran->assistant_manager ?></td>
                            <td><?php echo $meteran->petugas_dinas ?></td>
                            <td style="text-align:center" width="100px">
                                <?php
                echo anchor(site_url('meteran/cetak/'.acak($meteran->id)),'<i class="fa fa-print"></i>','target="_blank" class="btn btn-sm btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cetak data"');
                if($akses['is_update']==1){
				echo anchor(site_url('meteran/update/'.acak($meteran->id)),'<i class="fa fa-edit"></i>','class="btn btn-sm btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"'); }
				 if($akses['is_delete']==1){echo anchor(site_url('meteran/delete/'.acak($meteran->id)),'<i class="fa fa-trash"></i>','class="btn btn-sm btn-danger btn-xs" onclick="javasciprt: return confirm(\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus data"'); }

				?>
                            </td>
                        </tr>
                        <?php
              }
              ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <span class="pull-left">
                    <button type="button" class="btn btn-block btn-success btn-sm">Record :
                        <?php echo $total_rows ?></button>
                </span>
                <?php echo $pagination ?>
            </div>
        </div>
    </div>
</div>