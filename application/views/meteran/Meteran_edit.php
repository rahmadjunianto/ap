<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <form action="<?php echo $action; ?>" method="post" role="form">
                    <div class="box-body">
                    <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="panel"> ASSISTANT MANAGER OF HIGH & MEDIUM VOLTAGE STATION <?= "a".$assistant_manager ?><sup
                                            style="color:red;">*</sup></label>
                                    <select required name="assistant_manager" id="assistant_manager"
                                        class="form-control">
                                        <option value="">Pilih</option>
                                        <?php foreach ($asisten_list as $rk) { ?>
                                            <option <?= $rk->nama==$assistant_manager ? "selected":""?> value="<?= $rk->nama?>"><?= $rk->nama?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="panel"> PETUGAS DINAS<sup style="color:red;">*</sup></label>
                                        <select required name="petugas_dinas" id="petugas_dinas"
                                        class="form-control">
                                        <option value="">Pilih</option>
                                        <?php foreach ($petugas_list as $rk) { ?>
                                            <option <?= $rk->nama==$petugas_dinas ? "selected":""?> value="<?= $rk->nama?>"><?= $rk->nama?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <table border ="1">
                            <thead>
                                <tr style="background-color: cadetblue">
                                    <th rowspan="3">NO</th>
                                    <th rowspan="3" class="text-center">Panel</th>
                                    <th colspan="3" class="text-center">Tegangan (KV)</th>
                                    <th colspan="3" class="text-center">Arus (A)</th>
                                    <th colspan="4" class="text-center">Daya</th>
                                    <th rowspan="3" class="text-center">Frekuensi</th>
                                    <th rowspan="3" class="text-center">Cos</th>
                                </tr>
                                <tr style="background-color: cadetblue">
                                    <th rowspan="2" class="text-center">L1</th>
                                    <th rowspan="2" class="text-center">L2</th>
                                    <th rowspan="2" class="text-center">L3</th>
                                    <th rowspan="2" class="text-center">L1</th>
                                    <th rowspan="2" class="text-center">L2</th>
                                    <th rowspan="2" class="text-center">L3</th>
                                    <th colspan="2" class="text-center">Terima</th>
                                    <th colspan="2" class="text-center">Kirim</th>
                                </tr>
                                <tr style="background-color: cadetblue">
                                    <th class="text-center">Energi <br>(KWH)</th>
                                    <th class="text-center">Reaktif <br>(KvaRH)</th>
                                    <th class="text-center">Energi <br>(KWH)</th>
                                    <th class="text-center">Reaktif <br>(KvaRH)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($panel_list1 as $rk ) { ?>

                                    <tr>
                                    <td style="font-size:10px   " class="text-center"><?= $no++?></td>
                                    <td style="font-size:10px   "><?= $rk->panel?>
                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->T_L1?>" class="form-control number" name="T_L1[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->T_L2?>" class="form-control number" name="T_L2[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->T_L3?>" class="form-control number" name="T_L3[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->A_L1?>" class="form-control number" name="A_L1[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->A_L2?>" class="form-control number" name="A_L2[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->A_L3?>" class="form-control number" name="A_L3[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->DAYA_TERIMA_ENERGI?>" class="form-control number" name="DAYA_TERIMA_ENERGI[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->DAYA_TERIMA_REAKTIF?>" class="form-control number" name="DAYA_TERIMA_REAKTIF[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->DAYA_KIRIM_ENERGI?>" class="form-control number" name="DAYA_KIRIM_ENERGI[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->DAYA_KIRIM_REAKTIF?>" class="form-control number" name="DAYA_KIRIM_REAKTIF[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->FREKUENSI?>" class="form-control number" name="FREKUENSI[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->COS?>" class="form-control number" name="COS[]">

                                    </td>
                                    <input type="hidden" name="DAYA_AKTIF[]" value="" />
                                    <input type="hidden" name="DAYA_SEMU_1[]" value="" />
                                    <input type="hidden" name="DAYA_SEMU_2[]" value="" />
                                    <input type="hidden" name="DAYA_REAKTIF[]" value="" />
                                    <input type="hidden" name="jenis_panel[]" value="<?php echo $rk->jenis_panel; ?>" />
                                    <input type="hidden" name="id[]" value="<?php echo $rk->id; ?>" />
                                </tr>
                                <?php } ?>
                                <tr style="background-color: cadetblue">
                                    <th rowspan="2">NO</th>
                                    <th rowspan="2" class="text-center">Panel</th>
                                    <th colspan="3" class="text-center">Tegangan (KV)</th>
                                    <th colspan="3" class="text-center">Arus (A)</th>
                                    <th rowspan="2" class="text-center">DAYA AKTIF (KW)</th>
                                    <th rowspan="2" colspan="2" class="text-center">DAYA SEMU (KVA)</th>
                                    <th rowspan="2" class="text-center">DAYA REAKTIF (KVaR)</th>
                                    <th rowspan="2" class="text-center">Frekuensi</th>
                                    <th rowspan="2" class="text-center">Cos</th>
                                </tr>
                                <tr style="background-color: cadetblue">
                                    <th class="text-center">L1</th>
                                    <th class="text-center">L2</th>
                                    <th class="text-center">L3</th>
                                    <th class="text-center">L1</th>
                                    <th class="text-center">L2</th>
                                    <th class="text-center">L3</th>
                                </tr>
                                <?php foreach ($panel_list2 as $rk ) { ?>

                                    <tr>
                                    <td style="font-size:10px   " class="text-center"><?= $no++?></td>
                                    <td style="font-size:10px   "><?= $rk->panel?>
                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->T_L1?>" class="form-control number" name="T_L1[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->T_L2?>" class="form-control number" name="T_L2[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->T_L3?>" class="form-control number" name="T_L3[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->A_L1?>" class="form-control number" name="A_L1[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->A_L2?>" class="form-control number" name="A_L2[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->A_L3?>" class="form-control number" name="A_L3[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->DAYA_AKTIF?>" class="form-control number" name="DAYA_AKTIF[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->DAYA_SEMU_1?>" class="form-control number" name="DAYA_SEMU_1[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->DAYA_SEMU_2?>" class="form-control number" name="DAYA_SEMU_2[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->DAYA_REAKTIF?>" class="form-control number" name="DAYA_REAKTIF[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->FREKUENSI?>" class="form-control number" name="FREKUENSI[]">

                                    </td>
                                    <td>
                                        <input  style="width: 80px;" type="text" value="<?= $rk->COS?>" class="form-control number" name="COS[]">

                                    </td>
                                    <input type="hidden" name="DAYA_TERIMA_ENERGI[]" value="" />
                                    <input type="hidden" name="DAYA_TERIMA_REAKTIF[]" value="" />
                                    <input type="hidden" name="DAYA_KIRIM_ENERGI[]" value="" />
                                    <input type="hidden" name="DAYA_KIRIM_REAKTIF[]" value="" />
                                    <input type="hidden" name="jenis_panel[]" value="<?php echo $rk->jenis_panel; ?>" />
                                    <input type="hidden" name="id[]" value="<?php echo $rk->id; ?>" />
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="hidden" name="header_id" value="<?php echo $header_id; ?>" />
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        <?= anchor('meteran','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function isNumberKey(evt)
  {
    var e = evt || window.event; //window.event is safer, thanks @ThiefMaster
    var charCode = e.which || e.keyCode;
    if (charCode > 31 && (charCode < 47 || charCode > 57))
    return false;
    if (e.shiftKey) return false;
    return true;
 }
</script>