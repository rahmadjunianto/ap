<!DOCTYPE html>
<html>

<head>
    <title>Meteran</title>
</head>
<style>
    .left {
        margin-left: 20px
    }
</style>

<body>
BIDANG FASILITAS BANDARA <br>
DIVISI ENERGY & POWER SUPPLY <br>
DINAS GARDU INDUK <br>

Tanggal : <?=$header->tanggal?> <br>
Pukul : <?= $header->pukul?><br>

    <table border="1" cellspacing="0" cellpadding="0" width="100%">
        <th style="font-size:10px ;padding:5" ead>
            <tr style="background-color: cadetblue">
                <th style="font-size:10px ;padding:5" rowspan="3">NO</th>
                <th style="font-size:10px ;padding:5" rowspan="3" align="center">Panel</th>
                <th style="font-size:10px ;padding:5" colspan="3" align="center">Tegangan (KV)</th>
                <th style="font-size:10px ;padding:5" colspan="3" align="center">Arus (A)</th>
                <th style="font-size:10px ;padding:5" colspan="4" align="center">Daya</th>
                <th style="font-size:10px ;padding:5" rowspan="3" align="center">Frekuensi (F)</th>
                <th style="font-size:10px ;padding:5" rowspan="3" align="center">Cos ( µ)</th>
            </tr>
            <tr style="background-color: cadetblue">
                <th style="font-size:10px ;padding:5" rowspan="2" align="center">L1</th>
                <th style="font-size:10px ;padding:5" rowspan="2" align="center">L2</th>
                <th style="font-size:10px ;padding:5" rowspan="2" align="center">L3</th>
                <th style="font-size:10px ;padding:5" rowspan="2" align="center">L1</th>
                <th style="font-size:10px ;padding:5" rowspan="2" align="center">L2</th>
                <th style="font-size:10px ;padding:5" rowspan="2" align="center">L3</th>
                <th style="font-size:10px ;padding:5" colspan="2" align="center">Terima</th>
                <th style="font-size:10px ;padding:5" colspan="2" align="center">Kirim</th>
            </tr>
            <tr style="background-color: cadetblue">
                <th style="font-size:10px ;padding:5" align="center">Energi <br>(KWH)</th>
                <th style="font-size:10px ;padding:5" align="center">Reaktif <br>(KvaRH)</th>
                <th style="font-size:10px ;padding:5" align="center">Energi <br>(KWH)</th>
                <th style="font-size:10px ;padding:5" align="center">Reaktif <br>(KvaRH)</th>
            </tr>
            </thead>
            <tbody>
                <?php $no=1; foreach ($panel_list1 as $rk ) { ?>

                <tr>
                    <td style="font-size:10px ; padding:5" align="center"><?= $no++?></td>
                    <td style="font-size:10px ; padding:5"><?= $rk->panel?>
                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->T_L1?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->T_L2?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->T_L3?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->A_L1?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->A_L2?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->A_L3?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->DAYA_TERIMA_ENERGI?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->DAYA_TERIMA_REAKTIF?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->DAYA_KIRIM_ENERGI?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->DAYA_KIRIM_REAKTIF?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->FREKUENSI?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->COS?>

                    </td>
                    <input type="hidden" name="DAYA_AKTIF[]" value="" />
                    <input type="hidden" name="DAYA_SEMU_1[]" value="" />
                    <input type="hidden" name="DAYA_SEMU_2[]" value="" />
                    <input type="hidden" name="DAYA_REAKTIF[]" value="" />
                    <input type="hidden" name="jenis_panel[]" value="<?php echo $rk->jenis_panel; ?>" />
                    <input type="hidden" name="id[]" value="<?php echo $rk->id; ?>" />
                </tr>
                <?php } ?>
                <tr style="background-color: cadetblue">
                    <th style="font-size:10px ;padding:5" rowspan="2">NO</th>
                    <th style="font-size:10px ;padding:5" rowspan="2" align="center">Panel</th>
                    <th style="font-size:10px ;padding:5" colspan="3" align="center">Tegangan (KV)</th>
                    <th style="font-size:10px ;padding:5" colspan="3" align="center">Arus (A)</th>
                    <th style="font-size:10px ;padding:5" rowspan="2" align="center">DAYA AKTIF (KW)</th>
                    <th style="font-size:10px ;padding:5" rowspan="2" colspan="2" align="center">DAYA SEMU (KVA)</th>
                    <th style="font-size:10px ;padding:5" rowspan="2" align="center">DAYA REAKTIF (KVaR)</th>
                    <th style="font-size:10px ;padding:5" rowspan="2" align="center">Frekuensi</th>
                    <th style="font-size:10px ;padding:5" rowspan="2" align="center">Cos ( µ)</th>
                </tr>
                <tr style="background-color: cadetblue">
                    <th style="font-size:10px ;padding:5" align="center">L1</th>
                    <th style="font-size:10px ;padding:5" align="center">L2</th>
                    <th style="font-size:10px ;padding:5" align="center">L3</th>
                    <th style="font-size:10px ;padding:5" align="center">L1</th>
                    <th style="font-size:10px ;padding:5" align="center">L2</th>
                    <th style="font-size:10px ;padding:5" align="center">L3</th>
                </tr>
                <?php foreach ($panel_list2 as $rk ) { ?>

                <tr>
                    <td style="font-size:10px ; padding:5" align="center"><?= $no++?></td>
                    <td style="font-size:10px ; padding:5"><?= $rk->panel?>
                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->T_L1?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->T_L2?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->T_L3?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->A_L1?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->A_L2?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->A_L3?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->DAYA_AKTIF?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->DAYA_SEMU_1?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->DAYA_SEMU_2?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->DAYA_REAKTIF?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->FREKUENSI?>

                    </td>
                    <td style="font-size:10px ; padding:5" align="center">
                        <?= $rk->COS?>

                    </td>
                    <input type="hidden" name="DAYA_TERIMA_ENERGI[]" value="" />
                    <input type="hidden" name="DAYA_TERIMA_REAKTIF[]" value="" />
                    <input type="hidden" name="DAYA_KIRIM_ENERGI[]" value="" />
                    <input type="hidden" name="DAYA_KIRIM_REAKTIF[]" value="" />
                    <input type="hidden" name="jenis_panel[]" value="<?php echo $rk->jenis_panel; ?>" />
                    <input type="hidden" name="id[]" value="<?php echo $rk->id; ?>" />
                </tr>
                <?php } ?>
            </tbody>
    </table>
<table border='0' width='100%'>
    <tbody>
        <tr>
            <td align="left"> ASSISTANT MANAGER OF HIGH & MEDIUM VOLTAGE STATION<br>
                <br><br><br>
                <img width="80" height="80" src="<?= base_url()?>/assets/qrcode/assisten/<?=qr_a($header->assistant_manager) ?>" alt=""><br>(<?= $header->assistant_manager?>)
            </td>
            <td align="left">Tangerang,
                <br>PETUGAS DINAS
                <br>
                <br>
                <br>
                <br>
                <img width="80" height="80" src="<?= base_url()?>/assets/qrcode/petugas/<?=qr_b($header->petugas_dinas) ?>" alt="">
                <br>(<?= $header->petugas_dinas?>)

            </td>
        </tr>
    </tbody>
</table>
</body>

</html>