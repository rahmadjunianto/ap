<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $title ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url() ?>adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url() ?>adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url() ?>adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>adminlte/dist/css/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url() ?>adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url() ?>adminlte/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?= base_url() ?>adminlte/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <!-- <link rel="stylesheet" href="<?= base_url() ?>adminlte/bower_components/jvectormap/jquery-jvectormap.css"> -->
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?= base_url() ?>adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="<?= base_url() ?>adminlte/https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="<?= base_url() ?>adminlte/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?= base_url() ?>adminlte/plugins/iCheck/all.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <!-- <span class="logo-lg"><img src="<?= base_url(  ) ?>assets/logo.jpg" alt="" srcset="" height="150px" width="150px"></span> -->
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <!-- <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a> -->

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php
                  if(foto()==null){
                    $img=base_url().'adminlte/dist/img/user2-160x160.jpg';
                  } else {

                    $img=base_url().'assets/foto/'.foto();
                  }
              ?>
              <img src="<?=$img?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('app_nama'); ?> </span>
            </a>

          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php $this->load->view('menu'); ?>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?= $title ?>
      </h1>
      <ol class="breadcrumb" style="top:5px">

      <div class="btn-group float-right">
              <?php
                if(isset($akses)){
                    if($akses['is_create']==1){
                        echo anchor($create,'<i class="mdi mdi-note-plus"></i> Tambah','class="btn btn-success"');
                          }
                      }
                      if(isset($kembali)){
                          echo anchor($kembali,'<i class="mdi mdi-backburger"></i> kembali','class="btn btn-info"');
                      }
                      if(isset($import)){
                          echo anchor($import,'<i class="mdi mdi-backburger"></i> import','class="btn btn-info"');
                      }
              ?>
        </div>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?= $contents ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.13
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?= base_url() ?>adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url() ?>adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url() ?>adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?= base_url() ?>adminlte/bower_components/raphael/raphael.min.js"></script>
<!-- <script src="<?= base_url() ?>adminlte/bower_components/morris.js/morris.min.js"></script> -->
<!-- Sparkline -->
<script src="<?= base_url() ?>adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?= base_url() ?>adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= base_url() ?>adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= base_url() ?>adminlte/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?= base_url() ?>adminlte/bower_components/moment/min/moment.min.js"></script>
<script src="<?= base_url() ?>adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?= base_url() ?>adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= base_url() ?>adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?= base_url() ?>adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>adminlte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>adminlte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?= base_url() ?>adminlte/dist/js/pages/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>adminlte/dist/js/demo.js"></script>
<script src="<?= base_url() ?>node_modules/sweetalert2/dist/sweetalert2.all.min.js"></script>
<script src="<?= base_url() ?>node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
<link rel="<?= base_url() ?>node_modules/sweetalert2/dist/stylesheet" href="sweetalert2.min.css">
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>adminlte/plugins/iCheck/icheck.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url() ?>adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url() ?>adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="<?= base_url()?>/assets/plugins/datepicker/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?= base_url()?>/assets/plugins/datetimepicker/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="<?= base_url()?>/assets/plugins/datetimepicker/bootstrap-datetimepicker.min.css">
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<script src="<?= base_url()?>/assets/plugins/datetimepicker/bootstrap-datetimepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>

<script type="text/javascript" src="<?= base_url()?>/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<?php if(isset($script)){ $this->load->view($script);  }?>
<!--
<link rel="stylesheet" href="/assets/plugins/datepicker/bootstrap-datepicker.css">
<link rel="stylesheet" href="/assets/plugins/datetimepicker/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="/assets/plugins/datetimepicker/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="/assets/plugins/select2/select2.min.css">


<script src="/assets/plugins/jQueryUI/jquery-ui.js"></script>
<script type="text/javascript" src="/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/assets/plugins/select2/select2.full.min.js"></script> -->
<script>
    $(".bulanTahun").datepicker({
        format: "M yyyy",
        viewMode: "months",
        minViewMode: "months",
        autoclose: true,
        todayBtn: 1,
    });
</script>
<script>
  $('.number').keypress(function(event) {
  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});
      function isNumberKey(evt) {
        var e = evt || window.event; //window.event is safer, thanks @ThiefMaster
        var charCode = e.which || e.keyCode;
        if (charCode > 31 && (charCode < 47 || charCode > 57))
            return false;
        if (e.shiftKey) return false;
        return true;
    }
function ribuan(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}
 $(".year").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years",
                      autoclose:true});
                $('.datepicker').datepicker({
                      format: 'dd-mm-yyyy',
                      autoclose:true
    // startDate: '-3d'
                    });
  function formatangka(objek) {
     a = objek.value;
     b = a.replace(/[^\d]/g,"");
     c = "";
     panjang = b.length;
     j = 0;
     for (i = panjang; i > 0; i--) {
       j = j + 1;
       if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + "." + c;
       } else {
         c = b.substr(i-1,1) + c;
       }
     }
     objek.value = c;
  };
<?php  if(!empty(get_flashdata('icon'))){?>

  Swal.fire({
    type: '<?= get_flashdata("icon")?>',
    title: '<?= get_flashdata("tittle")?>',
    text: '<?= get_flashdata("msg")?>'
  })
<?php } ?>
function swalAlert(type,title,text) {
  Swal.fire({
        type: type,
        title: title,
        text: text
    })
}
var base='<?= base_url() ?>'
          var seg='<?= strtolower($this->uri->segment(1)); ?>';
          $('.treeview  a[href~="' + base+seg + '"]').parents('li').addClass('active');
          $('a[href~="' + base+seg + '"]').parents('li').addClass('active');
$(document).ready(function(){
  $("ul.pagination").children().removeClass( "active" );
  $("a.page-link").parent().addClass("active");
});
$('.jam').datetimepicker({
            format: 'HH:ii:ss',
            language: 'id',
            defaultDate: new Date(),
            weekStart: 1,
            autoclose: 1,
            minuteStep: 1,
            startView: 1,
        }).on('changeDate', function (e) {});
</script>
</body>
</html>
