<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Asisten_manajer extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Asisten_manajer_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
        $this->load->library('ciqrcode');
        $this->load->model('Pegawai_model');
    }

    private function cekAkses($var=null){
        $url='Asisten_manajer';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'asisten_manajer?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'asisten_manajer?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'asisten_manajer';
            $config['first_url'] = base_url() . 'asisten_manajer';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Asisten_manajer_model->total_rows($q);
        $asisten_manajer                      = $this->Asisten_manajer_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'asisten_manajer_data' => $asisten_manajer,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Asisten_manajer',
            'create'              => 'Asisten_manajer/create',
            'akses'               =>$akses
        );
        $this->template->load('layout','asisten_manajer/Asisten_manajer_list', $data);
    }



    public function create()
    {
        $this->cekAkses('create');

        $data = array(
            'title'   =>'Tambah Data Asisten_manajer',
            'kembali' =>'Asisten_manajer',
            'action'  => site_url('asisten_manajer/create_action'),
	    'id' => set_value('id'),
	    'nama' => set_value('nama'),
        'nip' => set_value('nip'),
        'pegawai'      => $this->Pegawai_model->get_all(),
        'script'       => 'petugas/js_petugas'
	);
        $this->template->load('layout','asisten_manajer/Asisten_manajer_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $qr['data'] = $this->input->post('nama', TRUE) . "+" . $this->input->post('nip', TRUE);
            $qr['level'] = 'H';
            $qr['size'] = 10;
            $nama_qr = sha1('qrcode_' . mt_rand()) . '.png';
            $qr['savename'] = './assets/qrcode/assisten/' . $nama_qr;
            $this->ciqrcode->generate($qr);
            $data = array(
                'nama' => $this->input->post('nama', TRUE),
                'nip' => $this->input->post('nip', TRUE),
                'qr' => $nama_qr
            );

            $this->Asisten_manajer_model->insert($data);
            set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('asisten_manajer'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Asisten_manajer_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'Edit data Asisten_manajer',
                'action' => site_url('asisten_manajer/update_action'),
                'kembali' =>'Asisten_manajer',
		'id' => set_value('id', $row->id),
		'nama' => set_value('nama', $row->nama),
		'nip' => set_value('nip', $row->nip),
	    );
            $this->template->load('layout','asisten_manajer/Asisten_manajer_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('asisten_manajer'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $qr['data'] = $this->input->post('nama', TRUE) . "+" . $this->input->post('nip', TRUE);
            $qr['level'] = 'H';
            $qr['size'] = 10;
            $nama_qr = sha1('qrcode_' . mt_rand()) . '.png';
            $qr['savename'] = './assets/qrcode/assisten/' . $nama_qr;
            $this->ciqrcode->generate($qr);
            $data = array(
                'nama' => $this->input->post('nama', TRUE),
                'nip' => $this->input->post('nip', TRUE),
                'qr' => $nama_qr
            );
            $this->Asisten_manajer_model->update($this->input->post('id', TRUE), $data);
            set_flashdata('success', 'Update Record Success');
            redirect(site_url('asisten_manajer'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Asisten_manajer_model->get_by_id($id);

        if ($row) {
            $this->Asisten_manajer_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('asisten_manajer'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('asisten_manajer'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Asisten_manajer.php */
/* Location: ./application/controllers/Asisten_manajer.php */