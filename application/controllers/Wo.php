<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wo extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Wo_model');
        $this->load->model('Wo_material_model');
        $this->load->model('Wo_teknisi_model');
        $this->load->model('Teknisi_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
    }

    private function cekAkses($var=null){
        $url='Wo';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal     = urldecode($this->input->get('tanggal', TRUE));
        $nomor     = urldecode($this->input->get('nomor', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'wo?q=' . urlencode($q)."&tanggal=".urlencode($tanggal)."&nomor=".urlencode($nomor);
            $config['first_url'] = base_url() . 'wo?q=' . urlencode($q)."&tanggal=".urlencode($tanggal)."&nomor=".urlencode($nomor);
        } else {
            $config['base_url']  = base_url() . 'wo';
            $config['first_url'] = base_url() . 'wo';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
		$tanggal<>"" ? $this->db->where('tanggal', date("Y-m-d", strtotime($tanggal))):"";
        $config['total_rows']        = $this->Wo_model->total_rows($q);
		$tanggal<>"" ? $this->db->where('tanggal', date("Y-m-d", strtotime($tanggal))):"";
        $wo                      = $this->Wo_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'wo_data' => $wo,
            'tanggal'                   => $tanggal,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Wo',
            'create'              => 'Wo/create',
            'akses'               =>$akses
        );
        $this->template->load('layout','wo/Wo_list', $data);
    }

    public function cetak($ide)
    {
        $id = rapikan($ide);

        $data['wo'] = $this->Wo_model->get_by_id($id);
        $data['material'] = $this->Wo_material_model->get_by_id($id);
        $data['teknisi'] = $this->Wo_teknisi_model->get_by_id($id);
        $data['klasifikasi']=array(
            '1' => 'Preventive',
            '2' =>'Predictive',
            '3' => 'Corrective'
        );
		// $html = $this->load->view('wo/cetak', $data);
		$html = $this->load->view('wo/cetak', $data, true);
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$mpdf->WriteHTML($html);
		$mpdf->Output('wo.pdf', 'I');
    }

    public function create()
    {
        $this->cekAkses('create');

        $data = array(
            'title'   =>'Tambah Data Wo',
            'kembali' =>'Wo',
            'action'  => site_url('wo/create_action'),
	    'id' => set_value('id'),
	    'tanggal' => set_value('tanggal'),
	    'nomor' => set_value('nomor'),
	    'klasifikasi' => set_value('klasifikasi'),
	    'lokasi_pekerjaan' => set_value('lokasi_pekerjaan'),
	    'jenis_pekerjaan' => set_value('jenis_pekerjaan'),
	    'uraian_pekerjaan' => set_value('uraian_pekerjaan'),
	    'catatan' => set_value('catatan'),
	    'ttd_1' => set_value('ttd_1'),
	    'ttd_2' => set_value('ttd_2'),
	    'mulai' => set_value('mulai'),
        'selesai' => set_value('selesai'),
        'script' => 'wo/wo_js',
        'teknisi' => $this->Teknisi_model->get_all(),
	);
        $this->template->load('layout','wo/Wo_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $this->db->trans_start();
            $data = array(
		'klasifikasi' => $this->input->post('klasifikasi',TRUE),
		'lokasi_pekerjaan' => $this->input->post('lokasi_pekerjaan',TRUE),
		'jenis_pekerjaan' => $this->input->post('jenis_pekerjaan',TRUE),
		'uraian_pekerjaan' => $this->input->post('uraian_pekerjaan',TRUE),
		'catatan' => $this->input->post('catatan',TRUE),
		'mulai' => $this->input->post('mulai',TRUE),
		'selesai' => $this->input->post('selesai',TRUE),
		'nomor' => nomor_wo(),
	    );

        $this->db->set('tanggal', "STR_TO_DATE('" . $this->input->post('tanggal', TRUE) . "','%d-%m-%Y')", false);
            $this->Wo_model->insert($data);
            $wo_id=$this->db->insert_id();
			for ($i=0; $i < count($this->input->post('teknisi_id', TRUE)); $i++) {
				$data = array(
					'wo_id' => $wo_id ,
					'teknisi_id' => $this->input->post('teknisi_id', TRUE)[$i]
				);
                $this->Wo_teknisi_model->insert($data);
            }
			for ($i=0; $i < count($this->input->post('material', TRUE)); $i++) {
				$data = array(
					'wo_id' => $wo_id ,
					'material' => $this->input->post('material', TRUE)[$i],
					'jumlah' => $this->input->post('jumlah', TRUE)[$i],
				);
                $this->Wo_material_model->insert($data);
			}
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                set_flashdata('warning', 'Data tidak tersimpan.');
            } else {
                set_flashdata('success', 'Data telah di simpan.');
            }
            redirect(site_url('wo'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Wo_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'Edit data Wo',
                'action' => site_url('wo/update_action'),
                'kembali' =>'Wo',
		'id' => set_value('id', $row->id),
        'tanggal' => set_value('tanggal', date("d-m-Y", strtotime($row->tanggal))),
		'klasifikasi' => set_value('klasifikasi', $row->klasifikasi),
		'lokasi_pekerjaan' => set_value('lokasi_pekerjaan', $row->lokasi_pekerjaan),
		'jenis_pekerjaan' => set_value('jenis_pekerjaan', $row->jenis_pekerjaan),
		'uraian_pekerjaan' => set_value('uraian_pekerjaan', $row->uraian_pekerjaan),
		'catatan' => set_value('catatan', $row->catatan),
		'mulai' => set_value('mulai', $row->mulai),
        'selesai' => set_value('selesai', $row->selesai),
        'teknisi_list' => $this->Wo_teknisi_model->get_all($row->id),
        'material_list' => $this->Wo_material_model->get_all($row->id),
        'script' => 'wo/wo_js',
        'teknisi' => $this->Teknisi_model->get_all()
	    );
            $this->template->load('layout','wo/Wo_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('wo'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $this->db->trans_start();
            $data = array(
		'klasifikasi' => $this->input->post('klasifikasi',TRUE),
		'lokasi_pekerjaan' => $this->input->post('lokasi_pekerjaan',TRUE),
		'jenis_pekerjaan' => $this->input->post('jenis_pekerjaan',TRUE),
		'uraian_pekerjaan' => $this->input->post('uraian_pekerjaan',TRUE),
		'catatan' => $this->input->post('catatan',TRUE),
		'mulai' => $this->input->post('mulai',TRUE),
		'selesai' => $this->input->post('selesai',TRUE),
	    );
        $this->db->set('tanggal', "STR_TO_DATE('" . $this->input->post('tanggal', TRUE) . "','%d-%m-%Y')", false);

            $this->Wo_model->update($this->input->post('id', TRUE), $data);
            $wo_id=$this->input->post('id', TRUE);
			$this->Wo_teknisi_model->delete($wo_id);
			$this->Wo_material_model->delete($wo_id);
			for ($i=0; $i < count($this->input->post('teknisi_id', TRUE)); $i++) {
				$data = array(
					'wo_id' => $wo_id ,
					'teknisi_id' => $this->input->post('teknisi_id', TRUE)[$i]
				);
                $this->Wo_teknisi_model->insert($data);
            }
			for ($i=0; $i < count($this->input->post('material', TRUE)); $i++) {
				$data = array(
					'wo_id' => $wo_id ,
					'material' => $this->input->post('material', TRUE)[$i],
					'jumlah' => $this->input->post('jumlah', TRUE)[$i],
				);
                $this->Wo_material_model->insert($data);
			}
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                set_flashdata('warning', 'Data tidak tersimpan.');
            } else {
                set_flashdata('success', 'Data telah di simpan.');
            }
			set_flashdata('success', 'Update Record Success');
			redirect(site_url('wo'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Wo_model->get_by_id($id);

        if ($row) {
            $this->Wo_model->delete($id);
            $this->Wo_material_model->delete($id);
            $this->Wo_teknisi_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('wo'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('wo'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');
	$this->form_validation->set_rules('klasifikasi', 'klasifikasi', 'trim|required');
	$this->form_validation->set_rules('lokasi_pekerjaan', 'lokasi pekerjaan', 'trim|required');
	$this->form_validation->set_rules('jenis_pekerjaan', 'jenis pekerjaan', 'trim|required');
	$this->form_validation->set_rules('uraian_pekerjaan', 'uraian pekerjaan', 'trim|required');
	$this->form_validation->set_rules('catatan', 'catatan', 'trim|required');
	$this->form_validation->set_rules('mulai', 'mulai', 'trim|required');
	$this->form_validation->set_rules('selesai', 'selesai', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Wo.php */
/* Location: ./application/controllers/Wo.php */