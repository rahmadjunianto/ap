<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pegawai extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pegawai_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
    }

    private function cekAkses($var=null){
        $url='Pegawai';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'pegawai?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pegawai?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'pegawai';
            $config['first_url'] = base_url() . 'pegawai';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $this->db->where_not_in('kd_pegawai',[1,2]);
        $config['total_rows']        = $this->Pegawai_model->total_rows($q);
        $this->db->where_not_in('kd_pegawai',[1,2]);
        $pegawai                      = $this->Pegawai_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pegawai_data' => $pegawai,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Pegawai',
            'create'              => 'Pegawai/create',
            'akses'               =>$akses
        );
        $this->template->load('layout','pegawai/Pegawai_list', $data);
    }



    public function create()
    {
        $this->cekAkses('create');

        $data = array(
            'title'   =>'Tambah Data Pegawai',
            'kembali' =>'Pegawai',
            'action'  => site_url('pegawai/create_action'),
	    'nip' => set_value('nip'),
	    'kd_pegawai' => set_value('kd_pegawai'),
	    'nm_pegawai' => set_value('nm_pegawai'),
	    'jabatan' => set_value('jabatan'),
	);
        $this->template->load('layout','pegawai/Pegawai_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nip' => $this->input->post('nip',TRUE),
		'nm_pegawai' => $this->input->post('nm_pegawai',TRUE),
		'jabatan' => $this->input->post('jabatan',TRUE),
	    );

            $this->Pegawai_model->insert($data);
            set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('pegawai'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Pegawai_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'Edit data Pegawai',
                'action' => site_url('pegawai/update_action'),
                'kembali' =>'Pegawai',
		'nip' => set_value('nip', $row->nip),
		'kd_pegawai' => set_value('kd_pegawai', $row->kd_pegawai),
		'nm_pegawai' => set_value('nm_pegawai', $row->nm_pegawai),
		'jabatan' => set_value('jabatan', $row->jabatan),
	    );
            $this->template->load('layout','pegawai/Pegawai_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('pegawai'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kd_pegawai', TRUE));
        } else {
            $data = array(
		'nip' => $this->input->post('nip',TRUE),
		'nm_pegawai' => $this->input->post('nm_pegawai',TRUE),
		'jabatan' => $this->input->post('jabatan',TRUE),
	    );

            $this->Pegawai_model->update($this->input->post('kd_pegawai', TRUE), $data);
            set_flashdata('success', 'Update Record Success');
            redirect(site_url('pegawai'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Pegawai_model->get_by_id($id);

        if ($row) {
            $this->Pegawai_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('pegawai'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('pegawai'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nip', 'nip', 'trim|required');
	$this->form_validation->set_rules('nm_pegawai', 'nm pegawai', 'trim|required');

	$this->form_validation->set_rules('kd_pegawai', 'kd_pegawai', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Pegawai.php */
/* Location: ./application/controllers/Pegawai.php */