<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Log_book extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Log_book_model');
		$this->load->model('Log_book_detail_model');
		$this->load->model('Log_book_petugas_model');
		$this->load->model('Petugas_model');
		$this->load->library('form_validation');
		$this->id_pengguna = get_userdata('app_id_pengguna');
	}

	private function cekAkses($var = null)
	{
		$url = 'Log_book';
		return cek($this->id_pengguna, $url, $var);
	}

	public function index()

	{
		$akses = $this->cekAkses('read');
		$q     = urldecode($this->input->get('q', TRUE));
        $tanggal     = urldecode($this->input->get('tanggal', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> ''||$tanggal<>'') {
			$config['base_url']  = base_url() . 'log_book?q=' . urlencode($q)."tanggal=".urlencode($tanggal);
			$config['first_url'] = base_url() . 'log_book?q=' . urlencode($q)."tanggal=".urlencode($tanggal);
		} else {
			$config['base_url']  = base_url() . 'log_book';
			$config['first_url'] = base_url() . 'log_book';
		}

		$config['per_page']          = 10;
		$config['page_query_string'] = TRUE;
		$tanggal<>"" ? $this->db->where('tanggal', date("Y-m-d", strtotime($tanggal))):"";
		$config['total_rows']        = $this->Log_book_model->total_rows($q);
		$tanggal<>"" ? $this->db->where('tanggal', date("Y-m-d", strtotime($tanggal))):"";
		$log_book                      = $this->Log_book_model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'log_book_data' => $log_book,
			'q'                   => $q,
			'tanggal'                   => $tanggal,
			'pagination'          => $this->pagination->create_links(),
			'total_rows'          => $config['total_rows'],
			'start'               => $start,
			'title'               => 'Data Log_book',
			'create'              => 'Log_book/create',
			'akses'               => $akses
		);
		$this->template->load('layout', 'log_book/Log_book_list', $data);
	}



	public function create()
	{
		$this->cekAkses('create');

		$data = array(
			'title'   => 'Tambah Data Log_book',
			'kembali' => 'Log_book',
			'action'  => site_url('log_book/create_action'),
			'id' => set_value('id'),
			'tanggal' => set_value('tanggal'),
			'shift' => set_value('shift'),
			'gangguan_permanen' => set_value('gangguan_permanen'),
			'pg_incoming_1' => set_value('pg_incoming_1'),
			'pg_incoming_2' => set_value('pg_incoming_2'),
			'pg_outgoing_1' => set_value('pg_outgoing_1'),
			'pg_outgoing_2' => set_value('pg_outgoing_2'),
			'pg_coupler' => set_value('pg_coupler'),
			'pg_bust_vt' => set_value('pg_bust_vt'),
			'pg_spare_1' => set_value('pg_spare_1'),
			'm_daya_1' => set_value('m_daya_1'),
			'm_arus_1' => set_value('m_arus_1'),
			'm_tegangan_1' => set_value('m_tegangan_1'),
			'm_cos_phi_1' => set_value('m_cos_phi_1'),
			'm_frequensi_1' => set_value('m_frequensi_1'),
			'm_daya_2' => set_value('m_daya_2'),
			'm_arus_2' => set_value('m_arus_2'),
			'm_tegangan_2' => set_value('m_tegangan_2'),
			'm_cos_phi_2' => set_value('m_cos_phi_2'),
			'm_frequensi_2' => set_value('m_frequensi_2'),
			'cm_sas' => set_value('cm_sas'),
			'cm_rcu' => set_value('cm_rcu'),
			'temp_t1' => set_value('temp_t1'),
			'temp_t2' => set_value('temp_t2'),
			'tap_t1' => set_value('tap_t1'),
			'tap_t2' => set_value('tap_t2'),
			'petugas' => $this->Petugas_model->get_all(),
			'petugas_list' => [],
			'script' => 'log_book/log_book_js'
		);
		$this->template->load('layout', 'log_book/Log_book_form', $data);
	}

	public function create_action()
	{
		$this->cekAkses('create');

		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {

            $this->db->trans_start();
			$data = array(
				'shift' => $this->input->post('shift', TRUE),
				'gangguan_permanen' => $this->input->post('gangguan_permanen', TRUE),
				'pg_incoming_1' => $this->input->post('pg_incoming_1', TRUE),
				'pg_incoming_2' => $this->input->post('pg_incoming_2', TRUE),
				'pg_outgoing_1' => $this->input->post('pg_outgoing_1', TRUE),
				'pg_outgoing_2' => $this->input->post('pg_outgoing_2', TRUE),
				'pg_coupler' => $this->input->post('pg_coupler', TRUE),
				'pg_bust_vt' => $this->input->post('pg_bust_vt', TRUE),
				'pg_spare_1' => $this->input->post('pg_spare_1', TRUE),
				'm_daya_1' => $this->input->post('m_daya_1', TRUE),
				'm_arus_1' => $this->input->post('m_arus_1', TRUE),
				'm_tegangan_1' => $this->input->post('m_tegangan_1', TRUE),
				'm_cos_phi_1' => $this->input->post('m_cos_phi_1', TRUE),
				'm_frequensi_1' => $this->input->post('m_frequensi_1', TRUE),
				'm_daya_2' => $this->input->post('m_daya_2', TRUE),
				'm_arus_2' => $this->input->post('m_arus_2', TRUE),
				'm_tegangan_2' => $this->input->post('m_tegangan_2', TRUE),
				'm_cos_phi_2' => $this->input->post('m_cos_phi_2', TRUE),
				'm_frequensi_2' => $this->input->post('m_frequensi_2', TRUE),
				'cm_sas' => $this->input->post('cm_sas', TRUE),
				'cm_rcu' => $this->input->post('cm_rcu', TRUE),
				'temp_t1' => $this->input->post('temp_t1', TRUE),
				'temp_t2' => $this->input->post('temp_t2', TRUE),
				'tap_t1' => $this->input->post('tap_t1', TRUE),
				'tap_t2' => $this->input->post('tap_t2', TRUE),
			);
			$this->db->set('tanggal', "STR_TO_DATE('" . $this->input->post('tanggal', TRUE) . "','%d-%m-%Y')", false);
			$this->Log_book_model->insert($data);
			$log_book_id=$this->db->insert_id();
			for ($i=0; $i < count($this->input->post('jam', TRUE)); $i++) {
				$data = array(
					'jam' => $this->input->post('jam', TRUE)[$i],
					'uraian' => $this->input->post('uraian', TRUE)[$i],
					'tindak_lanjut' => $this->input->post('tindak_lanjut', TRUE)[$i],
					'selesai' => $this->input->post('selesai', TRUE)[$i],
					'hasil' => $this->input->post('hasil', TRUE)[$i],
					'ket' => $this->input->post('ket', TRUE)[$i],
					'log_book_id' => $log_book_id

				);
                $this->Log_book_detail_model->insert($data);
			}
			for ($i=0; $i < count($this->input->post('petugas_id', TRUE)); $i++) {
				$data = array(
					'log_book_id' => $log_book_id ,
					'petugas_id' => $this->input->post('petugas_id', TRUE)[$i]
				);
                $this->Log_book_petugas_model->insert($data);
			}
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                set_flashdata('warning', 'Data tidak tersimpan.');
            } else {
                set_flashdata('success', 'Data telah di simpan.');
            }
            redirect(site_url('log_book'));
		}
	}

	public function update($ide)
	{
		$this->cekAkses('update');
		$id = rapikan($ide);
		$row = $this->Log_book_model->get_by_id($id);

		if ($row) {
			$data = array(
				'title' => 'Edit data Log_book',
				'action' => site_url('log_book/update_action'),
				'kembali' => 'Log_book',
				'id' => set_value('id', $row->id),
				'tanggal' => set_value('tanggal', date("d-m-Y", strtotime($row->tanggal))),
				'shift' => set_value('shift', $row->shift),
				'gangguan_permanen' => set_value('gangguan_permanen', $row->gangguan_permanen),
				'pg_incoming_1' => set_value('pg_incoming_1', $row->pg_incoming_1),
				'pg_incoming_2' => set_value('pg_incoming_2', $row->pg_incoming_2),
				'pg_outgoing_1' => set_value('pg_outgoing_1', $row->pg_outgoing_1),
				'pg_outgoing_2' => set_value('pg_outgoing_2', $row->pg_outgoing_2),
				'pg_coupler' => set_value('pg_coupler', $row->pg_coupler),
				'pg_bust_vt' => set_value('pg_bust_vt', $row->pg_bust_vt),
				'pg_spare_1' => set_value('pg_spare_1', $row->pg_spare_1),
				'm_daya_1' => set_value('m_daya_1', $row->m_daya_1),
				'm_arus_1' => set_value('m_arus_1', $row->m_arus_1),
				'm_tegangan_1' => set_value('m_tegangan_1', $row->m_tegangan_1),
				'm_cos_phi_1' => set_value('m_cos_phi_1', $row->m_cos_phi_1),
				'm_frequensi_1' => set_value('m_frequensi_1', $row->m_frequensi_1),
				'm_daya_2' => set_value('m_daya_2', $row->m_daya_2),
				'm_arus_2' => set_value('m_arus_2', $row->m_arus_2),
				'm_tegangan_2' => set_value('m_tegangan_2', $row->m_tegangan_2),
				'm_cos_phi_2' => set_value('m_cos_phi_2', $row->m_cos_phi_2),
				'm_frequensi_2' => set_value('m_frequensi_2', $row->m_frequensi_2),
				'cm_sas' => set_value('cm_sas', $row->cm_sas),
				'cm_rcu' => set_value('cm_rcu', $row->cm_rcu),
				'temp_t1' => set_value('temp_t1', $row->temp_t1),
				'temp_t2' => set_value('temp_t2', $row->temp_t2),
				'tap_t1' => set_value('tap_t1', $row->tap_t1),
				'tap_t2' => set_value('tap_t2', $row->tap_t2),
				'detail' => $this->Log_book_detail_model->get_by_id($row->id),
				'petugas_list' => $this->Log_book_petugas_model->get_by_id($row->id),
				'petugas' => $this->Petugas_model->get_all(),
				'script' => 'log_book/log_book_js'
			);
			$this->template->load('layout', 'log_book/Log_book_form', $data);
		} else {
			set_flashdata('warning', 'Record Not Found.');
			redirect(site_url('log_book'));
		}
	}
    public function cetak($ide)
    {
        $id = rapikan($ide);

        $data['header'] = $this->Log_book_model->get_by_id($id);
        $data['detail'] = $this->Log_book_detail_model->get_by_id($id);
        $data['petugas_list'] = $this->Log_book_petugas_model->get_by_id($id);
		$html = $this->load->view('log_book/cetak', $data, true);
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$mpdf->WriteHTML($html);
		$mpdf->Output('log_book.pdf', 'I');
    }
	public function update_action()
	{
		$this->cekAkses('update');
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id', TRUE));
		} else {

            $this->db->trans_start();
			$data = array(
				'shift' => $this->input->post('shift', TRUE),
				'gangguan_permanen' => $this->input->post('gangguan_permanen', TRUE),
				'pg_incoming_1' => $this->input->post('pg_incoming_1', TRUE),
				'pg_incoming_2' => $this->input->post('pg_incoming_2', TRUE),
				'pg_outgoing_1' => $this->input->post('pg_outgoing_1', TRUE),
				'pg_outgoing_2' => $this->input->post('pg_outgoing_2', TRUE),
				'pg_coupler' => $this->input->post('pg_coupler', TRUE),
				'pg_bust_vt' => $this->input->post('pg_bust_vt', TRUE),
				'pg_spare_1' => $this->input->post('pg_spare_1', TRUE),
				'm_daya_1' => $this->input->post('m_daya_1', TRUE),
				'm_arus_1' => $this->input->post('m_arus_1', TRUE),
				'm_tegangan_1' => $this->input->post('m_tegangan_1', TRUE),
				'm_cos_phi_1' => $this->input->post('m_cos_phi_1', TRUE),
				'm_frequensi_1' => $this->input->post('m_frequensi_1', TRUE),
				'm_daya_2' => $this->input->post('m_daya_2', TRUE),
				'm_arus_2' => $this->input->post('m_arus_2', TRUE),
				'm_tegangan_2' => $this->input->post('m_tegangan_2', TRUE),
				'm_cos_phi_2' => $this->input->post('m_cos_phi_2', TRUE),
				'm_frequensi_2' => $this->input->post('m_frequensi_2', TRUE),
				'cm_sas' => $this->input->post('cm_sas', TRUE),
				'cm_rcu' => $this->input->post('cm_rcu', TRUE),
				'temp_t1' => $this->input->post('temp_t1', TRUE),
				'temp_t2' => $this->input->post('temp_t2', TRUE),
				'tap_t1' => $this->input->post('tap_t1', TRUE),
				'tap_t2' => $this->input->post('tap_t2', TRUE),
			);

			$this->Log_book_model->update($this->input->post('id', TRUE), $data);
			$log_book_id=$this->input->post('id', TRUE);
			$this->Log_book_detail_model->delete($log_book_id);
			$this->Log_book_petugas_model->delete($log_book_id);
			for ($i=0; $i < count($this->input->post('jam', TRUE)); $i++) {
				$data = array(
					'jam' => $this->input->post('jam', TRUE)[$i],
					'uraian' => $this->input->post('uraian', TRUE)[$i],
					'tindak_lanjut' => $this->input->post('tindak_lanjut', TRUE)[$i],
					'selesai' => $this->input->post('selesai', TRUE)[$i],
					'hasil' => $this->input->post('hasil', TRUE)[$i],
					'ket' => $this->input->post('ket', TRUE)[$i],
					'log_book_id' => $log_book_id

				);
                $this->Log_book_detail_model->insert($data);
			}
			for ($i=0; $i < count($this->input->post('petugas_id', TRUE)); $i++) {
				$data = array(
					'log_book_id' => $log_book_id ,
					'petugas_id' => $this->input->post('petugas_id', TRUE)[$i]
				);
                $this->Log_book_petugas_model->insert($data);
			}
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                set_flashdata('warning', 'Data tidak tersimpan.');
            } else {
                set_flashdata('success', 'Data telah di simpan.');
            }
			set_flashdata('success', 'Update Record Success');
			redirect(site_url('log_book'));
		}
	}

	public function delete($ide)
	{
		$this->cekAkses('delete');
		$id = rapikan($ide);
		$row = $this->Log_book_model->get_by_id($id);

		if ($row) {
			$this->Log_book_model->delete($id);
			$this->Log_book_detail_model->delete($id);
			$this->Log_book_petugas_model->delete($id);
			set_flashdata('success', 'Delete Record Success');
			redirect(site_url('log_book'));
		} else {
			set_flashdata('message', 'Record Not Found');
			redirect(site_url('log_book'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');
		$this->form_validation->set_rules('shift', 'shift', 'trim|required');
		$this->form_validation->set_rules('gangguan_permanen', 'gangguan permanen', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
}

/* End of file Log_book.php */
/* Location: ./application/controllers/Log_book.php */
