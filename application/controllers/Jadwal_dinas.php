<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    use Carbon\Carbon;
use Carbon\CarbonPeriod;

class Jadwal_dinas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Jadwal_dinas_model');
        $this->load->model('Pegawai_model');
        $this->load->library('form_validation');
        $this->id_pengguna = get_userdata('app_id_pengguna');
    }

    private function cekAkses($var = null)
    {
        $url = 'Jadwal_dinas';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()

    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal     = urldecode($this->input->get('tanggal', TRUE));
        $pegawai     = urldecode($this->input->get('pegawai', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '' || $tanggal<>''|| $pegawai<>'') {
            $config['base_url']  = base_url() . 'jadwal_dinas?q=' . urlencode($q)."&tanggal=".urlencode($tanggal)."&pegawai=".urlencode($pegawai);
            $config['first_url'] = base_url() . 'jadwal_dinas?q=' . urlencode($q)."&tanggal=".urlencode($tanggal)."&pegawai=".urlencode($pegawai);
        } else {
            $config['base_url']  = base_url() . 'jadwal_dinas';
            $config['first_url'] = base_url() . 'jadwal_dinas';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $tanggal<>"" ? $this->db->where('tanggal', date("Y-m-d", strtotime($tanggal))):"";
        $pegawai<>"" ? $this->db->where('pegawai_id', $pegawai):"";
        $config['total_rows']        = $this->Jadwal_dinas_model->total_rows($q);
        $tanggal<>"" ? $this->db->where('tanggal', date("Y-m-d", strtotime($tanggal))):"";
        $pegawai<>"" ? $this->db->where('pegawai_id', $pegawai):"";
        $jadwal_dinas                      = $this->Jadwal_dinas_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $this->db->where_not_in('kd_pegawai', [1, 2]);
        $pegawai_list = $this->Pegawai_model->get_all();
        $data = array(
            'jadwal_dinas_data' => $jadwal_dinas,
            'q'                   => $q,
            'tanggal'                   => $tanggal,
            'pegawai'                   => $pegawai,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Jadwal_dinas',
            'create'              => 'Jadwal_dinas/create',
            'akses'               => $akses,
            'pegawai_list' => $pegawai_list,
            'keterangan'          => array('D' => 'Dinas Pagi Staf (D)', 'PS' => 'Dinas Pagi-Siang Ops (PS)', 'M' => 'Dinas Malam Ops (M)')
        );
        $this->template->load('layout', 'jadwal_dinas/Jadwal_dinas_list', $data);
    }

    public function Rekap()
    {
        $akses = $this->cekAkses('read');
        $bulanTahun     = urldecode($this->input->get('bulanTahun', TRUE));
        $xx=date("Y-m", strtotime($bulanTahun));
        $cetak= base_url() . 'jadwal_dinas/cetakRekap?bulanTahun=' . urlencode($bulanTahun);
        $x = new Carbon($bulanTahun);
        $v1 = "";
        $v2 = "";
        $v3 = "";
        $period = CarbonPeriod::create(Carbon::parse($x->startOfMonth())->format('Y-m-d'), Carbon::parse($x->endOfMonth())->format('Y-m-d'));
        foreach ($period as $rk => $value) {
            $dateFormated = Carbon::parse($value);
            $v1 .= "case when right(tanggal,2)='" . $dateFormated->format('d') . "' then keterangan end tgl" . $dateFormated->format('d') . ",";
            $v2 .= "max(tgl" . $dateFormated->format('d') . ") tgl" . $dateFormated->format('d') . ",";
            $v3 .= "sum(case when right(tanggal,2)='" . $dateFormated->format('d') . "' then 1 end) tgl" . $dateFormated->format('d') . ",";
        }
        $sql="SELECT
        nm_pegawai,
        nip,
        jabatan,". rtrim($v2, ',')."
    FROM
        (
        SELECT
            pegawai_id,". rtrim($v1, ',')."
    FROM
        jadwal_dinas
    WHERE
        LEFT ( tanggal, 7 ) = '$xx'
        ) a
        JOIN ms_pegawai b ON a.pegawai_id = b.kd_pegawai
    GROUP BY
        nm_pegawai,
        nip,
        jabatan
    ORDER BY
        pegawai_id ASC";
        $sql2=" SELECT ". rtrim($v3, ',')." FROM jadwal_dinas WHERE LEFT ( tanggal, 7 ) = '$xx' and keterangan='PS'";
        $sql3=" SELECT ". rtrim($v3, ',')." FROM jadwal_dinas WHERE LEFT ( tanggal, 7 ) = '$xx' and keterangan='M'";
        $q=$this->db->query($sql)->result();
        $q2=$this->db->query($sql2)->result();
        $q3=$this->db->query($sql3)->result();
        $data = array(
            'x'             => $x,
            'q'             => $q,
            'q2'             => $q2,
            'q3'             => $q3,
            'cetak'             => $cetak,
            'bulanTahun'             => $bulanTahun,
            'title'               => 'Rekap Jadwal_dinas',
            'create'              => 'Jadwal_dinas/create',
            'akses'               => $akses,
            'period'               => $period,
            'jmlHari'               => count($period),
        );
        $this->template->load('layout', 'jadwal_dinas/Jadwal_dinas_rekap', $data);
    }

    public function cetakrekap()
    {
        $bulanTahun     = urldecode($this->input->get('bulanTahun', TRUE));
        $xx=date("Y-m", strtotime($bulanTahun));
        $cetak= base_url() . 'jadwal_dinas/cetakRekap?bulanTahun=' . urlencode($bulanTahun);
        $x = new Carbon($bulanTahun);
        $v1 = "";
        $v2 = "";
        $v3 = "";
        $period = CarbonPeriod::create(Carbon::parse($x->startOfMonth())->format('Y-m-d'), Carbon::parse($x->endOfMonth())->format('Y-m-d'));
        foreach ($period as $rk => $value) {
            $dateFormated = Carbon::parse($value);
            $v1 .= "case when right(tanggal,2)='" . $dateFormated->format('d') . "' then keterangan end tgl" . $dateFormated->format('d') . ",";
            $v2 .= "max(tgl" . $dateFormated->format('d') . ") tgl" . $dateFormated->format('d') . ",";
            $v3 .= "sum(case when right(tanggal,2)='" . $dateFormated->format('d') . "' then 1 end) tgl" . $dateFormated->format('d') . ",";
        }
        $sql="SELECT
        nm_pegawai,
        nip,
        jabatan,". rtrim($v2, ',')."
    FROM
        (
        SELECT
            pegawai_id,". rtrim($v1, ',')."
    FROM
        jadwal_dinas
    WHERE
        LEFT ( tanggal, 7 ) = '$xx'
        ) a
        JOIN ms_pegawai b ON a.pegawai_id = b.kd_pegawai
    GROUP BY
        nm_pegawai,
        nip,
        jabatan
    ORDER BY
        pegawai_id ASC";
        $sql2=" SELECT ". rtrim($v3, ',')." FROM jadwal_dinas WHERE LEFT ( tanggal, 7 ) = '$xx' and keterangan='PS'";
        $sql3=" SELECT ". rtrim($v3, ',')." FROM jadwal_dinas WHERE LEFT ( tanggal, 7 ) = '$xx' and keterangan='M'";
        $q=$this->db->query($sql)->result();
        $q2=$this->db->query($sql2)->result();
        $q3=$this->db->query($sql3)->result();
        $data = array(
            'x'             => $x,
            'q'             => $q,
            'q2'             => $q2,
            'q3'             => $q3,
            'cetak'             => $cetak,
            'bulanTahun'             => $bulanTahun,
            'period'               => $period,
            'jmlHari'               => count($period),
        );
		$html = $this->load->view('jadwal_dinas/cetakRekap', $data, true);
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$mpdf->WriteHTML($html);
		$mpdf->Output('Jadwal Dinas '.$bulanTahun.'.pdf', 'I');
    }
    public function create()
    {
        $this->cekAkses('create');
        $this->db->where_not_in('kd_pegawai', [1, 2]);
        $pegawai_list = $this->Pegawai_model->get_all();

        $data = array(
            'title'   => 'Tambah Data Jadwal_dinas',
            'kembali' => 'Jadwal_dinas',
            'action'  => site_url('jadwal_dinas/create_action'),
            'id' => set_value('id'),
            'pegawai_id' => set_value('pegawai_id'),
            'keterangan' => set_value('keterangan'),
            'tanggal' => set_value('tanggal'),
            'pegawai_list' => $pegawai_list
        );
        $this->template->load('layout', 'jadwal_dinas/Jadwal_dinas_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $tgl=date("Y-m-d", strtotime($this->input->post('tanggal', TRUE)));
            $id=$this->input->post('pegawai_id', TRUE);
            $cek=$this->db->query("select count(id) a from jadwal_dinas where pegawai_id=$id and tanggal='$tgl'")->row();
            if($cek->a>0){
                set_flashdata('warning', 'Pegawai Sudah Ada');
                redirect(site_url('jadwal_dinas'));
            }
            $data = array(
                'pegawai_id' => $this->input->post('pegawai_id', TRUE),
                'keterangan' => $this->input->post('keterangan', TRUE),
            );
            $this->db->set('tanggal', "STR_TO_DATE('" . $this->input->post('tanggal', TRUE) . "','%d-%m-%Y')", false);

            $this->Jadwal_dinas_model->insert($data);
            set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('jadwal_dinas'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id = rapikan($ide);
        $this->db->where_not_in('kd_pegawai', [1, 2]);
        $pegawai_list = $this->Pegawai_model->get_all();

        $row = $this->Jadwal_dinas_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'Edit data Jadwal_dinas',
                'action' => site_url('jadwal_dinas/update_action'),
                'kembali' => 'Jadwal_dinas',
                'id' => set_value('id', $row->id),
                'pegawai_id' => set_value('pegawai_id', $row->pegawai_id),
                'keterangan' => set_value('keterangan', $row->keterangan),
                'tanggal' => set_value('tanggal', date("d-m-Y", strtotime($row->tanggal))),
                'pegawai_list' => $pegawai_list
            );
            $this->template->load('layout', 'jadwal_dinas/Jadwal_dinas_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('jadwal_dinas'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'pegawai_id' => $this->input->post('pegawai_id', TRUE),
                'keterangan' => $this->input->post('keterangan', TRUE),
            );

            $this->db->set('tanggal', "STR_TO_DATE('" . $this->input->post('tanggal', TRUE) . "','%d-%m-%Y')", false);
            $this->Jadwal_dinas_model->update($this->input->post('id', TRUE), $data);
            set_flashdata('success', 'Update Record Success');
            redirect(site_url('jadwal_dinas'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id = rapikan($ide);
        $row = $this->Jadwal_dinas_model->get_by_id($id);

        if ($row) {
            $this->Jadwal_dinas_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('jadwal_dinas'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('jadwal_dinas'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('pegawai_id', 'pegawai id', 'trim|required|numeric');
        $this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}

/* End of file Jadwal_dinas.php */
/* Location: ./application/controllers/Jadwal_dinas.php */
