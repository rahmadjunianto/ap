<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Kelas_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
    }

    private function cekAkses($var=null){
        $url='Kelas';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'kelas?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'kelas?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'kelas';
            $config['first_url'] = base_url() . 'kelas';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Kelas_model->total_rows($q);
        $kelas                      = $this->Kelas_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'kelas_data' => $kelas,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Kelas',
            'create'              => 'Kelas/create',
            'akses'               =>$akses
        );
        $this->template->load('layout','kelas/Kelas_list', $data);
    }


    public function tes()
    {
        $data = array(
            'title'   =>'Tambah Data Kelas',
            'kembali' =>'Kelas',
            'action'  => site_url('kelas/create_action'),
	    'kd_kelas' => set_value('kd_kelas'),
	    'nama_kelas' => set_value('nama_kelas'),
	    'keterangan' => set_value('keterangan'),
	);
        $this->template->load('layout','kelas/tes_form', $data);
    }
    public function create()
    {
        $this->cekAkses('create');

        $data = array(
            'title'   =>'Tambah Data Kelas',
            'kembali' =>'Kelas',
            'action'  => site_url('kelas/create_action'),
	    'kd_kelas' => set_value('kd_kelas'),
	    'nama_kelas' => set_value('nama_kelas'),
	    'keterangan' => set_value('keterangan'),
	);
        $this->template->load('layout','kelas/Kelas_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_kelas' => $this->input->post('nama_kelas',TRUE),
		'keterangan' => $this->input->post('keterangan',TRUE),
	    );

            $this->Kelas_model->insert($data);
            set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('kelas'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Kelas_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'Edit data Kelas',
                'action' => site_url('kelas/update_action'),
                'kembali' =>'Kelas',
		'kd_kelas' => set_value('kd_kelas', $row->kd_kelas),
		'nama_kelas' => set_value('nama_kelas', $row->nama_kelas),
		'keterangan' => set_value('keterangan', $row->keterangan),
	    );
            $this->template->load('layout','kelas/Kelas_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('kelas'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kd_kelas', TRUE));
        } else {
            $data = array(
		'nama_kelas' => $this->input->post('nama_kelas',TRUE),
		'keterangan' => $this->input->post('keterangan',TRUE),
	    );

            $this->Kelas_model->update($this->input->post('kd_kelas', TRUE), $data);
            set_flashdata('success', 'Update Record Success');
            redirect(site_url('kelas'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Kelas_model->get_by_id($id);

        if ($row) {
            $this->Kelas_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('kelas'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('kelas'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama_kelas', 'nama kelas', 'trim|required');
	$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');

	$this->form_validation->set_rules('kd_kelas', 'kd_kelas', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Kelas.php */
/* Location: ./application/controllers/Kelas.php */