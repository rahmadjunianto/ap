<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require 'vendor/autoload.php';

use Carbon\Carbon;

class Meteran extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Meteran_model');
        $this->load->model('Meteran_detail_model');
        $this->load->model('Asisten_manajer_model');
        $this->load->model('Petugas_model');
        $this->load->model('Ms_panel_model');
        $this->load->library('form_validation');
        $this->id_pengguna = get_userdata('app_id_pengguna');
    }

    private function cekAkses($var = null)
    {
        $url = 'Meteran';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()

    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal     = urldecode($this->input->get('tanggal', TRUE));
        $petugas_dinas     = urldecode($this->input->get('petugas_dinas', TRUE));
        $assistant_manager     = urldecode($this->input->get('assistant_manager', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '' || $tanggal<>''|| $petugas_dinas<>''|| $assistant_manager<>'') {
            $config['base_url']  = base_url() . 'meteran?q=' . urlencode($q)."&tanggal=".urlencode($tanggal)."&petugas_dinas=".urlencode($petugas_dinas)."&assistant_manager=".urlencode($assistant_manager);
            $config['first_url'] = base_url() . 'meteran?q=' . urlencode($q)."&tanggal=".urlencode($tanggal)."&petugas_dinas=".urlencode($petugas_dinas)."&assistant_manager=".urlencode($assistant_manager);
        } else {
            $config['base_url']  = base_url() . 'meteran';
            $config['first_url'] = base_url() . 'meteran';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $tanggal<>"" ? $this->db->where('tanggal', date("Y-m-d", strtotime($tanggal))):"";
        $assistant_manager<>"" ? $this->db->where('assistant_manager', $assistant_manager):"";
        $petugas_dinas<>"" ? $this->db->where('petugas_dinas', $petugas_dinas):"";
        $config['total_rows']        = $this->Meteran_model->total_rows($q);
        $tanggal<>"" ? $this->db->where('tanggal', date("Y-m-d", strtotime($tanggal))):"";
        $assistant_manager<>"" ? $this->db->where('assistant_manager', $assistant_manager):"";
        $petugas_dinas<>"" ? $this->db->where('petugas_dinas', $petugas_dinas):"";
        $meteran                      = $this->Meteran_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'meteran_data' => $meteran,
            'q'                   => $q,
            'tanggal'             => $tanggal,
            'petugas_dinas'             => $petugas_dinas,
            'assistant_manager'             => $assistant_manager,
            'petugas_list' => $this->Petugas_model->get_all(),
            'asisten_list' => $this->Asisten_manajer_model->get_all(),
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Metering',
            'create'              => 'Meteran/create',
            'akses'               => $akses
        );
        $this->template->load('layout', 'meteran/Meteran_list', $data);
    }



    public function create()
    {
        // setlocale(LC_TIME, 'id');
        // printf("Now: %s", Carbon::now());
        // die();
        $this->cekAkses('create');
        $this->db->where('jenis_panel', '1');
        $this->db->order_by('id', 'asc');
        $list1 = $this->Ms_panel_model->get_all();
        $this->db->where('jenis_panel', '2');
        $this->db->order_by('id', 'asc');
        $list2 = $this->Ms_panel_model->get_all();
        $data = array(
            'title'   => 'Tambah Data Metering',
            'kembali' => 'Meteran',
            'action'  => site_url('meteran/create_action'),
            'id' => set_value('id'),
            'tanggal' => set_value('tanggal'),
            'pukul' => set_value('pukul'),
            'panel_list1' => $list1,
            'panel_list2' => $list2,
            'petugas_list' => $this->Petugas_model->get_all(),
            'asisten_list' => $this->Asisten_manajer_model->get_all(),
            'assistant_manager' => set_value('assistant_manager'),
            'petugas_dinas' => set_value('petugas_dinas'),
        );
        $this->template->load('layout', 'meteran/Meteran_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $this->db->trans_start();
            $data = array(
                'tanggal' => date('Y-m-d'),
                'pukul' => date('H:i:s'),
                'assistant_manager' => $this->input->post('assistant_manager', TRUE),
                'petugas_dinas' => $this->input->post('petugas_dinas', TRUE),
            );
            $this->Meteran_model->insert($data);
            $meteran_header_id=$this->db->insert_id();
            for ($i = 0; $i < count($this->input->post('panel_id', TRUE)); $i++) {
                $data = array(
                    'meteran_header_id' => $meteran_header_id,
                    'panel_id' => $this->input->post('panel_id', TRUE)[$i],
                    'T_L1' => $this->input->post('T_L1', TRUE)[$i],
                    'T_L2' => $this->input->post('T_L2', TRUE)[$i],
                    'T_L3' => $this->input->post('T_L3', TRUE)[$i],
                    'A_L1' => $this->input->post('A_L1', TRUE)[$i],
                    'A_L2' => $this->input->post('A_L2', TRUE)[$i],
                    'A_L3' => $this->input->post('A_L3', TRUE)[$i],
                    'DAYA_TERIMA_ENERGI' => $this->input->post('DAYA_TERIMA_ENERGI', TRUE)[$i]<> '' ?$this->input->post('DAYA_TERIMA_ENERGI', TRUE)[$i] :null,
                    'DAYA_TERIMA_REAKTIF' => $this->input->post('DAYA_TERIMA_REAKTIF', TRUE)[$i]<> '' ?$this->input->post('DAYA_TERIMA_REAKTIF', TRUE)[$i] : null,
                    'DAYA_KIRIM_ENERGI' => $this->input->post('DAYA_KIRIM_ENERGI', TRUE)[$i]<> '' ?$this->input->post('DAYA_KIRIM_ENERGI', TRUE)[$i] :null,
                    'DAYA_KIRIM_REAKTIF' => $this->input->post('DAYA_KIRIM_REAKTIF', TRUE)[$i]<> '' ? $this->input->post('DAYA_KIRIM_REAKTIF', TRUE)[$i] :null,
                    'DAYA_AKTIF' => $this->input->post('DAYA_AKTIF', TRUE)[$i]<> '' ?$this->input->post('DAYA_AKTIF', TRUE)[$i] :null,
                    'DAYA_SEMU_1' => $this->input->post('DAYA_SEMU_1', TRUE)[$i]<> '' ?$this->input->post('DAYA_SEMU_1', TRUE)[$i] :null,
                    'DAYA_SEMU_2' => $this->input->post('DAYA_SEMU_2', TRUE)[$i]<> '' ?$this->input->post('DAYA_SEMU_2', TRUE)[$i]:null,
                    'DAYA_REAKTIF' => $this->input->post('DAYA_REAKTIF', TRUE)[$i]<> '' ?$this->input->post('DAYA_REAKTIF', TRUE)[$i]:null,
                    'FREKUENSI' => $this->input->post('FREKUENSI', TRUE)[$i],
                    'COS' => $this->input->post('COS', TRUE)[$i],
                );
                $this->Meteran_detail_model->insert($data);
            }
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                set_flashdata('warning', 'Data tidak tersimpan.');
            } else {
                set_flashdata('success', 'Data telah di simpan.');
            }
            redirect(site_url('meteran'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id = rapikan($ide);
        $row = $this->Meteran_model->get_by_id($id);

        if ($row) {
            $this->cekAkses('create');
            $list1 = $this->db->query("SELECT a.*,b.panel,jenis_panel FROM `meteran_detail` a join ms_panel b on a.panel_id=b.id WHERE meteran_header_id=$id and b.jenis_panel=1")->result();
            $list2 = $this->db->query("SELECT a.*,b.panel,jenis_panel FROM `meteran_detail` a join ms_panel b on a.panel_id=b.id WHERE meteran_header_id=$id and b.jenis_panel=2")->result();
            // echo $this->db->last_query();
            // var_dump($list1);
            // echo $row->assistant_manager;
            // die();
            $data = array(
                'title'   => 'Edit Data Metering',
                'kembali' => 'Meteran',
                'action'  => site_url('meteran/update_action'),
                'id' => set_value('id'),
                'tanggal' => set_value('tanggal'),
                'pukul' => set_value('pukul'),
                'assistant_manager' => set_value('assistant_manager',$row->assistant_manager),
                'petugas_dinas' => set_value('petugas_dinas',$row->petugas_dinas),
                'header_id' => set_value('header_id',$row->id),
                'panel_list1' => $list1,
                'panel_list2' => $list2,
                'petugas_list' => $this->Petugas_model->get_all(),
                'asisten_list' => $this->Asisten_manajer_model->get_all(),
                // 'assistant_manager' => set_value('assistant_manager'),
                // 'petugas_dinas' => set_value('petugas_dinas'),
            );
            $this->template->load('layout', 'meteran/Meteran_edit', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('meteran'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {

            $this->db->trans_start();
            $header = array(
                'assistant_manager' => $this->input->post('assistant_manager', TRUE),
                'petugas_dinas' => $this->input->post('petugas_dinas', TRUE),
            );
            // $this->Meteran_model->insert($data);
            $this->Meteran_model->update($this->input->post('header_id', TRUE), $header);
            for ($i = 0; $i < count($this->input->post('id', TRUE)); $i++) {
                $data = array(
                    'T_L1' => $this->input->post('T_L1', TRUE)[$i],
                    'T_L2' => $this->input->post('T_L2', TRUE)[$i],
                    'T_L3' => $this->input->post('T_L3', TRUE)[$i],
                    'A_L1' => $this->input->post('A_L1', TRUE)[$i],
                    'A_L2' => $this->input->post('A_L2', TRUE)[$i],
                    'A_L3' => $this->input->post('A_L3', TRUE)[$i],
                    'DAYA_TERIMA_ENERGI' => $this->input->post('DAYA_TERIMA_ENERGI', TRUE)[$i]<> '' ?$this->input->post('DAYA_TERIMA_ENERGI', TRUE)[$i] :null,
                    'DAYA_TERIMA_REAKTIF' => $this->input->post('DAYA_TERIMA_REAKTIF', TRUE)[$i]<> '' ?$this->input->post('DAYA_TERIMA_REAKTIF', TRUE)[$i] : null,
                    'DAYA_KIRIM_ENERGI' => $this->input->post('DAYA_KIRIM_ENERGI', TRUE)[$i]<> '' ?$this->input->post('DAYA_KIRIM_ENERGI', TRUE)[$i] :null,
                    'DAYA_KIRIM_REAKTIF' => $this->input->post('DAYA_KIRIM_REAKTIF', TRUE)[$i]<> '' ? $this->input->post('DAYA_KIRIM_REAKTIF', TRUE)[$i] :null,
                    'DAYA_AKTIF' => $this->input->post('DAYA_AKTIF', TRUE)[$i]<> '' ?$this->input->post('DAYA_AKTIF', TRUE)[$i] :null,
                    'DAYA_SEMU_1' => $this->input->post('DAYA_SEMU_1', TRUE)[$i]<> '' ?$this->input->post('DAYA_SEMU_1', TRUE)[$i] :null,
                    'DAYA_SEMU_2' => $this->input->post('DAYA_SEMU_2', TRUE)[$i]<> '' ?$this->input->post('DAYA_SEMU_2', TRUE)[$i]:null,
                    'DAYA_REAKTIF' => $this->input->post('DAYA_REAKTIF', TRUE)[$i]<> '' ?$this->input->post('DAYA_REAKTIF', TRUE)[$i]:null,
                    'FREKUENSI' => $this->input->post('FREKUENSI', TRUE)[$i],
                    'COS' => $this->input->post('COS', TRUE)[$i],
                );
                $this->Meteran_detail_model->update($this->input->post('id', TRUE)[$i], $data);
                // var_dump($data);
                // echo $this->db->last_query();
                // die();
            }
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                set_flashdata('warning', 'Update Gagal');
            } else {
                set_flashdata('success', 'Data telah di Update.');
            }
            redirect(site_url('meteran'));
        }
    }
    public function cetak($ide)
    {
        $id = rapikan($ide);

        $data['header'] = $this->Meteran_model->get_by_id($id);
        $data['panel_list1'] = $this->db->query("SELECT a.*,b.panel,jenis_panel FROM `meteran_detail` a join ms_panel b on a.panel_id=b.id WHERE meteran_header_id=$id and b.jenis_panel=1")->result();
        $data['panel_list2'] = $this->db->query("SELECT a.*,b.panel,jenis_panel FROM `meteran_detail` a join ms_panel b on a.panel_id=b.id WHERE meteran_header_id=$id and b.jenis_panel=2")->result();
		$html = $this->load->view('meteran/cetak', $data, true);
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
		$mpdf->WriteHTML($html);
		$mpdf->Output('yourFileName.pdf', 'I');
    }
    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id = rapikan($ide);
        $row = $this->Meteran_model->get_by_id($id);

        if ($row) {
            $this->Meteran_model->delete($id);
            $this->db->query("DELETE FROM meteran_detail where meteran_header_id =".$id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('meteran'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('meteran'));
        }
    }

    public function _rules()
    {
        // $this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');
        // $this->form_validation->set_rules('pukul', 'pukul', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}

/* End of file Meteran.php */
/* Location: ./application/controllers/Meteran.php */
