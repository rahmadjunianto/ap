<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pengguna_model');
        $this->load->model('Pegawai_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
    }

    private function cekAkses($var=null){
        $url='Pengguna';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'pengguna?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pengguna?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'pengguna';
            $config['first_url'] = base_url() . 'pengguna';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Pengguna_model->total_rows($q);
        $pengguna                      = $this->Pengguna_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pengguna_data' => $pengguna,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Pengguna',
            'create'              => 'Pengguna/create',
            'akses'               =>$akses,
            'script'               => 'pengguna/index_js'
        );
        $this->template->load('layout','pengguna/Pengguna_list', $data);
    }


    public function reset()
    {
        $id=$this->input->post('id', TRUE);
        $id=rapikan($id);
        $data = array(
            'password'        => sha1($this->input->post('password',TRUE)),
        );

        if ($this->Pengguna_model->update($id, $data))
        {
            set_flashdata('warning', 'Reset Password Gagal.');
        }else{
            set_flashdata('success', 'Reset Password Berhasil.');
        }
        // echo $id;
        // echo $this->db->last_query();
        redirect(site_url('pengguna'));
    }
    public function create()
    {
        $this->cekAkses('create');
        $this->load->model('Msistem');
        $role =$this->Msistem->getRole()->result();

        $data = array(
            'title'        => 'Tambah Data Pengguna',
            'kembali'      => 'Pengguna',
            'action'       => site_url('pengguna/create_action'),
            'id_inc'       => set_value('id_inc'),
            'nama_lengkap' => set_value('nama_lengkap'),
            'no_telepon'   => set_value('no_telepon'),
            'email'        => set_value('email'),
            'username'     => set_value('username'),
            'password'     => set_value('password'),
            'nip'     => set_value('nip'),
            'role'         => $role,
            'nambah'       => true,
            'assign'       => [],
            'pegawai'      =>$this->Pegawai_model->get_all(),
            'script'       =>'pengguna/js_pengguna'

	);
        $this->template->load('layout','pengguna/Pengguna_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE && !empty($this->input->post('ms_role_id',true))) {
            $this->create();
        } else {
            $data = array(
                'id_petugas' => $this->input->post('id_petugas',TRUE),
                'nama_lengkap' => $this->input->post('nama_lengkap',TRUE),
                'no_telepon'   => $this->input->post('no_telepon',TRUE),
                'email'        => $this->input->post('email',TRUE),
                'username'     => $this->input->post('username',TRUE),
                'password'     => sha1($this->input->post('password',TRUE)),
                'time_insert'  => date('Y-m-d H:i:s'),
                'pengguna_insert'=>get_userdata('app_nama'),
    	    );

            $this->db->trans_start();
                $this->Pengguna_model->insert($data);
                // get id pengguna
                $this->db->where('username',$this->input->post('username',TRUE));
                $this->db->where('password',sha1($this->input->post('password',TRUE)));
                $this->db->select('id_inc');
                $rr=$this->db->get('ms_pengguna')->row();
                $ms_pengguna_id=$rr->id_inc;

                $role=$this->input->post('ms_role_id',true);
                for($i=0; $i<count($role);$i++){
                    $dd=array(
                        'ms_pengguna_id'=>$ms_pengguna_id,
                        'ms_role_id'=>$role[$i]
                    );
                    $this->db->insert('ms_assign_role',$dd);
                }

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                set_flashdata('warning', 'Data tidak tersimpan.');
            }else{
                set_flashdata('success', 'Data telah di simpan.');
            }

            redirect(site_url('pengguna'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Pengguna_model->get_by_id($id);

        if ($row) {
            $this->load->model('Msistem');
            $role   =$this->Msistem->getRole()->result();
            $assign =$this->db->query("SELECT GROUP_CONCAT(ms_role_id) role FROM ms_assign_role  WHERE ms_pengguna_id=$id")->row();
            $dd=explode(',',$assign->role);
            $data = array(
                'title'        => 'Edit data Pengguna',
                'action'       => site_url('pengguna/update_action'),
                'kembali'      =>'Pengguna',
                'id_inc'       => set_value('id_inc', $row->id_inc),
                'nama_lengkap' => set_value('nama_lengkap', $row->nama_lengkap),
                'no_telepon'   => set_value('no_telepon', $row->no_telepon),
                'email'        => set_value('email', $row->email),
                'nip'          => set_value('id_petugas', $row->nip),
                'role'         => $role,
                'nambah'       => false,
                'assign'       => $dd,
                'pegawai'      =>$this->Pegawai_model->get_all(),
                'script'       =>'pengguna/js_pengguna'
	    );
            $this->template->load('layout','pengguna/Pengguna_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('pengguna'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rulesupdate();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $this->db->trans_start();
            // update data pengguna
            $data = array(
                'nip'    => $this->input->post('nip',TRUE),
                'nama_lengkap'    => $this->input->post('nama_lengkap',TRUE),
                'no_telepon'      => $this->input->post('no_telepon',TRUE),
                'email'           => $this->input->post('email',TRUE),
                'time_update'     => date('Y-m-d H:i:s'),
                'pengguna_update' =>get_userdata('app_nama'),
        	    );
            $this->Pengguna_model->update($this->input->post('id_inc', TRUE), $data);

            // assign role
            $this->db->where('ms_pengguna_id',$this->input->post('id_inc', TRUE));
            $this->db->delete('ms_assign_role');

            $role=$this->input->post('ms_role_id',true);
                for($i=0; $i<count($role);$i++){
                    $dd=array(
                        'ms_pengguna_id'=>$this->input->post('id_inc', TRUE),
                        'ms_role_id'=>$role[$i]
                    );
                    $this->db->insert('ms_assign_role',$dd);
                }


            $this->db->trans_complete();

            // set_flashdata('success', 'Update Record Success');

            if ($this->db->trans_status() === FALSE)
            {
                set_flashdata('warning', 'Data tidak tersimpan.');
            }else{
                set_flashdata('success', 'Data telah di simpan.');
            }

            redirect(site_url('pengguna'));
        }
    }

    public function changefoto()
    {

        $data = array(
            'title'     => 'Ganti Foto',
            'action'    => site_url('pengguna/changeFoto_action'),
            'id_inc'    => set_value('id_inc', $this->id_pengguna),
            'foto'      => set_value('foto'),
            'script'    => 'pengguna/js_foto'
        );
        $this->template->load('layout','pengguna/changeFoto_form',$data);
    }

    public function changefoto_action()
    {
        $x=$this->Pengguna_model->get_by_id($this->input->post('id_inc',TRUE));
        unlink('./assets/foto/'.$x->foto);

        $config['upload_path']   = './assets/foto/';
        $config['allowed_types'] = 'jpeg|jpg|png|JPG|PNG|JPEG';
        $config['encrypt_name']  =TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!empty($_FILES['foto']['name'])) {
            if ( ! $this->upload->do_upload('foto'))
            {
                $eror=$this->upload->display_errors();
            set_flashdata('warning', $eror);
            redirect(site_url('pengguna/changefoto'));
            }
            else {



                $dataq = array('upload_data' => $this->upload->data());
                $nama = $dataq['upload_data']['raw_name'].$dataq['upload_data']['file_ext'];
                $data=$this->upload->data();
                $this->db->set('foto',$nama);
                $this->db->where('id_inc',$this->input->post('id_inc',TRUE));
                $this->db->update('ms_pengguna');

            set_flashdata('success', 'Foto Berhasil diganti.');
            redirect(site_url('pengguna/changefoto'));
            }
        } else {

            set_flashdata('warning', 'Foto  Gagal diganti.');
            redirect(site_url('pengguna/changefoto'));

        }
    }
    public function changePassword()
    {

        $data = array(
            'title'     => 'Ganti Password',
            'action'    => site_url('pengguna/changePassword_action'),
            'id_inc'    => set_value('id_inc', $this->id_pengguna),
            'lama'      => set_value('lama'),
            'baru'      => set_value('baru'),
        );
        $this->template->load('layout','pengguna/changePassword_form',$data);
    }
    public function changePassword_action()
    {
        $lama= sha1($this->input->post('lama', TRUE));
        $id=$this->input->post('id_inc', TRUE);
        $row = $this->Pengguna_model->get_by_id($id);
        if($row->password==$lama){
            $data = array(
                'password'    => sha1($this->input->post('baru',TRUE)),
        	    );
            $this->Pengguna_model->update($id, $data);
            set_flashdata('success', 'Password Berhasil diganti.');
            redirect(site_url('pengguna/changePassword'));
        } else {

            set_flashdata('warning', 'Password Lama Salah');
            $this->changePassword();
        }
    }
    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Pengguna_model->get_by_id($id);

        if ($row) {
            $this->Pengguna_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('pengguna'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengguna'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama_lengkap', 'nama lengkap', 'trim|required');
	$this->form_validation->set_rules('no_telepon', 'no telepon', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function _rulesupdate(){
        $this->form_validation->set_rules('nama_lengkap', 'nama lengkap', 'trim|required');
        $this->form_validation->set_rules('no_telepon', 'no telepon', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Pengguna.php */
/* Location: ./application/controllers/Pengguna.php */