<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Teknisi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Teknisi_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
    }

    private function cekAkses($var=null){
        $url='Teknisi';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'teknisi?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'teknisi?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'teknisi';
            $config['first_url'] = base_url() . 'teknisi';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Teknisi_model->total_rows($q);
        $teknisi                      = $this->Teknisi_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'teknisi_data' => $teknisi,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Teknisi',
            'create'              => 'Teknisi/create',
            'akses'               =>$akses
        );
        $this->template->load('layout','teknisi/Teknisi_list', $data);
    }



    public function create()
    {
        $this->cekAkses('create');

        $data = array(
            'title'   =>'Tambah Data Teknisi',
            'kembali' =>'Teknisi',
            'action'  => site_url('teknisi/create_action'),
	    'id' => set_value('id'),
	    'nama' => set_value('nama'),
	);
        $this->template->load('layout','teknisi/Teknisi_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
	    );

            $this->Teknisi_model->insert($data);
            set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('teknisi'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Teknisi_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'Edit data Teknisi',
                'action' => site_url('teknisi/update_action'),
                'kembali' =>'Teknisi',
		'id' => set_value('id', $row->id),
		'nama' => set_value('nama', $row->nama),
	    );
            $this->template->load('layout','teknisi/Teknisi_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('teknisi'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
	    );

            $this->Teknisi_model->update($this->input->post('id', TRUE), $data);
            set_flashdata('success', 'Update Record Success');
            redirect(site_url('teknisi'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Teknisi_model->get_by_id($id);

        if ($row) {
            $this->Teknisi_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('teknisi'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('teknisi'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Teknisi.php */
/* Location: ./application/controllers/Teknisi.php */