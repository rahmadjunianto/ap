<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Check_list_gis extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Check_list_gis_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
        $this->load->model('Asisten_manajer_model');
        $this->load->model('Petugas_model');
    }

    private function cekAkses($var=null){
        $url='Check_list_gis';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal     = urldecode($this->input->get('tanggal', TRUE));
        $petugas_dinas     = urldecode($this->input->get('petugas_dinas', TRUE));
        $assistant_manager     = urldecode($this->input->get('assistant_manager', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '' || $tanggal<>''|| $petugas_dinas<>''|| $assistant_manager<>'') {
            $config['base_url']  = base_url() . 'check_list_gis?q=' . urlencode($q)."&tanggal=".urlencode($tanggal)."&petugas_dinas=".urlencode($petugas_dinas)."&assistant_manager=".urlencode($assistant_manager);
            $config['first_url'] = base_url() . 'check_list_gis?q=' . urlencode($q)."&tanggal=".urlencode($tanggal)."&petugas_dinas=".urlencode($petugas_dinas)."&assistant_manager=".urlencode($assistant_manager);
        } else {
            $config['base_url']  = base_url() . 'check_list_gis';
            $config['first_url'] = base_url() . 'check_list_gis';
        }
        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $tanggal<>"" ? $this->db->where('tanggal', date("Y-m-d", strtotime($tanggal))):"";
        $assistant_manager<>"" ? $this->db->where('assistant_manager', $assistant_manager):"";
        $petugas_dinas<>"" ? $this->db->where('petugas_dinas', $petugas_dinas):"";
        $config['total_rows']        = $this->Check_list_gis_model->total_rows($q);
        $tanggal<>"" ? $this->db->where('tanggal', date("Y-m-d", strtotime($tanggal))):"";
        $assistant_manager<>"" ? $this->db->where('assistant_manager', $assistant_manager):"";
        $petugas_dinas<>"" ? $this->db->where('petugas_dinas', $petugas_dinas):"";
        $check_list_gis                      = $this->Check_list_gis_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'check_list_gis_data' => $check_list_gis,
            'q'                   => $q,
            'tanggal'             => $tanggal,
            'petugas_dinas'             => $petugas_dinas,
            'assistant_manager'             => $assistant_manager,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Check_list_gis',
            'create'              => 'Check_list_gis/create',
            'petugas_list' => $this->Petugas_model->get_all(),
            'asisten_list' => $this->Asisten_manajer_model->get_all(),
            'akses'               =>$akses
        );
        $this->template->load('layout','check_list_gis/Check_list_gis_list', $data);
    }

    public function cetak($ide)
    {
        $id = rapikan($ide);

        $data['header'] = $this->Check_list_gis_model->get_by_id($id);
		$html = $this->load->view('check_list_gis/cetak', $data, true);
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'P']);
		$mpdf->WriteHTML($html);
		$mpdf->Output('Check_list_gis_model.pdf', 'I');
    }

    public function create()
    {
        $this->cekAkses('create');

        $data = array(
            'title'   =>'Tambah Data Check_list_gis',
            'kembali' =>'Check_list_gis',
            'action'  => site_url('check_list_gis/create_action'),
            'petugas_list' => $this->Petugas_model->get_all(),
            'asisten_list' => $this->Asisten_manajer_model->get_all(),
	    'id' => set_value('id'),
	    'tanggal' => set_value('tanggal'),
	    'pukul' => set_value('pukul'),
	    'assistant_manager' => set_value('assistant_manager'),
	    'petugas_dinas' => set_value('petugas_dinas'),
	    'pms_line_pht1' => set_value('pms_line_pht1'),
	    'pms_line_pht2' => set_value('pms_line_pht2'),
	    'pht1_q1_pms_bus' => set_value('pht1_q1_pms_bus'),
	    'pht1_q2_pms_bus' => set_value('pht1_q2_pms_bus'),
	    'pht2_q1_pms_bus' => set_value('pht2_q1_pms_bus'),
	    'pht2_q2_pms_bus' => set_value('pht2_q2_pms_bus'),
	    'out1_q1_pms_bus' => set_value('out1_q1_pms_bus'),
	    'out1_q2_pms_bus' => set_value('out1_q2_pms_bus'),
	    'out2_q1_pms_bus' => set_value('out2_q1_pms_bus'),
	    'out2_q2_pms_bus' => set_value('out2_q2_pms_bus'),
	    'out3_q1_pms_bus' => set_value('out3_q1_pms_bus'),
	    'out3_q2_pms_bus' => set_value('out3_q2_pms_bus'),
	    'coupler_q1_pms_bus' => set_value('coupler_q1_pms_bus'),
	    'coupler_q2_pms_bus' => set_value('coupler_q2_pms_bus'),
	    'bus_vt_q1_pms_bus' => set_value('bus_vt_q1_pms_bus'),
	    'bus_vt_q2_pms_bus' => set_value('bus_vt_q2_pms_bus'),
	    'pht1_q1_pmt' => set_value('pht1_q1_pmt'),
	    'pht1_q2_pmt' => set_value('pht1_q2_pmt'),
	    'pht2_q1_pmt' => set_value('pht2_q1_pmt'),
	    'pht2_q2_pmt' => set_value('pht2_q2_pmt'),
	    'out1_q1_pmt' => set_value('out1_q1_pmt'),
	    'out1_q2_pmt' => set_value('out1_q2_pmt'),
	    'out2_q1_pmt' => set_value('out2_q1_pmt'),
	    'out2_q2_pmt' => set_value('out2_q2_pmt'),
	    'out3_q1_pmt' => set_value('out3_q1_pmt'),
	    'out3_q2_pmt' => set_value('out3_q2_pmt'),
	    'coupler_q1_pmt' => set_value('coupler_q1_pmt'),
	    'coupler_q2_pmt' => set_value('coupler_q2_pmt'),
	    'pht1_q1_ceilingend' => set_value('pht1_q1_ceilingend'),
	    'pht1_q2_ceilingend' => set_value('pht1_q2_ceilingend'),
	    'pht2_q1_ceilingend' => set_value('pht2_q1_ceilingend'),
	    'pht2_q2_ceilingend' => set_value('pht2_q2_ceilingend'),
	    'out1_q1_ceilingend' => set_value('out1_q1_ceilingend'),
	    'out1_q2_ceilingend' => set_value('out1_q2_ceilingend'),
	    'out2_q1_ceilingend' => set_value('out2_q1_ceilingend'),
	    'out2_q2_ceilingend' => set_value('out2_q2_ceilingend'),
	    'out3_q1_ceilingend' => set_value('out3_q1_ceilingend'),
	    'out3_q2_ceilingend' => set_value('out3_q2_ceilingend'),
	    'pht1_q1_vt_line' => set_value('pht1_q1_vt_line'),
	    'pht1_q2_vt_line' => set_value('pht1_q2_vt_line'),
	    'pht2_q1_vt_line' => set_value('pht2_q1_vt_line'),
	    'pht2_q2_vt_line' => set_value('pht2_q2_vt_line'),
	    'pht1_q1_pms_bus_posisi' => set_value('pht1_q1_pms_bus_posisi'),
	    'pht1_q2_pms_bus_posisi' => set_value('pht1_q2_pms_bus_posisi'),
	    'pht2_q1_pms_bus_posisi' => set_value('pht2_q1_pms_bus_posisi'),
	    'pht2_q2_pms_bus_posisi' => set_value('pht2_q2_pms_bus_posisi'),
	    'out1_q1_pms_bus_posisi' => set_value('out1_q1_pms_bus_posisi'),
	    'out1_q2_pms_bus_posisi' => set_value('out1_q2_pms_bus_posisi'),
	    'out2_q1_pms_bus_posisi' => set_value('out2_q1_pms_bus_posisi'),
	    'out2_q2_pms_bus_posisi' => set_value('out2_q2_pms_bus_posisi'),
	    'out3_q1_pms_bus_posisi' => set_value('out3_q1_pms_bus_posisi'),
	    'out3_q2_pms_bus_posisi' => set_value('out3_q2_pms_bus_posisi'),
	    'coupler_q1_pms_bus_posisi' => set_value('coupler_q1_pms_bus_posisi'),
	    'coupler_q2_pms_bus_posisi' => set_value('coupler_q2_pms_bus_posisi'),
	    'bus_vt_q1_pms_bus_posisi' => set_value('bus_vt_q1_pms_bus_posisi'),
	    'bus_vt_q2_pms_bus_posisi' => set_value('bus_vt_q2_pms_bus_posisi'),
	    'pht1_q1_pmt_posisi' => set_value('pht1_q1_pmt_posisi'),
	    'pht2_q1_pmt_posisi' => set_value('pht2_q1_pmt_posisi'),
	    'pht2_q2_pmt_posisi' => set_value('pht2_q2_pmt_posisi'),
	    'out1_q1_pmt_posisi' => set_value('out1_q1_pmt_posisi'),
	    'out1_q2_pmt_posisi' => set_value('out1_q2_pmt_posisi'),
	    'out2_q1_pmt_posisi' => set_value('out2_q1_pmt_posisi'),
	    'out2_q2_pmt_posisi' => set_value('out2_q2_pmt_posisi'),
	    'out3_q1_pmt_posisi' => set_value('out3_q1_pmt_posisi'),
	    'out3_q2_pmt_posisi' => set_value('out3_q2_pmt_posisi'),
	    'coupler_q1_pmt_posisi' => set_value('coupler_q1_pmt_posisi'),
	    'coupler_q2_pmt_posisi' => set_value('coupler_q2_pmt_posisi'),
	    'bus_vt_q1_pmt_posisi' => set_value('bus_vt_q1_pmt_posisi'),
	    'bus_vt_q2_pmt_posisi' => set_value('bus_vt_q2_pmt_posisi'),
	    'pht1_counter_pmt_r' => set_value('pht1_counter_pmt_r'),
	    'pht2_counter_pmt_r' => set_value('pht2_counter_pmt_r'),
	    'out1_counter_pmt_r' => set_value('out1_counter_pmt_r'),
	    'out2_counter_pmt_r' => set_value('out2_counter_pmt_r'),
	    'out3_counter_pmt_r' => set_value('out3_counter_pmt_r'),
	    'coupler_counter_pmt_r' => set_value('coupler_counter_pmt_r'),
	    'bus_vt_counter_pmt_r' => set_value('bus_vt_counter_pmt_r'),
	    'pht1_counter_pmt_s' => set_value('pht1_counter_pmt_s'),
	    'pht2_counter_pmt_s' => set_value('pht2_counter_pmt_s'),
	    'out1_counter_pmt_s' => set_value('out1_counter_pmt_s'),
	    'out2_counter_pmt_s' => set_value('out2_counter_pmt_s'),
	    'out3_counter_pmt_s' => set_value('out3_counter_pmt_s'),
	    'coupler_counter_pmt_s' => set_value('coupler_counter_pmt_s'),
	    'bus_vt_counter_pmt_s' => set_value('bus_vt_counter_pmt_s'),
	    'pht1_counter_pmt_t' => set_value('pht1_counter_pmt_t'),
	    'pht2_counter_pmt_t' => set_value('pht2_counter_pmt_t'),
	    'out1_counter_pmt_t' => set_value('out1_counter_pmt_t'),
	    'out2_counter_pmt_t' => set_value('out2_counter_pmt_t'),
	    'out3_counter_pmt_t' => set_value('out3_counter_pmt_t'),
	    'coupler_counter_pmt_t' => set_value('coupler_counter_pmt_t'),
	    'bus_vt_counter_pmt_t' => set_value('bus_vt_counter_pmt_t'),
	    'q15' => set_value('q15'),
	    'q25' => set_value('q25'),
	    'pht1_earthing_pmt' => set_value('pht1_earthing_pmt'),
	    'pht2_earthing_pmt' => set_value('pht2_earthing_pmt'),
	    'out1_earthing_pmt' => set_value('out1_earthing_pmt'),
	    'out2_earthing_pmt' => set_value('out2_earthing_pmt'),
	    'out3_earthing_pmt' => set_value('out3_earthing_pmt'),
	    'coupler_earthing_pmt' => set_value('coupler_earthing_pmt'),
	    'pht1_earthing_pmt_q2' => set_value('pht1_earthing_pmt_q2'),
	    'pht2_earthing_pmt_q2' => set_value('pht2_earthing_pmt_q2'),
	    'out1_earthing_pmt_q2' => set_value('out1_earthing_pmt_q2'),
	    'out2_earthing_pmt_q2' => set_value('out2_earthing_pmt_q2'),
	    'out3_earthing_pmt_q2' => set_value('out3_earthing_pmt_q2'),
	    'coupler_earthing_pmt_q2' => set_value('coupler_earthing_pmt_q2'),
	    'pht1_earthing_line' => set_value('pht1_earthing_line'),
	    'pht2_earthing_line' => set_value('pht2_earthing_line'),
	    'dh110_r1' => set_value('dh110_r1'),
	    'dh110_r2' => set_value('dh110_r2'),
	    'dh48_r1' => set_value('dh48_r1'),
	    'dh48_r2' => set_value('dh48_r2'),
	    'dh110_lr' => set_value('dh110_lr'),
	    'dh48_lr' => set_value('dh48_lr'),
	    'trafo1_level' => set_value('trafo1_level'),
	    'oltc1_level' => set_value('oltc1_level'),
	    'trafo2_level' => set_value('trafo2_level'),
	    'oltc2_level' => set_value('oltc2_level'),
	    'trafo_ps_level' => set_value('trafo_ps_level'),
	    'oltc1_suhu' => set_value('oltc1_suhu'),
	    'oltc2_suhu' => set_value('oltc2_suhu'),
	    'trafo_ps_suhu' => set_value('trafo_ps_suhu'),
	    'trafo1_oil' => set_value('trafo1_oil'),
	    'trafo2_oil' => set_value('trafo2_oil'),
	    'trafo1_hv' => set_value('trafo1_hv'),
	    'trafo2_hv' => set_value('trafo2_hv'),
	    'trafo1_lv' => set_value('trafo1_lv'),
	    'trafo2_lv' => set_value('trafo2_lv'),
	    'trafo1_bucholz' => set_value('trafo1_bucholz'),
	    'trafo1_jansen' => set_value('trafo1_jansen'),
	    'trafo1_termal' => set_value('trafo1_termal'),
	    'trafo1_sudden_press' => set_value('trafo1_sudden_press'),
	    'trafo1_fire' => set_value('trafo1_fire'),
	    'trafo1_ngr' => set_value('trafo1_ngr'),
	    'trafo1_dc' => set_value('trafo1_dc'),
	    'trafo2_bucholz' => set_value('trafo2_bucholz'),
	    'trafo2_jansen' => set_value('trafo2_jansen'),
	    'trafo2_termal' => set_value('trafo2_termal'),
	    'trafo2_sudden_press' => set_value('trafo2_sudden_press'),
	    'trafo2_fire' => set_value('trafo2_fire'),
	    'trafo2_ngr' => set_value('trafo2_ngr'),
	    'trafo2_dc' => set_value('trafo2_dc'),
	    'keterangan_1' => set_value('keterangan_1'),
	    'keterangan_2' => set_value('keterangan_2'),
	    'kondisi_peralatan' => set_value('kondisi_peralatan'),
	    'trafo1_posisi_tap' => set_value('trafo1_posisi_tap'),
	    'trafo2_posisi_tap' => set_value('trafo2_posisi_tap'),
	    'trafo1_counter_oltc' => set_value('trafo1_counter_oltc'),
	    'trafo2_counter_oltc' => set_value('trafo2_counter_oltc'),
	    'suhu_indoor' => set_value('suhu_indoor'),
	    'suhu_outdoor' => set_value('suhu_outdoor'),
	    'kelembaban_indoor' => set_value('kelembaban_indoor'),
	    'kelembaban_outdoor' => set_value('kelembaban_outdoor'),
	    'benda_asing_indoor' => set_value('benda_asing_indoor'),
	    'benda_asing_outdoor' => set_value('benda_asing_outdoor'),
	);
        $this->template->load('layout','check_list_gis/Check_list_gis_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'tanggal' => date('Y-m-d'),
                'pukul' => date('H:i:s'),
		'assistant_manager' => $this->input->post('assistant_manager',TRUE),
		'petugas_dinas' => $this->input->post('petugas_dinas',TRUE),
		'pms_line_pht1' => $this->input->post('pms_line_pht1',TRUE),
		'pms_line_pht2' => $this->input->post('pms_line_pht2',TRUE),
		'pht1_q1_pms_bus' => $this->input->post('pht1_q1_pms_bus',TRUE),
		'pht1_q2_pms_bus' => $this->input->post('pht1_q2_pms_bus',TRUE),
		'pht2_q1_pms_bus' => $this->input->post('pht2_q1_pms_bus',TRUE),
		'pht2_q2_pms_bus' => $this->input->post('pht2_q2_pms_bus',TRUE),
		'out1_q1_pms_bus' => $this->input->post('out1_q1_pms_bus',TRUE),
		'out1_q2_pms_bus' => $this->input->post('out1_q2_pms_bus',TRUE),
		'out2_q1_pms_bus' => $this->input->post('out2_q1_pms_bus',TRUE),
		'out2_q2_pms_bus' => $this->input->post('out2_q2_pms_bus',TRUE),
		'out3_q1_pms_bus' => $this->input->post('out3_q1_pms_bus',TRUE),
		'out3_q2_pms_bus' => $this->input->post('out3_q2_pms_bus',TRUE),
		'coupler_q1_pms_bus' => $this->input->post('coupler_q1_pms_bus',TRUE),
		'coupler_q2_pms_bus' => $this->input->post('coupler_q2_pms_bus',TRUE),
		'bus_vt_q1_pms_bus' => $this->input->post('bus_vt_q1_pms_bus',TRUE),
		'bus_vt_q2_pms_bus' => $this->input->post('bus_vt_q2_pms_bus',TRUE),
		'pht1_q1_pmt' => $this->input->post('pht1_q1_pmt',TRUE),
		'pht1_q2_pmt' => $this->input->post('pht1_q2_pmt',TRUE),
		'pht2_q1_pmt' => $this->input->post('pht2_q1_pmt',TRUE),
		'pht2_q2_pmt' => $this->input->post('pht2_q2_pmt',TRUE),
		'out1_q1_pmt' => $this->input->post('out1_q1_pmt',TRUE),
		'out1_q2_pmt' => $this->input->post('out1_q2_pmt',TRUE),
		'out2_q1_pmt' => $this->input->post('out2_q1_pmt',TRUE),
		'out2_q2_pmt' => $this->input->post('out2_q2_pmt',TRUE),
		'out3_q1_pmt' => $this->input->post('out3_q1_pmt',TRUE),
		'out3_q2_pmt' => $this->input->post('out3_q2_pmt',TRUE),
		'coupler_q1_pmt' => $this->input->post('coupler_q1_pmt',TRUE),
		'coupler_q2_pmt' => $this->input->post('coupler_q2_pmt',TRUE),
		'pht1_q1_ceilingend' => $this->input->post('pht1_q1_ceilingend',TRUE),
		'pht1_q2_ceilingend' => $this->input->post('pht1_q2_ceilingend',TRUE),
		'pht2_q1_ceilingend' => $this->input->post('pht2_q1_ceilingend',TRUE),
		'pht2_q2_ceilingend' => $this->input->post('pht2_q2_ceilingend',TRUE),
		'out1_q1_ceilingend' => $this->input->post('out1_q1_ceilingend',TRUE),
		'out1_q2_ceilingend' => $this->input->post('out1_q2_ceilingend',TRUE),
		'out2_q1_ceilingend' => $this->input->post('out2_q1_ceilingend',TRUE),
		'out2_q2_ceilingend' => $this->input->post('out2_q2_ceilingend',TRUE),
		'out3_q1_ceilingend' => $this->input->post('out3_q1_ceilingend',TRUE),
		'out3_q2_ceilingend' => $this->input->post('out3_q2_ceilingend',TRUE),
		'pht1_q1_vt_line' => $this->input->post('pht1_q1_vt_line',TRUE),
		'pht1_q2_vt_line' => $this->input->post('pht1_q2_vt_line',TRUE),
		'pht2_q1_vt_line' => $this->input->post('pht2_q1_vt_line',TRUE),
		'pht2_q2_vt_line' => $this->input->post('pht2_q2_vt_line',TRUE),
		'pht1_q1_pms_bus_posisi' => $this->input->post('pht1_q1_pms_bus_posisi',TRUE),
		'pht1_q2_pms_bus_posisi' => $this->input->post('pht1_q2_pms_bus_posisi',TRUE),
		'pht2_q1_pms_bus_posisi' => $this->input->post('pht2_q1_pms_bus_posisi',TRUE),
		'pht2_q2_pms_bus_posisi' => $this->input->post('pht2_q2_pms_bus_posisi',TRUE),
		'out1_q1_pms_bus_posisi' => $this->input->post('out1_q1_pms_bus_posisi',TRUE),
		'out1_q2_pms_bus_posisi' => $this->input->post('out1_q2_pms_bus_posisi',TRUE),
		'out2_q1_pms_bus_posisi' => $this->input->post('out2_q1_pms_bus_posisi',TRUE),
		'out2_q2_pms_bus_posisi' => $this->input->post('out2_q2_pms_bus_posisi',TRUE),
		'out3_q1_pms_bus_posisi' => $this->input->post('out3_q1_pms_bus_posisi',TRUE),
		'out3_q2_pms_bus_posisi' => $this->input->post('out3_q2_pms_bus_posisi',TRUE),
		'coupler_q1_pms_bus_posisi' => $this->input->post('coupler_q1_pms_bus_posisi',TRUE),
		'coupler_q2_pms_bus_posisi' => $this->input->post('coupler_q2_pms_bus_posisi',TRUE),
		'bus_vt_q1_pms_bus_posisi' => $this->input->post('bus_vt_q1_pms_bus_posisi',TRUE),
		'bus_vt_q2_pms_bus_posisi' => $this->input->post('bus_vt_q2_pms_bus_posisi',TRUE),
		'pht1_q1_pmt_posisi' => $this->input->post('pht1_q1_pmt_posisi',TRUE),
		'pht2_q1_pmt_posisi' => $this->input->post('pht2_q1_pmt_posisi',TRUE),
		'pht2_q2_pmt_posisi' => $this->input->post('pht2_q2_pmt_posisi',TRUE),
		'out1_q1_pmt_posisi' => $this->input->post('out1_q1_pmt_posisi',TRUE),
		'out1_q2_pmt_posisi' => $this->input->post('out1_q2_pmt_posisi',TRUE),
		'out2_q1_pmt_posisi' => $this->input->post('out2_q1_pmt_posisi',TRUE),
		'out2_q2_pmt_posisi' => $this->input->post('out2_q2_pmt_posisi',TRUE),
		'out3_q1_pmt_posisi' => $this->input->post('out3_q1_pmt_posisi',TRUE),
		'out3_q2_pmt_posisi' => $this->input->post('out3_q2_pmt_posisi',TRUE),
		'coupler_q1_pmt_posisi' => $this->input->post('coupler_q1_pmt_posisi',TRUE),
		'coupler_q2_pmt_posisi' => $this->input->post('coupler_q2_pmt_posisi',TRUE),
		'bus_vt_q1_pmt_posisi' => $this->input->post('bus_vt_q1_pmt_posisi',TRUE),
		'bus_vt_q2_pmt_posisi' => $this->input->post('bus_vt_q2_pmt_posisi',TRUE),
		'pht1_counter_pmt_r' => $this->input->post('pht1_counter_pmt_r',TRUE),
		'pht2_counter_pmt_r' => $this->input->post('pht2_counter_pmt_r',TRUE),
		'out1_counter_pmt_r' => $this->input->post('out1_counter_pmt_r',TRUE),
		'out2_counter_pmt_r' => $this->input->post('out2_counter_pmt_r',TRUE),
		'out3_counter_pmt_r' => $this->input->post('out3_counter_pmt_r',TRUE),
		'coupler_counter_pmt_r' => $this->input->post('coupler_counter_pmt_r',TRUE),
		'bus_vt_counter_pmt_r' => $this->input->post('bus_vt_counter_pmt_r',TRUE),
		'pht1_counter_pmt_s' => $this->input->post('pht1_counter_pmt_s',TRUE),
		'pht2_counter_pmt_s' => $this->input->post('pht2_counter_pmt_s',TRUE),
		'out1_counter_pmt_s' => $this->input->post('out1_counter_pmt_s',TRUE),
		'out2_counter_pmt_s' => $this->input->post('out2_counter_pmt_s',TRUE),
		'out3_counter_pmt_s' => $this->input->post('out3_counter_pmt_s',TRUE),
		'coupler_counter_pmt_s' => $this->input->post('coupler_counter_pmt_s',TRUE),
		'bus_vt_counter_pmt_s' => $this->input->post('bus_vt_counter_pmt_s',TRUE),
		'pht1_counter_pmt_t' => $this->input->post('pht1_counter_pmt_t',TRUE),
		'pht2_counter_pmt_t' => $this->input->post('pht2_counter_pmt_t',TRUE),
		'out1_counter_pmt_t' => $this->input->post('out1_counter_pmt_t',TRUE),
		'out2_counter_pmt_t' => $this->input->post('out2_counter_pmt_t',TRUE),
		'out3_counter_pmt_t' => $this->input->post('out3_counter_pmt_t',TRUE),
		'coupler_counter_pmt_t' => $this->input->post('coupler_counter_pmt_t',TRUE),
		'bus_vt_counter_pmt_t' => $this->input->post('bus_vt_counter_pmt_t',TRUE),
		'q15' => $this->input->post('q15',TRUE),
		'q25' => $this->input->post('q25',TRUE),
		'pht1_earthing_pmt' => $this->input->post('pht1_earthing_pmt',TRUE),
		'pht2_earthing_pmt' => $this->input->post('pht2_earthing_pmt',TRUE),
		'out1_earthing_pmt' => $this->input->post('out1_earthing_pmt',TRUE),
		'out2_earthing_pmt' => $this->input->post('out2_earthing_pmt',TRUE),
		'out3_earthing_pmt' => $this->input->post('out3_earthing_pmt',TRUE),
		'coupler_earthing_pmt' => $this->input->post('coupler_earthing_pmt',TRUE),
		'pht1_earthing_pmt_q2' => $this->input->post('pht1_earthing_pmt_q2',TRUE),
		'pht2_earthing_pmt_q2' => $this->input->post('pht2_earthing_pmt_q2',TRUE),
		'out1_earthing_pmt_q2' => $this->input->post('out1_earthing_pmt_q2',TRUE),
		'out2_earthing_pmt_q2' => $this->input->post('out2_earthing_pmt_q2',TRUE),
		'out3_earthing_pmt_q2' => $this->input->post('out3_earthing_pmt_q2',TRUE),
		'coupler_earthing_pmt_q2' => $this->input->post('coupler_earthing_pmt_q2',TRUE),
		'pht1_earthing_line' => $this->input->post('pht1_earthing_line',TRUE),
		'pht2_earthing_line' => $this->input->post('pht2_earthing_line',TRUE),
		'dh110_r1' => $this->input->post('dh110_r1',TRUE),
		'dh110_r2' => $this->input->post('dh110_r2',TRUE),
		'dh48_r1' => $this->input->post('dh48_r1',TRUE),
		'dh48_r2' => $this->input->post('dh48_r2',TRUE),
		'dh110_lr' => $this->input->post('dh110_lr',TRUE),
		'dh48_lr' => $this->input->post('dh48_lr',TRUE),
		'trafo1_level' => $this->input->post('trafo1_level',TRUE),
		'oltc1_level' => $this->input->post('oltc1_level',TRUE),
		'trafo2_level' => $this->input->post('trafo2_level',TRUE),
		'oltc2_level' => $this->input->post('oltc2_level',TRUE),
		'trafo_ps_level' => $this->input->post('trafo_ps_level',TRUE),
		'oltc1_suhu' => $this->input->post('oltc1_suhu',TRUE),
		'oltc2_suhu' => $this->input->post('oltc2_suhu',TRUE),
		'trafo_ps_suhu' => $this->input->post('trafo_ps_suhu',TRUE),
		'trafo1_oil' => $this->input->post('trafo1_oil',TRUE),
		'trafo2_oil' => $this->input->post('trafo2_oil',TRUE),
		'trafo1_hv' => $this->input->post('trafo1_hv',TRUE),
		'trafo2_hv' => $this->input->post('trafo2_hv',TRUE),
		'trafo1_lv' => $this->input->post('trafo1_lv',TRUE),
		'trafo2_lv' => $this->input->post('trafo2_lv',TRUE),
		'trafo1_bucholz' => $this->input->post('trafo1_bucholz',TRUE),
		'trafo1_jansen' => $this->input->post('trafo1_jansen',TRUE),
		'trafo1_termal' => $this->input->post('trafo1_termal',TRUE),
		'trafo1_sudden_press' => $this->input->post('trafo1_sudden_press',TRUE),
		'trafo1_fire' => $this->input->post('trafo1_fire',TRUE),
		'trafo1_ngr' => $this->input->post('trafo1_ngr',TRUE),
		'trafo1_dc' => $this->input->post('trafo1_dc',TRUE),
		'trafo2_bucholz' => $this->input->post('trafo2_bucholz',TRUE),
		'trafo2_jansen' => $this->input->post('trafo2_jansen',TRUE),
		'trafo2_termal' => $this->input->post('trafo2_termal',TRUE),
		'trafo2_sudden_press' => $this->input->post('trafo2_sudden_press',TRUE),
		'trafo2_fire' => $this->input->post('trafo2_fire',TRUE),
		'trafo2_ngr' => $this->input->post('trafo2_ngr',TRUE),
		'trafo2_dc' => $this->input->post('trafo2_dc',TRUE),
		'keterangan_1' => $this->input->post('keterangan_1',TRUE),
		'keterangan_2' => $this->input->post('keterangan_2',TRUE),
		'kondisi_peralatan' => $this->input->post('kondisi_peralatan',TRUE),
		'trafo1_posisi_tap' => $this->input->post('trafo1_posisi_tap',TRUE),
		'trafo2_posisi_tap' => $this->input->post('trafo2_posisi_tap',TRUE),
		'trafo1_counter_oltc' => $this->input->post('trafo1_counter_oltc',TRUE),
		'trafo2_counter_oltc' => $this->input->post('trafo2_counter_oltc',TRUE),
		'suhu_indoor' => $this->input->post('suhu_indoor',TRUE),
		'suhu_outdoor' => $this->input->post('suhu_outdoor',TRUE),
		'kelembaban_indoor' => $this->input->post('kelembaban_indoor',TRUE),
		'kelembaban_outdoor' => $this->input->post('kelembaban_outdoor',TRUE),
		'benda_asing_indoor' => $this->input->post('benda_asing_indoor',TRUE),
		'benda_asing_outdoor' => $this->input->post('benda_asing_outdoor',TRUE),
	    );

            $this->Check_list_gis_model->insert($data);
            set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('check_list_gis'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Check_list_gis_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'Edit data Check_list_gis',
                'action' => site_url('check_list_gis/update_action'),
                'kembali' =>'Check_list_gis',
				'petugas_list' => $this->Petugas_model->get_all(),
				'asisten_list' => $this->Asisten_manajer_model->get_all(),
		'id' => set_value('id', $row->id),
		'tanggal' => set_value('tanggal', $row->tanggal),
		'pukul' => set_value('pukul', $row->pukul),
		'assistant_manager' => set_value('assistant_manager', $row->assistant_manager),
		'petugas_dinas' => set_value('petugas_dinas', $row->petugas_dinas),
	    'pms_line_pht1' => set_value('pms_line_pht1', $row->pms_line_pht1),
	    'pms_line_pht2' => set_value('pms_line_pht2', $row->pms_line_pht2),
		'pht1_q1_pms_bus' => set_value('pht1_q1_pms_bus', $row->pht1_q1_pms_bus),
		'pht1_q2_pms_bus' => set_value('pht1_q2_pms_bus', $row->pht1_q2_pms_bus),
		'pht2_q1_pms_bus' => set_value('pht2_q1_pms_bus', $row->pht2_q1_pms_bus),
		'pht2_q2_pms_bus' => set_value('pht2_q2_pms_bus', $row->pht2_q2_pms_bus),
		'out1_q1_pms_bus' => set_value('out1_q1_pms_bus', $row->out1_q1_pms_bus),
		'out1_q2_pms_bus' => set_value('out1_q2_pms_bus', $row->out1_q2_pms_bus),
		'out2_q1_pms_bus' => set_value('out2_q1_pms_bus', $row->out2_q1_pms_bus),
		'out2_q2_pms_bus' => set_value('out2_q2_pms_bus', $row->out2_q2_pms_bus),
		'out3_q1_pms_bus' => set_value('out3_q1_pms_bus', $row->out3_q1_pms_bus),
		'out3_q2_pms_bus' => set_value('out3_q2_pms_bus', $row->out3_q2_pms_bus),
		'coupler_q1_pms_bus' => set_value('coupler_q1_pms_bus', $row->coupler_q1_pms_bus),
		'coupler_q2_pms_bus' => set_value('coupler_q2_pms_bus', $row->coupler_q2_pms_bus),
		'bus_vt_q1_pms_bus' => set_value('bus_vt_q1_pms_bus', $row->bus_vt_q1_pms_bus),
		'bus_vt_q2_pms_bus' => set_value('bus_vt_q2_pms_bus', $row->bus_vt_q2_pms_bus),
		'pht1_q1_pmt' => set_value('pht1_q1_pmt', $row->pht1_q1_pmt),
		'pht1_q2_pmt' => set_value('pht1_q2_pmt', $row->pht1_q2_pmt),
		'pht2_q1_pmt' => set_value('pht2_q1_pmt', $row->pht2_q1_pmt),
		'pht2_q2_pmt' => set_value('pht2_q2_pmt', $row->pht2_q2_pmt),
		'out1_q1_pmt' => set_value('out1_q1_pmt', $row->out1_q1_pmt),
		'out1_q2_pmt' => set_value('out1_q2_pmt', $row->out1_q2_pmt),
		'out2_q1_pmt' => set_value('out2_q1_pmt', $row->out2_q1_pmt),
		'out2_q2_pmt' => set_value('out2_q2_pmt', $row->out2_q2_pmt),
		'out3_q1_pmt' => set_value('out3_q1_pmt', $row->out3_q1_pmt),
		'out3_q2_pmt' => set_value('out3_q2_pmt', $row->out3_q2_pmt),
		'coupler_q1_pmt' => set_value('coupler_q1_pmt', $row->coupler_q1_pmt),
		'coupler_q2_pmt' => set_value('coupler_q2_pmt', $row->coupler_q2_pmt),
		'pht1_q1_ceilingend' => set_value('pht1_q1_ceilingend', $row->pht1_q1_ceilingend),
		'pht1_q2_ceilingend' => set_value('pht1_q2_ceilingend', $row->pht1_q2_ceilingend),
		'pht2_q1_ceilingend' => set_value('pht2_q1_ceilingend', $row->pht2_q1_ceilingend),
		'pht2_q2_ceilingend' => set_value('pht2_q2_ceilingend', $row->pht2_q2_ceilingend),
		'out1_q1_ceilingend' => set_value('out1_q1_ceilingend', $row->out1_q1_ceilingend),
		'out1_q2_ceilingend' => set_value('out1_q2_ceilingend', $row->out1_q2_ceilingend),
		'out2_q1_ceilingend' => set_value('out2_q1_ceilingend', $row->out2_q1_ceilingend),
		'out2_q2_ceilingend' => set_value('out2_q2_ceilingend', $row->out2_q2_ceilingend),
		'out3_q1_ceilingend' => set_value('out3_q1_ceilingend', $row->out3_q1_ceilingend),
		'out3_q2_ceilingend' => set_value('out3_q2_ceilingend', $row->out3_q2_ceilingend),
		'pht1_q1_vt_line' => set_value('pht1_q1_vt_line', $row->pht1_q1_vt_line),
		'pht1_q2_vt_line' => set_value('pht1_q2_vt_line', $row->pht1_q2_vt_line),
		'pht2_q1_vt_line' => set_value('pht2_q1_vt_line', $row->pht2_q1_vt_line),
		'pht2_q2_vt_line' => set_value('pht2_q2_vt_line', $row->pht2_q2_vt_line),
		'pht1_q1_pms_bus_posisi' => set_value('pht1_q1_pms_bus_posisi', $row->pht1_q1_pms_bus_posisi),
		'pht1_q2_pms_bus_posisi' => set_value('pht1_q2_pms_bus_posisi', $row->pht1_q2_pms_bus_posisi),
		'pht2_q1_pms_bus_posisi' => set_value('pht2_q1_pms_bus_posisi', $row->pht2_q1_pms_bus_posisi),
		'pht2_q2_pms_bus_posisi' => set_value('pht2_q2_pms_bus_posisi', $row->pht2_q2_pms_bus_posisi),
		'out1_q1_pms_bus_posisi' => set_value('out1_q1_pms_bus_posisi', $row->out1_q1_pms_bus_posisi),
		'out1_q2_pms_bus_posisi' => set_value('out1_q2_pms_bus_posisi', $row->out1_q2_pms_bus_posisi),
		'out2_q1_pms_bus_posisi' => set_value('out2_q1_pms_bus_posisi', $row->out2_q1_pms_bus_posisi),
		'out2_q2_pms_bus_posisi' => set_value('out2_q2_pms_bus_posisi', $row->out2_q2_pms_bus_posisi),
		'out3_q1_pms_bus_posisi' => set_value('out3_q1_pms_bus_posisi', $row->out3_q1_pms_bus_posisi),
		'out3_q2_pms_bus_posisi' => set_value('out3_q2_pms_bus_posisi', $row->out3_q2_pms_bus_posisi),
		'coupler_q1_pms_bus_posisi' => set_value('coupler_q1_pms_bus_posisi', $row->coupler_q1_pms_bus_posisi),
		'coupler_q2_pms_bus_posisi' => set_value('coupler_q2_pms_bus_posisi', $row->coupler_q2_pms_bus_posisi),
		'bus_vt_q1_pms_bus_posisi' => set_value('bus_vt_q1_pms_bus_posisi', $row->bus_vt_q1_pms_bus_posisi),
		'bus_vt_q2_pms_bus_posisi' => set_value('bus_vt_q2_pms_bus_posisi', $row->bus_vt_q2_pms_bus_posisi),
		'pht1_q1_pmt_posisi' => set_value('pht1_q1_pmt_posisi', $row->pht1_q1_pmt_posisi),
		'pht2_q1_pmt_posisi' => set_value('pht2_q1_pmt_posisi', $row->pht2_q1_pmt_posisi),
		'pht2_q2_pmt_posisi' => set_value('pht2_q2_pmt_posisi', $row->pht2_q2_pmt_posisi),
		'out1_q1_pmt_posisi' => set_value('out1_q1_pmt_posisi', $row->out1_q1_pmt_posisi),
		'out1_q2_pmt_posisi' => set_value('out1_q2_pmt_posisi', $row->out1_q2_pmt_posisi),
		'out2_q1_pmt_posisi' => set_value('out2_q1_pmt_posisi', $row->out2_q1_pmt_posisi),
		'out2_q2_pmt_posisi' => set_value('out2_q2_pmt_posisi', $row->out2_q2_pmt_posisi),
		'out3_q1_pmt_posisi' => set_value('out3_q1_pmt_posisi', $row->out3_q1_pmt_posisi),
		'out3_q2_pmt_posisi' => set_value('out3_q2_pmt_posisi', $row->out3_q2_pmt_posisi),
		'coupler_q1_pmt_posisi' => set_value('coupler_q1_pmt_posisi', $row->coupler_q1_pmt_posisi),
		'coupler_q2_pmt_posisi' => set_value('coupler_q2_pmt_posisi', $row->coupler_q2_pmt_posisi),
		'bus_vt_q1_pmt_posisi' => set_value('bus_vt_q1_pmt_posisi', $row->bus_vt_q1_pmt_posisi),
		'bus_vt_q2_pmt_posisi' => set_value('bus_vt_q2_pmt_posisi', $row->bus_vt_q2_pmt_posisi),
		'pht1_counter_pmt_r' => set_value('pht1_counter_pmt_r', $row->pht1_counter_pmt_r),
		'pht2_counter_pmt_r' => set_value('pht2_counter_pmt_r', $row->pht2_counter_pmt_r),
		'out1_counter_pmt_r' => set_value('out1_counter_pmt_r', $row->out1_counter_pmt_r),
		'out2_counter_pmt_r' => set_value('out2_counter_pmt_r', $row->out2_counter_pmt_r),
		'out3_counter_pmt_r' => set_value('out3_counter_pmt_r', $row->out3_counter_pmt_r),
		'coupler_counter_pmt_r' => set_value('coupler_counter_pmt_r', $row->coupler_counter_pmt_r),
		'bus_vt_counter_pmt_r' => set_value('bus_vt_counter_pmt_r', $row->bus_vt_counter_pmt_r),
		'pht1_counter_pmt_s' => set_value('pht1_counter_pmt_s', $row->pht1_counter_pmt_s),
		'pht2_counter_pmt_s' => set_value('pht2_counter_pmt_s', $row->pht2_counter_pmt_s),
		'out1_counter_pmt_s' => set_value('out1_counter_pmt_s', $row->out1_counter_pmt_s),
		'out2_counter_pmt_s' => set_value('out2_counter_pmt_s', $row->out2_counter_pmt_s),
		'out3_counter_pmt_s' => set_value('out3_counter_pmt_s', $row->out3_counter_pmt_s),
		'coupler_counter_pmt_s' => set_value('coupler_counter_pmt_s', $row->coupler_counter_pmt_s),
		'bus_vt_counter_pmt_s' => set_value('bus_vt_counter_pmt_s', $row->bus_vt_counter_pmt_s),
		'pht1_counter_pmt_t' => set_value('pht1_counter_pmt_t', $row->pht1_counter_pmt_t),
		'pht2_counter_pmt_t' => set_value('pht2_counter_pmt_t', $row->pht2_counter_pmt_t),
		'out1_counter_pmt_t' => set_value('out1_counter_pmt_t', $row->out1_counter_pmt_t),
		'out2_counter_pmt_t' => set_value('out2_counter_pmt_t', $row->out2_counter_pmt_t),
		'out3_counter_pmt_t' => set_value('out3_counter_pmt_t', $row->out3_counter_pmt_t),
		'coupler_counter_pmt_t' => set_value('coupler_counter_pmt_t', $row->coupler_counter_pmt_t),
		'bus_vt_counter_pmt_t' => set_value('bus_vt_counter_pmt_t', $row->bus_vt_counter_pmt_t),
		'q15' => set_value('q15', $row->q15),
		'q25' => set_value('q25', $row->q25),
		'pht1_earthing_pmt' => set_value('pht1_earthing_pmt', $row->pht1_earthing_pmt),
		'pht2_earthing_pmt' => set_value('pht2_earthing_pmt', $row->pht2_earthing_pmt),
		'out1_earthing_pmt' => set_value('out1_earthing_pmt', $row->out1_earthing_pmt),
		'out2_earthing_pmt' => set_value('out2_earthing_pmt', $row->out2_earthing_pmt),
		'out3_earthing_pmt' => set_value('out3_earthing_pmt', $row->out3_earthing_pmt),
		'coupler_earthing_pmt' => set_value('coupler_earthing_pmt', $row->coupler_earthing_pmt),
		'pht1_earthing_pmt_q2' => set_value('pht1_earthing_pmt_q2', $row->pht1_earthing_pmt_q2),
		'pht2_earthing_pmt_q2' => set_value('pht2_earthing_pmt_q2', $row->pht2_earthing_pmt_q2),
		'out1_earthing_pmt_q2' => set_value('out1_earthing_pmt_q2', $row->out1_earthing_pmt_q2),
		'out2_earthing_pmt_q2' => set_value('out2_earthing_pmt_q2', $row->out2_earthing_pmt_q2),
		'out3_earthing_pmt_q2' => set_value('out3_earthing_pmt_q2', $row->out3_earthing_pmt_q2),
		'coupler_earthing_pmt_q2' => set_value('coupler_earthing_pmt_q2', $row->coupler_earthing_pmt_q2),
		'pht1_earthing_line' => set_value('pht1_earthing_line', $row->pht1_earthing_line),
		'pht2_earthing_line' => set_value('pht2_earthing_line', $row->pht2_earthing_line),
		'dh110_r1' => set_value('dh110_r1', $row->dh110_r1),
		'dh110_r2' => set_value('dh110_r2', $row->dh110_r2),
		'dh48_r1' => set_value('dh48_r1', $row->dh48_r1),
		'dh48_r2' => set_value('dh48_r2', $row->dh48_r2),
		'dh110_lr' => set_value('dh110_lr', $row->dh110_lr),
		'dh48_lr' => set_value('dh48_lr', $row->dh48_lr),
		'trafo1_level' => set_value('trafo1_level', $row->trafo1_level),
		'oltc1_level' => set_value('oltc1_level', $row->oltc1_level),
		'trafo2_level' => set_value('trafo2_level', $row->trafo2_level),
		'oltc2_level' => set_value('oltc2_level', $row->oltc2_level),
		'trafo_ps_level' => set_value('trafo_ps_level', $row->trafo_ps_level),
		'oltc1_suhu' => set_value('oltc1_suhu', $row->oltc1_suhu),
		'oltc2_suhu' => set_value('oltc2_suhu', $row->oltc2_suhu),
		'trafo_ps_suhu' => set_value('trafo_ps_suhu', $row->trafo_ps_suhu),
		'trafo1_oil' => set_value('trafo1_oil', $row->trafo1_oil),
		'trafo2_oil' => set_value('trafo2_oil', $row->trafo2_oil),
		'trafo1_hv' => set_value('trafo1_hv', $row->trafo1_hv),
		'trafo2_hv' => set_value('trafo2_hv', $row->trafo2_hv),
		'trafo1_lv' => set_value('trafo1_lv', $row->trafo1_lv),
		'trafo2_lv' => set_value('trafo2_lv', $row->trafo2_lv),
		'trafo1_bucholz' => set_value('trafo1_bucholz', $row->trafo1_bucholz),
		'trafo1_jansen' => set_value('trafo1_jansen', $row->trafo1_jansen),
		'trafo1_termal' => set_value('trafo1_termal', $row->trafo1_termal),
		'trafo1_sudden_press' => set_value('trafo1_sudden_press', $row->trafo1_sudden_press),
		'trafo1_fire' => set_value('trafo1_fire', $row->trafo1_fire),
		'trafo1_ngr' => set_value('trafo1_ngr', $row->trafo1_ngr),
		'trafo1_dc' => set_value('trafo1_dc', $row->trafo1_dc),
		'trafo2_bucholz' => set_value('trafo2_bucholz', $row->trafo2_bucholz),
		'trafo2_jansen' => set_value('trafo2_jansen', $row->trafo2_jansen),
		'trafo2_termal' => set_value('trafo2_termal', $row->trafo2_termal),
		'trafo2_sudden_press' => set_value('trafo2_sudden_press', $row->trafo2_sudden_press),
		'trafo2_fire' => set_value('trafo2_fire', $row->trafo2_fire),
		'trafo2_ngr' => set_value('trafo2_ngr', $row->trafo2_ngr),
		'trafo2_dc' => set_value('trafo2_dc', $row->trafo2_dc),
		'keterangan_1' => set_value('keterangan_1', $row->keterangan_1),
		'keterangan_2' => set_value('keterangan_2', $row->keterangan_2),
		'kondisi_peralatan' => set_value('kondisi_peralatan', $row->kondisi_peralatan),
		'trafo1_posisi_tap' => set_value('trafo1_posisi_tap', $row->trafo1_posisi_tap),
	    'trafo2_posisi_tap' => set_value('trafo2_posisi_tap', $row->trafo2_posisi_tap),
	    'trafo1_counter_oltc' => set_value('trafo1_counter_oltc', $row->trafo1_counter_oltc),
	    'trafo2_counter_oltc' => set_value('trafo2_counter_oltc', $row->trafo2_counter_oltc),
	    'kelembaban_indoor' => set_value('kelembaban_indoor', $row->kelembaban_indoor),
	    'kelembaban_outdoor' => set_value('kelembaban_outdoor', $row->kelembaban_outdoor),
	    'suhu_indoor' => set_value('suhu_indoor', $row->suhu_indoor),
	    'suhu_outdoor' => set_value('suhu_outdoor', $row->suhu_outdoor),
	    'benda_asing_indoor' => set_value('benda_asing_indoor', $row->benda_asing_indoor),
	    'benda_asing_outdoor' => set_value('benda_asing_outdoor', $row->benda_asing_outdoor),
	    );
            $this->template->load('layout','check_list_gis/Check_list_gis_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('check_list_gis'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'tanggal' => date('Y-m-d'),
                'pukul' => date('H:i:s'),
		'assistant_manager' => $this->input->post('assistant_manager',TRUE),
		'petugas_dinas' => $this->input->post('petugas_dinas',TRUE),
		'pms_line_pht1' => $this->input->post('pms_line_pht1',TRUE),
		'pms_line_pht2' => $this->input->post('pms_line_pht2',TRUE),
		'pht1_q1_pms_bus' => $this->input->post('pht1_q1_pms_bus',TRUE),
		'pht1_q2_pms_bus' => $this->input->post('pht1_q2_pms_bus',TRUE),
		'pht2_q1_pms_bus' => $this->input->post('pht2_q1_pms_bus',TRUE),
		'pht2_q2_pms_bus' => $this->input->post('pht2_q2_pms_bus',TRUE),
		'out1_q1_pms_bus' => $this->input->post('out1_q1_pms_bus',TRUE),
		'out1_q2_pms_bus' => $this->input->post('out1_q2_pms_bus',TRUE),
		'out2_q1_pms_bus' => $this->input->post('out2_q1_pms_bus',TRUE),
		'out2_q2_pms_bus' => $this->input->post('out2_q2_pms_bus',TRUE),
		'out3_q1_pms_bus' => $this->input->post('out3_q1_pms_bus',TRUE),
		'out3_q2_pms_bus' => $this->input->post('out3_q2_pms_bus',TRUE),
		'coupler_q1_pms_bus' => $this->input->post('coupler_q1_pms_bus',TRUE),
		'coupler_q2_pms_bus' => $this->input->post('coupler_q2_pms_bus',TRUE),
		'bus_vt_q1_pms_bus' => $this->input->post('bus_vt_q1_pms_bus',TRUE),
		'bus_vt_q2_pms_bus' => $this->input->post('bus_vt_q2_pms_bus',TRUE),
		'pht1_q1_pmt' => $this->input->post('pht1_q1_pmt',TRUE),
		'pht1_q2_pmt' => $this->input->post('pht1_q2_pmt',TRUE),
		'pht2_q1_pmt' => $this->input->post('pht2_q1_pmt',TRUE),
		'pht2_q2_pmt' => $this->input->post('pht2_q2_pmt',TRUE),
		'out1_q1_pmt' => $this->input->post('out1_q1_pmt',TRUE),
		'out1_q2_pmt' => $this->input->post('out1_q2_pmt',TRUE),
		'out2_q1_pmt' => $this->input->post('out2_q1_pmt',TRUE),
		'out2_q2_pmt' => $this->input->post('out2_q2_pmt',TRUE),
		'out3_q1_pmt' => $this->input->post('out3_q1_pmt',TRUE),
		'out3_q2_pmt' => $this->input->post('out3_q2_pmt',TRUE),
		'coupler_q1_pmt' => $this->input->post('coupler_q1_pmt',TRUE),
		'coupler_q2_pmt' => $this->input->post('coupler_q2_pmt',TRUE),
		'pht1_q1_ceilingend' => $this->input->post('pht1_q1_ceilingend',TRUE),
		'pht1_q2_ceilingend' => $this->input->post('pht1_q2_ceilingend',TRUE),
		'pht2_q1_ceilingend' => $this->input->post('pht2_q1_ceilingend',TRUE),
		'pht2_q2_ceilingend' => $this->input->post('pht2_q2_ceilingend',TRUE),
		'out1_q1_ceilingend' => $this->input->post('out1_q1_ceilingend',TRUE),
		'out1_q2_ceilingend' => $this->input->post('out1_q2_ceilingend',TRUE),
		'out2_q1_ceilingend' => $this->input->post('out2_q1_ceilingend',TRUE),
		'out2_q2_ceilingend' => $this->input->post('out2_q2_ceilingend',TRUE),
		'out3_q1_ceilingend' => $this->input->post('out3_q1_ceilingend',TRUE),
		'out3_q2_ceilingend' => $this->input->post('out3_q2_ceilingend',TRUE),
		'pht1_q1_vt_line' => $this->input->post('pht1_q1_vt_line',TRUE),
		'pht1_q2_vt_line' => $this->input->post('pht1_q2_vt_line',TRUE),
		'pht2_q1_vt_line' => $this->input->post('pht2_q1_vt_line',TRUE),
		'pht2_q2_vt_line' => $this->input->post('pht2_q2_vt_line',TRUE),
		'pht1_q1_pms_bus_posisi' => $this->input->post('pht1_q1_pms_bus_posisi',TRUE),
		'pht1_q2_pms_bus_posisi' => $this->input->post('pht1_q2_pms_bus_posisi',TRUE),
		'pht2_q1_pms_bus_posisi' => $this->input->post('pht2_q1_pms_bus_posisi',TRUE),
		'pht2_q2_pms_bus_posisi' => $this->input->post('pht2_q2_pms_bus_posisi',TRUE),
		'out1_q1_pms_bus_posisi' => $this->input->post('out1_q1_pms_bus_posisi',TRUE),
		'out1_q2_pms_bus_posisi' => $this->input->post('out1_q2_pms_bus_posisi',TRUE),
		'out2_q1_pms_bus_posisi' => $this->input->post('out2_q1_pms_bus_posisi',TRUE),
		'out2_q2_pms_bus_posisi' => $this->input->post('out2_q2_pms_bus_posisi',TRUE),
		'out3_q1_pms_bus_posisi' => $this->input->post('out3_q1_pms_bus_posisi',TRUE),
		'out3_q2_pms_bus_posisi' => $this->input->post('out3_q2_pms_bus_posisi',TRUE),
		'coupler_q1_pms_bus_posisi' => $this->input->post('coupler_q1_pms_bus_posisi',TRUE),
		'coupler_q2_pms_bus_posisi' => $this->input->post('coupler_q2_pms_bus_posisi',TRUE),
		'bus_vt_q1_pms_bus_posisi' => $this->input->post('bus_vt_q1_pms_bus_posisi',TRUE),
		'bus_vt_q2_pms_bus_posisi' => $this->input->post('bus_vt_q2_pms_bus_posisi',TRUE),
		'pht1_q1_pmt_posisi' => $this->input->post('pht1_q1_pmt_posisi',TRUE),
		'pht2_q1_pmt_posisi' => $this->input->post('pht2_q1_pmt_posisi',TRUE),
		'pht2_q2_pmt_posisi' => $this->input->post('pht2_q2_pmt_posisi',TRUE),
		'out1_q1_pmt_posisi' => $this->input->post('out1_q1_pmt_posisi',TRUE),
		'out1_q2_pmt_posisi' => $this->input->post('out1_q2_pmt_posisi',TRUE),
		'out2_q1_pmt_posisi' => $this->input->post('out2_q1_pmt_posisi',TRUE),
		'out2_q2_pmt_posisi' => $this->input->post('out2_q2_pmt_posisi',TRUE),
		'out3_q1_pmt_posisi' => $this->input->post('out3_q1_pmt_posisi',TRUE),
		'out3_q2_pmt_posisi' => $this->input->post('out3_q2_pmt_posisi',TRUE),
		'coupler_q1_pmt_posisi' => $this->input->post('coupler_q1_pmt_posisi',TRUE),
		'coupler_q2_pmt_posisi' => $this->input->post('coupler_q2_pmt_posisi',TRUE),
		'bus_vt_q1_pmt_posisi' => $this->input->post('bus_vt_q1_pmt_posisi',TRUE),
		'bus_vt_q2_pmt_posisi' => $this->input->post('bus_vt_q2_pmt_posisi',TRUE),
		'pht1_counter_pmt_r' => $this->input->post('pht1_counter_pmt_r',TRUE),
		'pht2_counter_pmt_r' => $this->input->post('pht2_counter_pmt_r',TRUE),
		'out1_counter_pmt_r' => $this->input->post('out1_counter_pmt_r',TRUE),
		'out2_counter_pmt_r' => $this->input->post('out2_counter_pmt_r',TRUE),
		'out3_counter_pmt_r' => $this->input->post('out3_counter_pmt_r',TRUE),
		'coupler_counter_pmt_r' => $this->input->post('coupler_counter_pmt_r',TRUE),
		'bus_vt_counter_pmt_r' => $this->input->post('bus_vt_counter_pmt_r',TRUE),
		'pht1_counter_pmt_s' => $this->input->post('pht1_counter_pmt_s',TRUE),
		'pht2_counter_pmt_s' => $this->input->post('pht2_counter_pmt_s',TRUE),
		'out1_counter_pmt_s' => $this->input->post('out1_counter_pmt_s',TRUE),
		'out2_counter_pmt_s' => $this->input->post('out2_counter_pmt_s',TRUE),
		'out3_counter_pmt_s' => $this->input->post('out3_counter_pmt_s',TRUE),
		'coupler_counter_pmt_s' => $this->input->post('coupler_counter_pmt_s',TRUE),
		'bus_vt_counter_pmt_s' => $this->input->post('bus_vt_counter_pmt_s',TRUE),
		'pht1_counter_pmt_t' => $this->input->post('pht1_counter_pmt_t',TRUE),
		'pht2_counter_pmt_t' => $this->input->post('pht2_counter_pmt_t',TRUE),
		'out1_counter_pmt_t' => $this->input->post('out1_counter_pmt_t',TRUE),
		'out2_counter_pmt_t' => $this->input->post('out2_counter_pmt_t',TRUE),
		'out3_counter_pmt_t' => $this->input->post('out3_counter_pmt_t',TRUE),
		'coupler_counter_pmt_t' => $this->input->post('coupler_counter_pmt_t',TRUE),
		'bus_vt_counter_pmt_t' => $this->input->post('bus_vt_counter_pmt_t',TRUE),
		'q15' => $this->input->post('q15',TRUE),
		'q25' => $this->input->post('q25',TRUE),
		'pht1_earthing_pmt' => $this->input->post('pht1_earthing_pmt',TRUE),
		'pht2_earthing_pmt' => $this->input->post('pht2_earthing_pmt',TRUE),
		'out1_earthing_pmt' => $this->input->post('out1_earthing_pmt',TRUE),
		'out2_earthing_pmt' => $this->input->post('out2_earthing_pmt',TRUE),
		'out3_earthing_pmt' => $this->input->post('out3_earthing_pmt',TRUE),
		'coupler_earthing_pmt_q2' => $this->input->post('coupler_earthing_pmt_q2',TRUE),
		'pht1_earthing_pmt_q2' => $this->input->post('pht1_earthing_pmt_q2',TRUE),
		'pht2_earthing_pmt_q2' => $this->input->post('pht2_earthing_pmt_q2',TRUE),
		'out1_earthing_pmt_q2' => $this->input->post('out1_earthing_pmt_q2',TRUE),
		'out2_earthing_pmt_q2' => $this->input->post('out2_earthing_pmt_q2',TRUE),
		'out3_earthing_pmt_q2' => $this->input->post('out3_earthing_pmt_q2',TRUE),
		'coupler_earthing_pmt_q2' => $this->input->post('coupler_earthing_pmt_q2',TRUE),
		'pht1_earthing_line' => $this->input->post('pht1_earthing_line',TRUE),
		'pht2_earthing_line' => $this->input->post('pht2_earthing_line',TRUE),
		'dh110_r1' => $this->input->post('dh110_r1',TRUE),
		'dh110_r2' => $this->input->post('dh110_r2',TRUE),
		'dh48_r1' => $this->input->post('dh48_r1',TRUE),
		'dh48_r2' => $this->input->post('dh48_r2',TRUE),
		'dh110_lr' => $this->input->post('dh110_lr',TRUE),
		'dh48_lr' => $this->input->post('dh48_lr',TRUE),
		'trafo1_level' => $this->input->post('trafo1_level',TRUE),
		'oltc1_level' => $this->input->post('oltc1_level',TRUE),
		'trafo2_level' => $this->input->post('trafo2_level',TRUE),
		'oltc2_level' => $this->input->post('oltc2_level',TRUE),
		'trafo_ps_level' => $this->input->post('trafo_ps_level',TRUE),
		'oltc1_suhu' => $this->input->post('oltc1_suhu',TRUE),
		'oltc2_suhu' => $this->input->post('oltc2_suhu',TRUE),
		'trafo_ps_suhu' => $this->input->post('trafo_ps_suhu',TRUE),
		'trafo1_oil' => $this->input->post('trafo1_oil',TRUE),
		'trafo2_oil' => $this->input->post('trafo2_oil',TRUE),
		'trafo1_hv' => $this->input->post('trafo1_hv',TRUE),
		'trafo2_hv' => $this->input->post('trafo2_hv',TRUE),
		'trafo1_lv' => $this->input->post('trafo1_lv',TRUE),
		'trafo2_lv' => $this->input->post('trafo2_lv',TRUE),
		'trafo1_bucholz' => $this->input->post('trafo1_bucholz',TRUE),
		'trafo1_jansen' => $this->input->post('trafo1_jansen',TRUE),
		'trafo1_termal' => $this->input->post('trafo1_termal',TRUE),
		'trafo1_sudden_press' => $this->input->post('trafo1_sudden_press',TRUE),
		'trafo1_fire' => $this->input->post('trafo1_fire',TRUE),
		'trafo1_ngr' => $this->input->post('trafo1_ngr',TRUE),
		'trafo1_dc' => $this->input->post('trafo1_dc',TRUE),
		'trafo2_bucholz' => $this->input->post('trafo2_bucholz',TRUE),
		'trafo2_jansen' => $this->input->post('trafo2_jansen',TRUE),
		'trafo2_termal' => $this->input->post('trafo2_termal',TRUE),
		'trafo2_sudden_press' => $this->input->post('trafo2_sudden_press',TRUE),
		'trafo2_fire' => $this->input->post('trafo2_fire',TRUE),
		'trafo2_ngr' => $this->input->post('trafo2_ngr',TRUE),
		'trafo2_dc' => $this->input->post('trafo2_dc',TRUE),
		'keterangan_1' => $this->input->post('keterangan_1',TRUE),
		'keterangan_2' => $this->input->post('keterangan_2',TRUE),
		'kondisi_peralatan' => $this->input->post('kondisi_peralatan',TRUE),
		'trafo1_posisi_tap' => $this->input->post('trafo1_posisi_tap',TRUE),
		'trafo2_posisi_tap' => $this->input->post('trafo2_posisi_tap',TRUE),
		'trafo1_counter_oltc' => $this->input->post('trafo1_counter_oltc',TRUE),
		'trafo2_counter_oltc' => $this->input->post('trafo2_counter_oltc',TRUE),
		'suhu_indoor' => $this->input->post('suhu_indoor',TRUE),
		'suhu_outdoor' => $this->input->post('suhu_outdoor',TRUE),
		'kelembaban_indoor' => $this->input->post('kelembaban_indoor',TRUE),
		'kelembaban_outdoor' => $this->input->post('kelembaban_outdoor',TRUE),
		'benda_asing_indoor' => $this->input->post('benda_asing_indoor',TRUE),
		'benda_asing_outdoor' => $this->input->post('benda_asing_outdoor',TRUE),
	    );

            $this->Check_list_gis_model->update($this->input->post('id', TRUE), $data);
            set_flashdata('success', 'Update Record Success');
            redirect(site_url('check_list_gis'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Check_list_gis_model->get_by_id($id);

        if ($row) {
            $this->Check_list_gis_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('check_list_gis'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('check_list_gis'));
        }
    }

    public function _rules()
    {
	// $this->form_validation->set_rules('assistant_manager', 'assistant manager', 'trim|required');
	// $this->form_validation->set_rules('petugas_dinas', 'petugas dinas', 'trim|required');
	// $this->form_validation->set_rules('pht1_q1_pms_bus', 'pht1 q1 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht1_q2_pms_bus', 'pht1 q2 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht2_q1_pms_bus', 'pht2 q1 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht2_q2_pms_bus', 'pht2 q2 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('out1_q1_pms_bus', 'out1 q1 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('out1_q2_pms_bus', 'out1 q2 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('out2_q1_pms_bus', 'out2 q1 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('out2_q2_pms_bus', 'out2 q2 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('out3_q1_pms_bus', 'out3 q1 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('out3_q2_pms_bus', 'out3 q2 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('coupler_q1_pms_bus', 'coupler q1 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('coupler_q2_pms_bus', 'coupler q2 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('bus_vt_q1_pms_bus', 'bus vt q1 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('bus_vt_q2_pms_bus', 'bus vt q2 pms bus', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht1_q1_pmt', 'pht1 q1 pmt', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht1_q2_pmt', 'pht1 q2 pmt', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht2_q1_pmt', 'pht2 q1 pmt', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht2_q2_pmt', 'pht2 q2 pmt', 'trim|required|numeric');
	// $this->form_validation->set_rules('out1_q1_pmt', 'out1 q1 pmt', 'trim|required|numeric');
	// $this->form_validation->set_rules('out1_q2_pmt', 'out1 q2 pmt', 'trim|required|numeric');
	// $this->form_validation->set_rules('out2_q1_pmt', 'out2 q1 pmt', 'trim|required|numeric');
	// $this->form_validation->set_rules('out2_q2_pmt', 'out2 q2 pmt', 'trim|required|numeric');
	// $this->form_validation->set_rules('out3_q1_pmt', 'out3 q1 pmt', 'trim|required|numeric');
	// $this->form_validation->set_rules('out3_q2_pmt', 'out3 q2 pmt', 'trim|required|numeric');
	// $this->form_validation->set_rules('coupler_q1_pmt', 'coupler q1 pmt', 'trim|required|numeric');
	// $this->form_validation->set_rules('coupler_q2_pmt', 'coupler q2 pmt', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht1_q1_ceilingend', 'pht1 q1 ceilingend', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht1_q2_ceilingend', 'pht1 q2 ceilingend', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht2_q1_ceilingend', 'pht2 q1 ceilingend', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht2_q2_ceilingend', 'pht2 q2 ceilingend', 'trim|required|numeric');
	// $this->form_validation->set_rules('out1_q1_ceilingend', 'out1 q1 ceilingend', 'trim|required|numeric');
	// $this->form_validation->set_rules('out1_q2_ceilingend', 'out1 q2 ceilingend', 'trim|required|numeric');
	// $this->form_validation->set_rules('out2_q1_ceilingend', 'out2 q1 ceilingend', 'trim|required|numeric');
	// $this->form_validation->set_rules('out2_q2_ceilingend', 'out2 q2 ceilingend', 'trim|required|numeric');
	// $this->form_validation->set_rules('out3_q1_ceilingend', 'out3 q1 ceilingend', 'trim|required|numeric');
	// $this->form_validation->set_rules('out3_q2_ceilingend', 'out3 q2 ceilingend', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht1_q1_vt_line', 'pht1 q1 vt line', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht1_q2_vt_line', 'pht1 q2 vt line', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht2_q1_vt_line', 'pht2 q1 vt line', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht2_q2_vt_line', 'pht2 q2 vt line', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht1_q1_pms_bus_posisi', 'pht1 q1 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('pht1_q2_pms_bus_posisi', 'pht1 q2 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('pht2_q1_pms_bus_posisi', 'pht2 q1 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('pht2_q2_pms_bus_posisi', 'pht2 q2 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('out1_q1_pms_bus_posisi', 'out1 q1 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('out1_q2_pms_bus_posisi', 'out1 q2 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('out2_q1_pms_bus_posisi', 'out2 q1 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('out2_q2_pms_bus_posisi', 'out2 q2 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('out3_q1_pms_bus_posisi', 'out3 q1 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('out3_q2_pms_bus_posisi', 'out3 q2 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('coupler_q1_pms_bus_posisi', 'coupler q1 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('coupler_q2_pms_bus_posisi', 'coupler q2 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('bus_vt_q1_pms_bus_posisi', 'bus vt q1 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('bus_vt_q2_pms_bus_posisi', 'bus vt q2 pms bus posisi', 'trim|required');
	// $this->form_validation->set_rules('pht1_q1_pmt_posisi', 'pht1 q1 pmt posisi', 'trim|required');
	// $this->form_validation->set_rules('pht2_q1_pmt_posisi', 'pht2 q1 pmt posisi', 'trim|required');
	// $this->form_validation->set_rules('pht2_q2_pmt_posisi', 'pht2 q2 pmt posisi', 'trim|required');
	// $this->form_validation->set_rules('out1_q1_pmt_posisi', 'out1 q1 pmt posisi', 'trim|required');
	// $this->form_validation->set_rules('out1_q2_pmt_posisi', 'out1 q2 pmt posisi', 'trim|required');
	// $this->form_validation->set_rules('out2_q1_pmt_posisi', 'out2 q1 pmt posisi', 'trim|required');
	// $this->form_validation->set_rules('out2_q2_pmt_posisi', 'out2 q2 pmt posisi', 'trim|required');
	// $this->form_validation->set_rules('out3_q1_pmt_posisi', 'out3 q1 pmt posisi', 'trim|required');
	// $this->form_validation->set_rules('out3_q2_pmt_posisi', 'out3 q2 pmt posisi', 'trim|required');
	// $this->form_validation->set_rules('coupler_q1_pmt_posisi', 'coupler q1 pmt posisi', 'trim|required');
	// $this->form_validation->set_rules('coupler_q2_pmt_posisi', 'coupler q2 pmt posisi', 'trim|required');
	// $this->form_validation->set_rules('bus_vt_q1_pmt_posisi', 'bus vt q1 pmt posisi', 'trim|required');
	// $this->form_validation->set_rules('bus_vt_q2_pmt_posisi', 'bus vt q2 pmt posisi', 'trim|required');
	// $this->form_validation->set_rules('pht1_counter_pmt_r', 'pht1 counter pmt r', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht2_counter_pmt_r', 'pht2 counter pmt r', 'trim|required|numeric');
	// $this->form_validation->set_rules('out1_counter_pmt_r', 'out1 counter pmt r', 'trim|required|numeric');
	// $this->form_validation->set_rules('out2_counter_pmt_r', 'out2 counter pmt r', 'trim|required|numeric');
	// $this->form_validation->set_rules('out3_counter_pmt_r', 'out3 counter pmt r', 'trim|required|numeric');
	// $this->form_validation->set_rules('coupler_counter_pmt_r', 'coupler counter pmt r', 'trim|required|numeric');
	// $this->form_validation->set_rules('bus_vt_counter_pmt_r', 'bus vt counter pmt r', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht1_counter_pmt_s', 'pht1 counter pmt s', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht2_counter_pmt_s', 'pht2 counter pmt s', 'trim|required|numeric');
	// $this->form_validation->set_rules('out1_counter_pmt_s', 'out1 counter pmt s', 'trim|required|numeric');
	// $this->form_validation->set_rules('out2_counter_pmt_s', 'out2 counter pmt s', 'trim|required|numeric');
	// $this->form_validation->set_rules('out3_counter_pmt_s', 'out3 counter pmt s', 'trim|required|numeric');
	// $this->form_validation->set_rules('coupler_counter_pmt_s', 'coupler counter pmt s', 'trim|required|numeric');
	// $this->form_validation->set_rules('bus_vt_counter_pmt_s', 'bus vt counter pmt s', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht1_counter_pmt_t', 'pht1 counter pmt t', 'trim|required|numeric');
	// $this->form_validation->set_rules('pht2_counter_pmt_t', 'pht2 counter pmt t', 'trim|required|numeric');
	// $this->form_validation->set_rules('out1_counter_pmt_t', 'out1 counter pmt t', 'trim|required|numeric');
	// $this->form_validation->set_rules('out2_counter_pmt_t', 'out2 counter pmt t', 'trim|required|numeric');
	// $this->form_validation->set_rules('out3_counter_pmt_t', 'out3 counter pmt t', 'trim|required|numeric');
	// $this->form_validation->set_rules('coupler_counter_pmt_t', 'coupler counter pmt t', 'trim|required|numeric');
	// $this->form_validation->set_rules('bus_vt_counter_pmt_t', 'bus vt counter pmt t', 'trim|required|numeric');
	// $this->form_validation->set_rules('q15', 'q15', 'trim|required');
	// $this->form_validation->set_rules('q25', 'q25', 'trim|required');
	// $this->form_validation->set_rules('pht1_earthing_pmt', 'pht1 earthing pmt', 'trim|required');
	// $this->form_validation->set_rules('pht2_earthing_pmt', 'pht2 earthing pmt', 'trim|required');
	// $this->form_validation->set_rules('out1_earthing_pmt', 'out1 earthing pmt', 'trim|required');
	// $this->form_validation->set_rules('out2_earthing_pmt', 'out2 earthing pmt', 'trim|required');
	// $this->form_validation->set_rules('out3_earthing_pmt', 'out3 earthing pmt', 'trim|required');
	// $this->form_validation->set_rules('coupler_earthing_pmt', 'coupler earthing pmt', 'trim|required');
	// $this->form_validation->set_rules('pht1_earthing_line', 'pht1 earthing line', 'trim|required');
	// $this->form_validation->set_rules('pht2_earthing_line', 'pht2 earthing line', 'trim|required');
	// $this->form_validation->set_rules('dh110_r1', 'dh110 r1', 'trim|required|numeric');
	// $this->form_validation->set_rules('dh110_r2', 'dh110 r2', 'trim|required|numeric');
	// $this->form_validation->set_rules('dh48_r1', 'dh48 r1', 'trim|required|numeric');
	// $this->form_validation->set_rules('dh48_r2', 'dh48 r2', 'trim|required|numeric');
	// $this->form_validation->set_rules('dh110_lr', 'dh110 lr', 'trim|required');
	// $this->form_validation->set_rules('dh48_lr', 'dh48 lr', 'trim|required');
	// $this->form_validation->set_rules('trafo1_level', 'trafo1 level', 'trim|required');
	// $this->form_validation->set_rules('oltc1_level', 'oltc1 level', 'trim|required');
	// $this->form_validation->set_rules('trafo2_level', 'trafo2 level', 'trim|required');
	// $this->form_validation->set_rules('oltc2_level', 'oltc2 level', 'trim|required');
	// $this->form_validation->set_rules('trafo_ps_level', 'trafo ps level', 'trim|required');
	// $this->form_validation->set_rules('oltc1_suhu', 'oltc1 suhu', 'trim|required|numeric');
	// $this->form_validation->set_rules('oltc2_suhu', 'oltc2 suhu', 'trim|required|numeric');
	// $this->form_validation->set_rules('trafo_ps_suhu', 'trafo ps suhu', 'trim|required|numeric');
	// $this->form_validation->set_rules('trafo1_oil', 'trafo1 oil', 'trim|required|numeric');
	// $this->form_validation->set_rules('trafo2_oil', 'trafo2 oil', 'trim|required|numeric');
	// $this->form_validation->set_rules('trafo1_hv', 'trafo1 hv', 'trim|required|numeric');
	// $this->form_validation->set_rules('trafo2_hv', 'trafo2 hv', 'trim|required|numeric');
	// $this->form_validation->set_rules('trafo1_lv', 'trafo1 lv', 'trim|required|numeric');
	// $this->form_validation->set_rules('trafo2_lv', 'trafo2 lv', 'trim|required|numeric');
	// $this->form_validation->set_rules('trafo1_bucholz', 'trafo1 bucholz', 'trim|required');
	// $this->form_validation->set_rules('trafo1_jansen', 'trafo1 jansen', 'trim|required');
	// $this->form_validation->set_rules('trafo1_termal', 'trafo1 termal', 'trim|required');
	// $this->form_validation->set_rules('trafo1_sudden_press', 'trafo1 sudden press', 'trim|required');
	// $this->form_validation->set_rules('trafo1_fire', 'trafo1 fire', 'trim|required');
	// $this->form_validation->set_rules('trafo1_ngr', 'trafo1 ngr', 'trim|required');
	// $this->form_validation->set_rules('trafo1_dc', 'trafo1 dc', 'trim|required');
	// $this->form_validation->set_rules('trafo2_bucholz', 'trafo2 bucholz', 'trim|required');
	// $this->form_validation->set_rules('trafo2_jansen', 'trafo2 jansen', 'trim|required');
	// $this->form_validation->set_rules('trafo2_termal', 'trafo2 termal', 'trim|required');
	// $this->form_validation->set_rules('trafo2_sudden_press', 'trafo2 sudden press', 'trim|required');
	// $this->form_validation->set_rules('trafo2_fire', 'trafo2 fire', 'trim|required');
	// $this->form_validation->set_rules('trafo2_ngr', 'trafo2 ngr', 'trim|required');
	// $this->form_validation->set_rules('trafo2_dc', 'trafo2 dc', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Check_list_gis.php */
/* Location: ./application/controllers/Check_list_gis.php */