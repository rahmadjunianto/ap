<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ms_panel extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Ms_panel_model');
        $this->load->library('form_validation');
        $this->id_pengguna=get_userdata('app_id_pengguna');
    }

    private function cekAkses($var=null){
        $url='Ms_panel';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()

    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'ms_panel?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'ms_panel?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'ms_panel';
            $config['first_url'] = base_url() . 'ms_panel';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Ms_panel_model->total_rows($q);
        $ms_panel                      = $this->Ms_panel_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'ms_panel_data' => $ms_panel,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Ms_panel',
            'create'              => 'Ms_panel/create',
            'akses'               =>$akses
        );
        $this->template->load('layout','ms_panel/Ms_panel_list', $data);
    }



    public function create()
    {
        $this->cekAkses('create');

        $data = array(
            'title'   =>'Tambah Data Ms_panel',
            'kembali' =>'Ms_panel',
            'action'  => site_url('ms_panel/create_action'),
	    'id' => set_value('id'),
	    'panel' => set_value('panel'),
	    'jenis_panel' => set_value('jenis_panel'),
	);
        $this->template->load('layout','ms_panel/Ms_panel_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'panel' => $this->input->post('panel',TRUE),
		'jenis_panel' => $this->input->post('jenis_panel',TRUE),
	    );

            $this->Ms_panel_model->insert($data);
            set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('ms_panel'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Ms_panel_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'Edit data Ms_panel',
                'action' => site_url('ms_panel/update_action'),
                'kembali' =>'Ms_panel',
		'id' => set_value('id', $row->id),
		'panel' => set_value('panel', $row->panel),
		'jenis_panel' => set_value('jenis_panel', $row->jenis_panel),
	    );
            $this->template->load('layout','ms_panel/Ms_panel_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('ms_panel'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'panel' => $this->input->post('panel',TRUE),
		'jenis_panel' => $this->input->post('jenis_panel',TRUE),
	    );

            $this->Ms_panel_model->update($this->input->post('id', TRUE), $data);
            set_flashdata('success', 'Update Record Success');
            redirect(site_url('ms_panel'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id=rapikan($ide);
        $row = $this->Ms_panel_model->get_by_id($id);

        if ($row) {
            $this->Ms_panel_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('ms_panel'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('ms_panel'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('panel', 'panel', 'trim|required');
	$this->form_validation->set_rules('jenis_panel', 'jenis panel', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Ms_panel.php */
/* Location: ./application/controllers/Ms_panel.php */